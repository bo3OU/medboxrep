﻿Public Class AddSupplier
    Private FormLoad As Boolean = False
    Private ID As Integer = -1
    Private modifying As Boolean = False
    Public Sub SetId(a As Integer)
        ID = a
    End Sub

    Private Nom As String
    Private Prenom As String
    Private Ville As String
    Private Adresse As String
    Private Email As String
    Private Societe As String
    Private Telephone As String



    Public Sub ShowAndFill(a As Integer)
        modifying = True
        ID = a
        AddModSupplier.Text = "Modifier"
        AddAnotherSupplier.Visible = False
        Access.AddParam("@id", a)
        Access.ExecQuery("Select 
                        FRS_NOM as nom,
                        FRS_PRENOM as prenom,
                        FRS_VILLE as ville,
                        FRS_ADRESSE as adresse,
                        FRS_EMAIL as email,
                        FRS_SOCIETE as societe,
                        FRS_TEL as telephone
                        from fournisseur where fournisseur.FRS_ID_FOURNISSEUR = @id")
        If Access.DBDT.Rows.Count = 1 Then
            Prenom = Access.DBDT.Rows(0).Item("prenom").ToString()
            Nom = Access.DBDT.Rows(0).Item("nom").ToString()
            Ville = Access.DBDT.Rows(0).Item("ville").ToString()
            Adresse = Access.DBDT.Rows(0).Item("adresse").ToString()
            Email = Access.DBDT.Rows(0).Item("email").ToString()
            Societe = Access.DBDT.Rows(0).Item("societe").ToString()
            Telephone = Access.DBDT.Rows(0).Item("telephone").ToString()

            PrenomFournisseur_TXB.Text = Prenom
            NomFournisseur_TXB.Text = Nom
            SocieteFournisseur_TXB.Text = Societe
            VilleFournisseur_TXB.Text = Ville
            AdresseFournisseur_TXB.Text = Adresse
            EmailFournisseur_TXB.Text = Email
            TelephoneFournisseur_TXB.Text = Telephone
        End If
        ShowDialog()
    End Sub

    Private Sub AddSupplier_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        FormLoad = True
    End Sub

#Region "PLACEHOLDER SIMULATION"
    Private Sub Suppliers_TXB_Enter(sender As Object, e As EventArgs) Handles VilleFournisseur_TXB.Enter, TelephoneFournisseur_TXB.Enter, SocieteFournisseur_TXB.Enter, PrenomFournisseur_TXB.Enter, NomFournisseur_TXB.Enter, EmailFournisseur_TXB.Enter, AdresseFournisseur_TXB.Enter
        Dim tags As Integer = DirectCast(sender, Bunifu.Framework.UI.BunifuMetroTextbox).Tag
        Select Case tags
            Case 0
                'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
                If Trim(PrenomFournisseur_TXB.Text) = "Prenom du Fournisseur" Then
                    PrenomFournisseur_TXB.Text = ""
                End If
            Case 1
                'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
                If Trim(NomFournisseur_TXB.Text) = "Nom du Fournisseur" Then
                    NomFournisseur_TXB.Text = ""
                End If
            Case 2
                'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
                If Trim(SocieteFournisseur_TXB.Text) = "Societe" Then
                    SocieteFournisseur_TXB.Text = ""
                End If
            Case 3
                'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
                If Trim(VilleFournisseur_TXB.Text) = "Ville du Fournisseur" Then
                    VilleFournisseur_TXB.Text = ""
                End If
            Case 4
                'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
                If Trim(AdresseFournisseur_TXB.Text) = "Adresse du Fournisseur" Then
                    AdresseFournisseur_TXB.Text = ""
                End If
            Case 5
                'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
                If Trim(EmailFournisseur_TXB.Text) = "Email du Fournisseur" Then
                    EmailFournisseur_TXB.Text = ""
                End If
            Case 6
                'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
                If Trim(TelephoneFournisseur_TXB.Text) = "N° de Telephone" Then
                    TelephoneFournisseur_TXB.Text = ""
                End If
        End Select
    End Sub

    Private Sub Suppliers_TXB_Leave(sender As Object, e As EventArgs) Handles VilleFournisseur_TXB.Leave, TelephoneFournisseur_TXB.Leave, SocieteFournisseur_TXB.Leave, PrenomFournisseur_TXB.Leave, NomFournisseur_TXB.Leave, EmailFournisseur_TXB.Leave, AdresseFournisseur_TXB.Leave
        Dim tags As Integer = DirectCast(sender, Bunifu.Framework.UI.BunifuMetroTextbox).Tag
        Select Case tags
            Case 0
                'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
                If Trim(PrenomFournisseur_TXB.Text) = "" Then
                    PrenomFournisseur_TXB.Text = "Prenom du Fournisseur"
                End If
            Case 1
                'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
                If Trim(NomFournisseur_TXB.Text) = "" Then
                    NomFournisseur_TXB.Text = "Nom du Fournisseur"
                End If
            Case 2
                'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
                If Trim(SocieteFournisseur_TXB.Text) = "" Then
                    SocieteFournisseur_TXB.Text = "Societe"
                End If
            Case 3
                'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
                If Trim(VilleFournisseur_TXB.Text) = "" Then
                    VilleFournisseur_TXB.Text = "Ville du Fournisseur"
                End If
            Case 4
                'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
                If Trim(AdresseFournisseur_TXB.Text) = "" Then
                    AdresseFournisseur_TXB.Text = "Adresse du Fournisseur"
                End If
            Case 5
                'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
                If Trim(EmailFournisseur_TXB.Text) = "" Then
                    EmailFournisseur_TXB.Text = "Email du Fournisseur"
                End If
            Case 6
                'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
                If Trim(TelephoneFournisseur_TXB.Text) = "" Then
                    TelephoneFournisseur_TXB.Text = "N° de Telephone"
                End If
        End Select
    End Sub
#End Region


    Private Function AddSupplier()
        Dim FournisseurID As Integer = -1
        Dim state As Integer = 0
        If Trim(PrenomFournisseur_TXB.Text) <> "" And Trim(PrenomFournisseur_TXB.Text) <> "Prenom du Fournisseur" Then
            If Trim(NomFournisseur_TXB.Text) <> "" And Trim(NomFournisseur_TXB.Text) <> "Nom du Fournisseur" Then
                If Trim(SocieteFournisseur_TXB.Text) <> "" And Trim(SocieteFournisseur_TXB.Text) <> "Societe" Then
                    If Trim(VilleFournisseur_TXB.Text) <> "" And Trim(VilleFournisseur_TXB.Text) <> "Ville du Fournisseur" Then
                        If Trim(AdresseFournisseur_TXB.Text) <> "" And Trim(AdresseFournisseur_TXB.Text) <> "Adresse du Fournisseur" Then
                            If Trim(EmailFournisseur_TXB.Text) <> "" And Trim(EmailFournisseur_TXB.Text) <> "Email du Fournisseur" Then
                                If Trim(TelephoneFournisseur_TXB.Text) <> "" And Trim(TelephoneFournisseur_TXB.Text) <> "N° de Telephone" Then
                                    Access.AddParam("@prenom", Trim(PrenomFournisseur_TXB.Text))
                                    Access.AddParam("@nom", Trim(NomFournisseur_TXB.Text))
                                    Access.AddParam("@email", Trim(EmailFournisseur_TXB.Text))
                                    Access.AddParam("@telephone", Trim(TelephoneFournisseur_TXB.Text))
                                    Access.ExecQuery("select fournisseur.FRS_ID_FOURNISSEUR 
                                                    From (FOURNI
                                                    LEFT JOIN Fournisseur ON fournisseur.FRS_ID_FOURNISSEUR = fourni.FO_FOURNISSEUR)
                                                    where FO_ARTICLE IS NULL and fourni.FO_CABINET = " & SESSION.GetCabinet() & "
                                                    and (fournisseur.FRS_PRENOM = @prenom and fournisseur.FRS_NOM = @nom ) 
                                                    OR fournisseur.FRS_EMAIL = @email 
                                                    OR fournisseur.FRS_TEL = @telephone")
                                    If NotEmpty(Access.Exception) Then
                                        MsgBox(Access.Exception)
                                    End If

                                    If Access.DBDT.Rows.Count > 0 Then
                                        MsgBox("erreur ! " & vbNewLine & " possibilité : nom prenom , telphone ou email deja existant ")
                                    Else
                                        Access.AddParam("@nom", Trim(NomFournisseur_TXB.Text))
                                        Access.AddParam("@prenom", Trim(PrenomFournisseur_TXB.Text))
                                        Access.AddParam("@societe", Trim(SocieteFournisseur_TXB.Text))
                                        Access.AddParam("@ville", Trim(VilleFournisseur_TXB.Text))
                                        Access.AddParam("@adresse", Trim(AdresseFournisseur_TXB.Text))
                                        Access.AddParam("@email", Trim(EmailFournisseur_TXB.Text))
                                        Access.AddParam("@telephone", Trim(TelephoneFournisseur_TXB.Text))
                                        Access.InsertQuery("insert into FOURNISSEUR (FRS_NOM,FRS_PRENOM,FRS_SOCIETE,FRS_VILLE,FRS_ADRESSE,FRS_EMAIL,FRS_TEL)
                                        values(@nom , @prenom , @societe , @ville , @adresse  ,@email , @telephone)")
                                        FournisseurID = Access.ID
                                        If NotEmpty(Access.Exception) Then
                                            MsgBox(Access.Exception)
                                        ElseIf FournisseurID <> 0 Then
                                            Access.InsertQuery("insert into FOURNI ([FO_ARTICLE],[FO_FOURNISSEUR],[FO_CABINET],[FO_PRIX])
                                            values(NULL," & FournisseurID & "," & SESSION.GetCabinet() & ",0)")
                                            If NotEmpty(Access.Exception) Then
                                                MsgBox(Access.Exception)
                                            Else
                                                Return 1
                                            End If
                                        End If
                                    End If
                                End If
                            Else
                            End If
                        Else
                        End If
                    Else
                    End If
                Else
                End If
            Else
            End If
        Else
        End If
        Return 0
    End Function

    Private Sub ClearFields()
        PrenomFournisseur_TXB.Text = "Prenom du Fournisseur"
        NomFournisseur_TXB.Text = "Nom du Fournisseur"
        SocieteFournisseur_TXB.Text = "Societe"
        VilleFournisseur_TXB.Text = "Ville du Fournisseur"
        AdresseFournisseur_TXB.Text = "Adresse du Fournisseur"
        EmailFournisseur_TXB.Text = "Email du Fournisseur"
        TelephoneFournisseur_TXB.Text = "N° de Telephone"
    End Sub



    Private Sub AddAnotherArticleButton_Click(sender As Object, e As EventArgs) Handles AddAnotherSupplier.Click
        If AddSupplier() = 1 Then
            MainForm.BringIndicator(MainForm.SubMenuButton_5_BTN, New EventArgs)
            ClearFields()
        End If
    End Sub

    Private Sub ModifySupplier()
        If Trim(PrenomFournisseur_TXB.Text) <> "" And Trim(PrenomFournisseur_TXB.Text) <> "Prenom du Fournisseur" Then
            If Trim(NomFournisseur_TXB.Text) <> "" And Trim(NomFournisseur_TXB.Text) <> "Nom du Fournisseur" Then
                If Trim(SocieteFournisseur_TXB.Text) <> "" And Trim(SocieteFournisseur_TXB.Text) <> "Societe" Then
                    If Trim(VilleFournisseur_TXB.Text) <> "" And Trim(VilleFournisseur_TXB.Text) <> "Ville du Fournisseur" Then
                        If Trim(AdresseFournisseur_TXB.Text) <> "" And Trim(AdresseFournisseur_TXB.Text) <> "Adresse du Fournisseur" Then
                            If Trim(EmailFournisseur_TXB.Text) <> "" And Trim(EmailFournisseur_TXB.Text) <> "Email du Fournisseur" Then
                                If Trim(TelephoneFournisseur_TXB.Text) <> "" And Trim(TelephoneFournisseur_TXB.Text) <> "N° de Telephone" Then
                                    Access.AddParam("@idsupplier", ID)
                                    Access.AddParam("@prenom", Trim(PrenomFournisseur_TXB.Text))
                                    Access.AddParam("@nom", Trim(NomFournisseur_TXB.Text))
                                    Access.AddParam("@email", Trim(EmailFournisseur_TXB.Text))
                                    Access.AddParam("@telephone", Trim(TelephoneFournisseur_TXB.Text))
                                    Access.ExecQuery("
                                                    select fournisseur.FRS_ID_FOURNISSEUR as id 
                                                    From (FOURNI
                                                    LEFT JOIN Fournisseur ON fournisseur.FRS_ID_FOURNISSEUR = fourni.FO_FOURNISSEUR)
                                                    where fournisseur.FRS_ID_FOURNISSEUR  <>  " & ID & "
                                                    and ( FO_ARTICLE IS NULL and fourni.FO_CABINET = " & SESSION.GetCabinet() & "
                                                    and (  fournisseur.FRS_PRENOM = @prenom and fournisseur.FRS_NOM = @nom ) 
                                                    OR (fournisseur.FRS_EMAIL = @email
                                                    or fournisseur.FRS_TEL = @telephone ))")
                                    If NotEmpty(Access.Exception) Then
                                        MsgBox(Access.Exception)
                                    End If
                                    If Access.DBDT.Rows.Count > 0 Then
                                        MsgBox(Access.DBDT.Rows(0).Item("id"))
                                        MsgBox("erreur ! " & vbNewLine & " possibilité : nom prenom deja existant , telephone deja existant ou Email deja existant ")
                                    Else
                                        Access.AddParam("@name", Trim(NomFournisseur_TXB.Text))
                                        Access.AddParam("@prenom", Trim(PrenomFournisseur_TXB.Text))
                                        Access.AddParam("@societe", Trim(SocieteFournisseur_TXB.Text))
                                        Access.AddParam("@ville", Trim(VilleFournisseur_TXB.Text))
                                        Access.AddParam("@adresse", Trim(AdresseFournisseur_TXB.Text))
                                        Access.AddParam("@telephone", Trim(TelephoneFournisseur_TXB.Text))
                                        Access.AddParam("@email", Trim(EmailFournisseur_TXB.Text))
                                        Access.ExecQuery("
                                                            update FOURNISSEUR 
                                                            set FRS_NOM = @name
                                                            , FRS_PRENOM = @prenom
                                                            , FRS_SOCIETE = @societe 
                                                            , FRS_VILLE= @ville 
                                                            , FRS_ADRESSE = @adresse
                                                            , FRS_TEL = @telephone
                                                            , FRS_EMAIL = @email
                                                            where FRS_ID_FOURNISSEUR = " & ID)
                                        If NotEmpty(Access.Exception) Then
                                            MsgBox(Access.Exception)
                                        Else
                                            modifying = False
                                            MainForm.BringIndicator(MainForm.SubMenuButton_5_BTN, New EventArgs)
                                            Close()
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub AddModSupplier_Click(sender As Object, e As EventArgs) Handles AddModSupplier.Click
        If modifying = False Then
            If AddSupplier() = 1 Then
                MainForm.BringIndicator(MainForm.SubMenuButton_5_BTN, New EventArgs)
                Close()
            End If
        Else
            ModifySupplier()
        End If

    End Sub

    Private Sub BunifuImageButton2_Click(sender As Object, e As EventArgs) Handles BunifuImageButton2.Click
        Close()
    End Sub

    Private Sub BunifuImageButton1_Click(sender As Object, e As EventArgs) Handles BunifuImageButton1.Click
        If modifying Then
            PrenomFournisseur_TXB.Text = Prenom
            NomFournisseur_TXB.Text = Nom
            SocieteFournisseur_TXB.Text = Societe
            VilleFournisseur_TXB.Text = Ville
            AdresseFournisseur_TXB.Text = Adresse
            EmailFournisseur_TXB.Text = Email
            TelephoneFournisseur_TXB.Text = Telephone
        Else
            ClearFields()
        End If
    End Sub
End Class