﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class AddArticle
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.BunifuCustomLabel3 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.Orders_Unite_CBB = New System.Windows.Forms.ComboBox()
        Me.Alerte_Min = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.BunifuImageButton1 = New Bunifu.Framework.UI.BunifuImageButton()
        Me.AddAnotherArticleButton = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.AddArticleButton = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.SubAlerteMin = New Bunifu.Framework.UI.BunifuImageButton()
        Me.AddAlerteMin = New Bunifu.Framework.UI.BunifuImageButton()
        Me.AddArticle_Nom_TXB = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.SubAlerteMax = New Bunifu.Framework.UI.BunifuImageButton()
        Me.AddAlerteMax = New Bunifu.Framework.UI.BunifuImageButton()
        Me.Alerte_Max = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.BunifuCustomLabel1 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.BunifuCustomLabel2 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.BunifuImageButton2 = New Bunifu.Framework.UI.BunifuImageButton()
        Me.BunifuCustomLabel4 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.BunifuCustomLabel5 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.ListSuppliers1 = New System.Windows.Forms.ComboBox()
        Me.SupplierPrice1 = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.SupplierPrice2 = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.ListSuppliers2 = New System.Windows.Forms.ComboBox()
        Me.SupplierPrice3 = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.ListSuppliers3 = New System.Windows.Forms.ComboBox()
        Me.Reset3 = New Bunifu.Framework.UI.BunifuImageButton()
        Me.Reset2 = New Bunifu.Framework.UI.BunifuImageButton()
        Me.Reset1 = New Bunifu.Framework.UI.BunifuImageButton()
        Me.BunifuDragControl1 = New Bunifu.Framework.UI.BunifuDragControl(Me.components)
        Me.BunifuFlatButton1 = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.PriceLabel = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.SubInitialQuantity = New Bunifu.Framework.UI.BunifuImageButton()
        Me.AddInitialQuantity = New Bunifu.Framework.UI.BunifuImageButton()
        Me.InitialQuantity = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.BunifuCustomLabel6 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.BunifuImageButton3 = New Bunifu.Framework.UI.BunifuImageButton()
        Me.BunifuElipse1 = New Bunifu.Framework.UI.BunifuElipse(Me.components)
        Me.BunifuElipse2 = New Bunifu.Framework.UI.BunifuElipse(Me.components)
        Me.BunifuElipse3 = New Bunifu.Framework.UI.BunifuElipse(Me.components)
        Me.BunifuElipse4 = New Bunifu.Framework.UI.BunifuElipse(Me.components)
        CType(Me.BunifuImageButton1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SubAlerteMin, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AddAlerteMin, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SubAlerteMax, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AddAlerteMax, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.BunifuImageButton2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Reset3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Reset2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Reset1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SubInitialQuantity, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AddInitialQuantity, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BunifuImageButton3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BunifuCustomLabel3
        '
        Me.BunifuCustomLabel3.AutoSize = True
        Me.BunifuCustomLabel3.Location = New System.Drawing.Point(43, 188)
        Me.BunifuCustomLabel3.Name = "BunifuCustomLabel3"
        Me.BunifuCustomLabel3.Size = New System.Drawing.Size(38, 13)
        Me.BunifuCustomLabel3.TabIndex = 26
        Me.BunifuCustomLabel3.Text = "Unite :"
        '
        'Orders_Unite_CBB
        '
        Me.Orders_Unite_CBB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Orders_Unite_CBB.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Orders_Unite_CBB.FormattingEnabled = True
        Me.Orders_Unite_CBB.ItemHeight = 22
        Me.Orders_Unite_CBB.Location = New System.Drawing.Point(133, 178)
        Me.Orders_Unite_CBB.Name = "Orders_Unite_CBB"
        Me.Orders_Unite_CBB.Size = New System.Drawing.Size(184, 30)
        Me.Orders_Unite_CBB.TabIndex = 17
        '
        'Alerte_Min
        '
        Me.Alerte_Min.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Alerte_Min.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Alerte_Min.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Alerte_Min.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Alerte_Min.BorderThickness = 3
        Me.Alerte_Min.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Alerte_Min.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Alerte_Min.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.Alerte_Min.isPassword = False
        Me.Alerte_Min.Location = New System.Drawing.Point(206, 229)
        Me.Alerte_Min.Margin = New System.Windows.Forms.Padding(4)
        Me.Alerte_Min.Name = "Alerte_Min"
        Me.Alerte_Min.Size = New System.Drawing.Size(54, 44)
        Me.Alerte_Min.TabIndex = 16
        Me.Alerte_Min.Text = "1"
        Me.Alerte_Min.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BunifuImageButton1
        '
        Me.BunifuImageButton1.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.BunifuImageButton1.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Refresh
        Me.BunifuImageButton1.ImageActive = Nothing
        Me.BunifuImageButton1.Location = New System.Drawing.Point(330, 90)
        Me.BunifuImageButton1.Name = "BunifuImageButton1"
        Me.BunifuImageButton1.Size = New System.Drawing.Size(21, 21)
        Me.BunifuImageButton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.BunifuImageButton1.TabIndex = 23
        Me.BunifuImageButton1.TabStop = False
        Me.BunifuImageButton1.Zoom = 10
        '
        'AddAnotherArticleButton
        '
        Me.AddAnotherArticleButton.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.AddAnotherArticleButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.AddAnotherArticleButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.AddAnotherArticleButton.BorderRadius = 0
        Me.AddAnotherArticleButton.ButtonText = "ajouter et rester"
        Me.AddAnotherArticleButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AddAnotherArticleButton.DisabledColor = System.Drawing.Color.Gray
        Me.AddAnotherArticleButton.Iconcolor = System.Drawing.Color.Transparent
        Me.AddAnotherArticleButton.Iconimage = Nothing
        Me.AddAnotherArticleButton.Iconimage_right = Nothing
        Me.AddAnotherArticleButton.Iconimage_right_Selected = Nothing
        Me.AddAnotherArticleButton.Iconimage_Selected = Nothing
        Me.AddAnotherArticleButton.IconMarginLeft = 0
        Me.AddAnotherArticleButton.IconMarginRight = 0
        Me.AddAnotherArticleButton.IconRightVisible = True
        Me.AddAnotherArticleButton.IconRightZoom = 0R
        Me.AddAnotherArticleButton.IconVisible = True
        Me.AddAnotherArticleButton.IconZoom = 90.0R
        Me.AddAnotherArticleButton.IsTab = False
        Me.AddAnotherArticleButton.Location = New System.Drawing.Point(122, 534)
        Me.AddAnotherArticleButton.Name = "AddAnotherArticleButton"
        Me.AddAnotherArticleButton.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.AddAnotherArticleButton.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.AddAnotherArticleButton.OnHoverTextColor = System.Drawing.Color.White
        Me.AddAnotherArticleButton.selected = False
        Me.AddAnotherArticleButton.Size = New System.Drawing.Size(113, 38)
        Me.AddAnotherArticleButton.TabIndex = 22
        Me.AddAnotherArticleButton.Text = "ajouter et rester"
        Me.AddAnotherArticleButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AddAnotherArticleButton.Textcolor = System.Drawing.Color.White
        Me.AddAnotherArticleButton.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'AddArticleButton
        '
        Me.AddArticleButton.Activecolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.AddArticleButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.AddArticleButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.AddArticleButton.BorderRadius = 0
        Me.AddArticleButton.ButtonText = "Ajouter et quitter"
        Me.AddArticleButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AddArticleButton.DisabledColor = System.Drawing.Color.Gray
        Me.AddArticleButton.Iconcolor = System.Drawing.Color.Transparent
        Me.AddArticleButton.Iconimage = Nothing
        Me.AddArticleButton.Iconimage_right = Nothing
        Me.AddArticleButton.Iconimage_right_Selected = Nothing
        Me.AddArticleButton.Iconimage_Selected = Nothing
        Me.AddArticleButton.IconMarginLeft = 0
        Me.AddArticleButton.IconMarginRight = 0
        Me.AddArticleButton.IconRightVisible = True
        Me.AddArticleButton.IconRightZoom = 0R
        Me.AddArticleButton.IconVisible = True
        Me.AddArticleButton.IconZoom = 90.0R
        Me.AddArticleButton.IsTab = False
        Me.AddArticleButton.Location = New System.Drawing.Point(241, 534)
        Me.AddArticleButton.Name = "AddArticleButton"
        Me.AddArticleButton.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.AddArticleButton.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.AddArticleButton.OnHoverTextColor = System.Drawing.Color.White
        Me.AddArticleButton.selected = False
        Me.AddArticleButton.Size = New System.Drawing.Size(109, 38)
        Me.AddArticleButton.TabIndex = 21
        Me.AddArticleButton.Text = "Ajouter et quitter"
        Me.AddArticleButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AddArticleButton.Textcolor = System.Drawing.Color.White
        Me.AddArticleButton.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'SubAlerteMin
        '
        Me.SubAlerteMin.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.SubAlerteMin.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Cancel
        Me.SubAlerteMin.ImageActive = Nothing
        Me.SubAlerteMin.Location = New System.Drawing.Point(174, 239)
        Me.SubAlerteMin.Name = "SubAlerteMin"
        Me.SubAlerteMin.Size = New System.Drawing.Size(25, 25)
        Me.SubAlerteMin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.SubAlerteMin.TabIndex = 19
        Me.SubAlerteMin.TabStop = False
        Me.SubAlerteMin.Zoom = 10
        '
        'AddAlerteMin
        '
        Me.AddAlerteMin.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.AddAlerteMin.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Add
        Me.AddAlerteMin.ImageActive = Nothing
        Me.AddAlerteMin.Location = New System.Drawing.Point(267, 239)
        Me.AddAlerteMin.Name = "AddAlerteMin"
        Me.AddAlerteMin.Size = New System.Drawing.Size(25, 25)
        Me.AddAlerteMin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.AddAlerteMin.TabIndex = 18
        Me.AddAlerteMin.TabStop = False
        Me.AddAlerteMin.Zoom = 10
        '
        'AddArticle_Nom_TXB
        '
        Me.AddArticle_Nom_TXB.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.AddArticle_Nom_TXB.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.AddArticle_Nom_TXB.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.AddArticle_Nom_TXB.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.AddArticle_Nom_TXB.BorderThickness = 3
        Me.AddArticle_Nom_TXB.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.AddArticle_Nom_TXB.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.AddArticle_Nom_TXB.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.AddArticle_Nom_TXB.isPassword = False
        Me.AddArticle_Nom_TXB.Location = New System.Drawing.Point(46, 124)
        Me.AddArticle_Nom_TXB.Margin = New System.Windows.Forms.Padding(4)
        Me.AddArticle_Nom_TXB.Name = "AddArticle_Nom_TXB"
        Me.AddArticle_Nom_TXB.Size = New System.Drawing.Size(271, 36)
        Me.AddArticle_Nom_TXB.TabIndex = 27
        Me.AddArticle_Nom_TXB.Text = "Nom de l'article"
        Me.AddArticle_Nom_TXB.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'SubAlerteMax
        '
        Me.SubAlerteMax.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.SubAlerteMax.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Cancel
        Me.SubAlerteMax.ImageActive = Nothing
        Me.SubAlerteMax.Location = New System.Drawing.Point(174, 306)
        Me.SubAlerteMax.Name = "SubAlerteMax"
        Me.SubAlerteMax.Size = New System.Drawing.Size(25, 25)
        Me.SubAlerteMax.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.SubAlerteMax.TabIndex = 30
        Me.SubAlerteMax.TabStop = False
        Me.SubAlerteMax.Zoom = 10
        '
        'AddAlerteMax
        '
        Me.AddAlerteMax.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.AddAlerteMax.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Add
        Me.AddAlerteMax.ImageActive = Nothing
        Me.AddAlerteMax.Location = New System.Drawing.Point(267, 306)
        Me.AddAlerteMax.Name = "AddAlerteMax"
        Me.AddAlerteMax.Size = New System.Drawing.Size(25, 25)
        Me.AddAlerteMax.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.AddAlerteMax.TabIndex = 29
        Me.AddAlerteMax.TabStop = False
        Me.AddAlerteMax.Zoom = 10
        '
        'Alerte_Max
        '
        Me.Alerte_Max.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Alerte_Max.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Alerte_Max.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Alerte_Max.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Alerte_Max.BorderThickness = 3
        Me.Alerte_Max.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Alerte_Max.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Alerte_Max.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.Alerte_Max.isPassword = False
        Me.Alerte_Max.Location = New System.Drawing.Point(206, 296)
        Me.Alerte_Max.Margin = New System.Windows.Forms.Padding(4)
        Me.Alerte_Max.Name = "Alerte_Max"
        Me.Alerte_Max.Size = New System.Drawing.Size(54, 44)
        Me.Alerte_Max.TabIndex = 28
        Me.Alerte_Max.Text = "1"
        Me.Alerte_Max.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BunifuCustomLabel1
        '
        Me.BunifuCustomLabel1.AutoSize = True
        Me.BunifuCustomLabel1.Location = New System.Drawing.Point(43, 246)
        Me.BunifuCustomLabel1.Name = "BunifuCustomLabel1"
        Me.BunifuCustomLabel1.Size = New System.Drawing.Size(87, 13)
        Me.BunifuCustomLabel1.TabIndex = 31
        Me.BunifuCustomLabel1.Text = "Alerte Minimum : "
        Me.BunifuCustomLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BunifuCustomLabel2
        '
        Me.BunifuCustomLabel2.AutoSize = True
        Me.BunifuCustomLabel2.Location = New System.Drawing.Point(43, 313)
        Me.BunifuCustomLabel2.Name = "BunifuCustomLabel2"
        Me.BunifuCustomLabel2.Size = New System.Drawing.Size(90, 13)
        Me.BunifuCustomLabel2.TabIndex = 32
        Me.BunifuCustomLabel2.Text = "Alerte Maximum : "
        Me.BunifuCustomLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Panel1.Controls.Add(Me.BunifuImageButton2)
        Me.Panel1.Controls.Add(Me.BunifuCustomLabel4)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(362, 84)
        Me.Panel1.TabIndex = 33
        '
        'BunifuImageButton2
        '
        Me.BunifuImageButton2.BackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.BunifuImageButton2.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Delete
        Me.BunifuImageButton2.ImageActive = Nothing
        Me.BunifuImageButton2.Location = New System.Drawing.Point(332, 0)
        Me.BunifuImageButton2.Name = "BunifuImageButton2"
        Me.BunifuImageButton2.Size = New System.Drawing.Size(30, 27)
        Me.BunifuImageButton2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.BunifuImageButton2.TabIndex = 3
        Me.BunifuImageButton2.TabStop = False
        Me.BunifuImageButton2.Zoom = 10
        '
        'BunifuCustomLabel4
        '
        Me.BunifuCustomLabel4.AutoSize = True
        Me.BunifuCustomLabel4.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel4.ForeColor = System.Drawing.Color.White
        Me.BunifuCustomLabel4.Location = New System.Drawing.Point(104, 27)
        Me.BunifuCustomLabel4.Name = "BunifuCustomLabel4"
        Me.BunifuCustomLabel4.Size = New System.Drawing.Size(156, 25)
        Me.BunifuCustomLabel4.TabIndex = 0
        Me.BunifuCustomLabel4.Text = "Ajouter Article"
        '
        'BunifuCustomLabel5
        '
        Me.BunifuCustomLabel5.AutoSize = True
        Me.BunifuCustomLabel5.Location = New System.Drawing.Point(43, 370)
        Me.BunifuCustomLabel5.Name = "BunifuCustomLabel5"
        Me.BunifuCustomLabel5.Size = New System.Drawing.Size(67, 13)
        Me.BunifuCustomLabel5.TabIndex = 35
        Me.BunifuCustomLabel5.Text = "Fournisseur :"
        '
        'ListSuppliers1
        '
        Me.ListSuppliers1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ListSuppliers1.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListSuppliers1.FormattingEnabled = True
        Me.ListSuppliers1.ItemHeight = 22
        Me.ListSuppliers1.Location = New System.Drawing.Point(25, 397)
        Me.ListSuppliers1.Name = "ListSuppliers1"
        Me.ListSuppliers1.Size = New System.Drawing.Size(210, 30)
        Me.ListSuppliers1.TabIndex = 34
        '
        'SupplierPrice1
        '
        Me.SupplierPrice1.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierPrice1.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierPrice1.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierPrice1.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierPrice1.BorderThickness = 3
        Me.SupplierPrice1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.SupplierPrice1.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SupplierPrice1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.SupplierPrice1.isPassword = False
        Me.SupplierPrice1.Location = New System.Drawing.Point(242, 397)
        Me.SupplierPrice1.Margin = New System.Windows.Forms.Padding(4)
        Me.SupplierPrice1.Name = "SupplierPrice1"
        Me.SupplierPrice1.Size = New System.Drawing.Size(54, 30)
        Me.SupplierPrice1.TabIndex = 36
        Me.SupplierPrice1.Text = "1"
        Me.SupplierPrice1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'SupplierPrice2
        '
        Me.SupplierPrice2.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierPrice2.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierPrice2.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierPrice2.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierPrice2.BorderThickness = 3
        Me.SupplierPrice2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.SupplierPrice2.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SupplierPrice2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.SupplierPrice2.isPassword = False
        Me.SupplierPrice2.Location = New System.Drawing.Point(242, 433)
        Me.SupplierPrice2.Margin = New System.Windows.Forms.Padding(4)
        Me.SupplierPrice2.Name = "SupplierPrice2"
        Me.SupplierPrice2.Size = New System.Drawing.Size(54, 30)
        Me.SupplierPrice2.TabIndex = 38
        Me.SupplierPrice2.Text = "1"
        Me.SupplierPrice2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.SupplierPrice2.Visible = False
        '
        'ListSuppliers2
        '
        Me.ListSuppliers2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ListSuppliers2.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListSuppliers2.FormattingEnabled = True
        Me.ListSuppliers2.ItemHeight = 22
        Me.ListSuppliers2.Location = New System.Drawing.Point(25, 433)
        Me.ListSuppliers2.Name = "ListSuppliers2"
        Me.ListSuppliers2.Size = New System.Drawing.Size(210, 30)
        Me.ListSuppliers2.TabIndex = 37
        Me.ListSuppliers2.Visible = False
        '
        'SupplierPrice3
        '
        Me.SupplierPrice3.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierPrice3.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierPrice3.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierPrice3.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierPrice3.BorderThickness = 3
        Me.SupplierPrice3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.SupplierPrice3.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SupplierPrice3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.SupplierPrice3.isPassword = False
        Me.SupplierPrice3.Location = New System.Drawing.Point(242, 469)
        Me.SupplierPrice3.Margin = New System.Windows.Forms.Padding(4)
        Me.SupplierPrice3.Name = "SupplierPrice3"
        Me.SupplierPrice3.Size = New System.Drawing.Size(54, 30)
        Me.SupplierPrice3.TabIndex = 40
        Me.SupplierPrice3.Text = "1"
        Me.SupplierPrice3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.SupplierPrice3.Visible = False
        '
        'ListSuppliers3
        '
        Me.ListSuppliers3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ListSuppliers3.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListSuppliers3.FormattingEnabled = True
        Me.ListSuppliers3.ItemHeight = 22
        Me.ListSuppliers3.Location = New System.Drawing.Point(25, 469)
        Me.ListSuppliers3.Name = "ListSuppliers3"
        Me.ListSuppliers3.Size = New System.Drawing.Size(210, 30)
        Me.ListSuppliers3.TabIndex = 39
        Me.ListSuppliers3.Visible = False
        '
        'Reset3
        '
        Me.Reset3.BackColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.Reset3.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Cancel
        Me.Reset3.ImageActive = Nothing
        Me.Reset3.Location = New System.Drawing.Point(303, 469)
        Me.Reset3.Name = "Reset3"
        Me.Reset3.Size = New System.Drawing.Size(32, 30)
        Me.Reset3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Reset3.TabIndex = 41
        Me.Reset3.TabStop = False
        Me.Reset3.Visible = False
        Me.Reset3.Zoom = 10
        '
        'Reset2
        '
        Me.Reset2.BackColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.Reset2.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Cancel
        Me.Reset2.ImageActive = Nothing
        Me.Reset2.Location = New System.Drawing.Point(303, 433)
        Me.Reset2.Name = "Reset2"
        Me.Reset2.Size = New System.Drawing.Size(32, 30)
        Me.Reset2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Reset2.TabIndex = 42
        Me.Reset2.TabStop = False
        Me.Reset2.Visible = False
        Me.Reset2.Zoom = 10
        '
        'Reset1
        '
        Me.Reset1.BackColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.Reset1.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Cancel
        Me.Reset1.ImageActive = Nothing
        Me.Reset1.Location = New System.Drawing.Point(303, 397)
        Me.Reset1.Name = "Reset1"
        Me.Reset1.Size = New System.Drawing.Size(32, 30)
        Me.Reset1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Reset1.TabIndex = 43
        Me.Reset1.TabStop = False
        Me.Reset1.Zoom = 10
        '
        'BunifuDragControl1
        '
        Me.BunifuDragControl1.Fixed = True
        Me.BunifuDragControl1.Horizontal = True
        Me.BunifuDragControl1.TargetControl = Me.Panel1
        Me.BunifuDragControl1.Vertical = True
        '
        'BunifuFlatButton1
        '
        Me.BunifuFlatButton1.Activecolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.BunifuFlatButton1.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.BunifuFlatButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BunifuFlatButton1.BorderRadius = 0
        Me.BunifuFlatButton1.ButtonText = "Ajout Initiale"
        Me.BunifuFlatButton1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BunifuFlatButton1.DisabledColor = System.Drawing.Color.Gray
        Me.BunifuFlatButton1.Iconcolor = System.Drawing.Color.Transparent
        Me.BunifuFlatButton1.Iconimage = Nothing
        Me.BunifuFlatButton1.Iconimage_right = Nothing
        Me.BunifuFlatButton1.Iconimage_right_Selected = Nothing
        Me.BunifuFlatButton1.Iconimage_Selected = Nothing
        Me.BunifuFlatButton1.IconMarginLeft = 0
        Me.BunifuFlatButton1.IconMarginRight = 0
        Me.BunifuFlatButton1.IconRightVisible = True
        Me.BunifuFlatButton1.IconRightZoom = 0R
        Me.BunifuFlatButton1.IconVisible = True
        Me.BunifuFlatButton1.IconZoom = 90.0R
        Me.BunifuFlatButton1.IsTab = False
        Me.BunifuFlatButton1.Location = New System.Drawing.Point(12, 534)
        Me.BunifuFlatButton1.Name = "BunifuFlatButton1"
        Me.BunifuFlatButton1.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.BunifuFlatButton1.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.BunifuFlatButton1.OnHoverTextColor = System.Drawing.Color.White
        Me.BunifuFlatButton1.selected = False
        Me.BunifuFlatButton1.Size = New System.Drawing.Size(104, 38)
        Me.BunifuFlatButton1.TabIndex = 44
        Me.BunifuFlatButton1.Text = "Ajout Initiale"
        Me.BunifuFlatButton1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BunifuFlatButton1.Textcolor = System.Drawing.Color.White
        Me.BunifuFlatButton1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'PriceLabel
        '
        Me.PriceLabel.AutoSize = True
        Me.PriceLabel.Location = New System.Drawing.Point(242, 369)
        Me.PriceLabel.Name = "PriceLabel"
        Me.PriceLabel.Size = New System.Drawing.Size(30, 13)
        Me.PriceLabel.TabIndex = 45
        Me.PriceLabel.Text = "Prix :"
        '
        'SubInitialQuantity
        '
        Me.SubInitialQuantity.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.SubInitialQuantity.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Cancel
        Me.SubInitialQuantity.ImageActive = Nothing
        Me.SubInitialQuantity.Location = New System.Drawing.Point(174, 618)
        Me.SubInitialQuantity.Name = "SubInitialQuantity"
        Me.SubInitialQuantity.Size = New System.Drawing.Size(25, 25)
        Me.SubInitialQuantity.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.SubInitialQuantity.TabIndex = 48
        Me.SubInitialQuantity.TabStop = False
        Me.SubInitialQuantity.Zoom = 10
        '
        'AddInitialQuantity
        '
        Me.AddInitialQuantity.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.AddInitialQuantity.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Add
        Me.AddInitialQuantity.ImageActive = Nothing
        Me.AddInitialQuantity.Location = New System.Drawing.Point(267, 618)
        Me.AddInitialQuantity.Name = "AddInitialQuantity"
        Me.AddInitialQuantity.Size = New System.Drawing.Size(25, 25)
        Me.AddInitialQuantity.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.AddInitialQuantity.TabIndex = 47
        Me.AddInitialQuantity.TabStop = False
        Me.AddInitialQuantity.Zoom = 10
        '
        'InitialQuantity
        '
        Me.InitialQuantity.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.InitialQuantity.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.InitialQuantity.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.InitialQuantity.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.InitialQuantity.BorderThickness = 3
        Me.InitialQuantity.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.InitialQuantity.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.InitialQuantity.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.InitialQuantity.isPassword = False
        Me.InitialQuantity.Location = New System.Drawing.Point(206, 608)
        Me.InitialQuantity.Margin = New System.Windows.Forms.Padding(4)
        Me.InitialQuantity.Name = "InitialQuantity"
        Me.InitialQuantity.Size = New System.Drawing.Size(54, 44)
        Me.InitialQuantity.TabIndex = 46
        Me.InitialQuantity.Text = "1"
        Me.InitialQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BunifuCustomLabel6
        '
        Me.BunifuCustomLabel6.AutoSize = True
        Me.BunifuCustomLabel6.Location = New System.Drawing.Point(43, 626)
        Me.BunifuCustomLabel6.Name = "BunifuCustomLabel6"
        Me.BunifuCustomLabel6.Size = New System.Drawing.Size(84, 13)
        Me.BunifuCustomLabel6.TabIndex = 49
        Me.BunifuCustomLabel6.Text = "Quantite Intiale :"
        '
        'BunifuImageButton3
        '
        Me.BunifuImageButton3.BackColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.BunifuImageButton3.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Cancel
        Me.BunifuImageButton3.ImageActive = Nothing
        Me.BunifuImageButton3.Location = New System.Drawing.Point(303, 616)
        Me.BunifuImageButton3.Name = "BunifuImageButton3"
        Me.BunifuImageButton3.Size = New System.Drawing.Size(32, 30)
        Me.BunifuImageButton3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.BunifuImageButton3.TabIndex = 50
        Me.BunifuImageButton3.TabStop = False
        Me.BunifuImageButton3.Zoom = 10
        '
        'BunifuElipse1
        '
        Me.BunifuElipse1.ElipseRadius = 15
        Me.BunifuElipse1.TargetControl = Me.AddAnotherArticleButton
        '
        'BunifuElipse2
        '
        Me.BunifuElipse2.ElipseRadius = 15
        Me.BunifuElipse2.TargetControl = Me.BunifuFlatButton1
        '
        'BunifuElipse3
        '
        Me.BunifuElipse3.ElipseRadius = 15
        Me.BunifuElipse3.TargetControl = Me.AddArticleButton
        '
        'BunifuElipse4
        '
        Me.BunifuElipse4.ElipseRadius = 25
        Me.BunifuElipse4.TargetControl = Me.AddArticle_Nom_TXB
        '
        'AddArticle
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(362, 600)
        Me.Controls.Add(Me.BunifuImageButton3)
        Me.Controls.Add(Me.BunifuCustomLabel6)
        Me.Controls.Add(Me.SubInitialQuantity)
        Me.Controls.Add(Me.AddInitialQuantity)
        Me.Controls.Add(Me.InitialQuantity)
        Me.Controls.Add(Me.PriceLabel)
        Me.Controls.Add(Me.BunifuFlatButton1)
        Me.Controls.Add(Me.Reset1)
        Me.Controls.Add(Me.Reset2)
        Me.Controls.Add(Me.Reset3)
        Me.Controls.Add(Me.SupplierPrice3)
        Me.Controls.Add(Me.ListSuppliers3)
        Me.Controls.Add(Me.SupplierPrice2)
        Me.Controls.Add(Me.ListSuppliers2)
        Me.Controls.Add(Me.SupplierPrice1)
        Me.Controls.Add(Me.BunifuCustomLabel5)
        Me.Controls.Add(Me.ListSuppliers1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.BunifuCustomLabel2)
        Me.Controls.Add(Me.BunifuCustomLabel1)
        Me.Controls.Add(Me.SubAlerteMax)
        Me.Controls.Add(Me.AddAlerteMax)
        Me.Controls.Add(Me.Alerte_Max)
        Me.Controls.Add(Me.AddArticle_Nom_TXB)
        Me.Controls.Add(Me.BunifuCustomLabel3)
        Me.Controls.Add(Me.BunifuImageButton1)
        Me.Controls.Add(Me.AddAnotherArticleButton)
        Me.Controls.Add(Me.AddArticleButton)
        Me.Controls.Add(Me.SubAlerteMin)
        Me.Controls.Add(Me.AddAlerteMin)
        Me.Controls.Add(Me.Orders_Unite_CBB)
        Me.Controls.Add(Me.Alerte_Min)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "AddArticle"
        Me.Text = "AddArticle"
        CType(Me.BunifuImageButton1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SubAlerteMin, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AddAlerteMin, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SubAlerteMax, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AddAlerteMax, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.BunifuImageButton2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Reset3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Reset2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Reset1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SubInitialQuantity, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AddInitialQuantity, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BunifuImageButton3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents BunifuCustomLabel3 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents BunifuImageButton1 As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents AddAnotherArticleButton As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents AddArticleButton As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents SubAlerteMin As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents AddAlerteMin As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents Orders_Unite_CBB As ComboBox
    Friend WithEvents Alerte_Min As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents AddArticle_Nom_TXB As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents SubAlerteMax As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents AddAlerteMax As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents Alerte_Max As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents BunifuCustomLabel1 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents BunifuCustomLabel2 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents BunifuCustomLabel4 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents BunifuImageButton2 As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents BunifuCustomLabel5 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents ListSuppliers1 As ComboBox
    Friend WithEvents SupplierPrice1 As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents SupplierPrice2 As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents ListSuppliers2 As ComboBox
    Friend WithEvents SupplierPrice3 As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents ListSuppliers3 As ComboBox
    Friend WithEvents Reset3 As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents Reset2 As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents Reset1 As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents BunifuDragControl1 As Bunifu.Framework.UI.BunifuDragControl
    Friend WithEvents BunifuFlatButton1 As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents PriceLabel As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents SubInitialQuantity As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents AddInitialQuantity As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents InitialQuantity As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents BunifuCustomLabel6 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents BunifuImageButton3 As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents BunifuElipse1 As Bunifu.Framework.UI.BunifuElipse
    Friend WithEvents BunifuElipse2 As Bunifu.Framework.UI.BunifuElipse
    Friend WithEvents BunifuElipse3 As Bunifu.Framework.UI.BunifuElipse
    Friend WithEvents BunifuElipse4 As Bunifu.Framework.UI.BunifuElipse
End Class
