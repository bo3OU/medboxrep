﻿Imports System.Convert

Public Class AddArticle

    Private FormLoad As Boolean = False
    Private Univect As New Dictionary(Of Integer, Integer)
    Private i As Integer
    Private id As Integer
    Private AlertMin As Integer
    Private AlertMax As Integer
    Private VectSup As New Dictionary(Of Integer, Integer)

    'For modifying an article
    Private IdUnite As Integer = -1
    Private IdModify As Integer = -1
    Private IdFourn() As Integer = {-1, -1, -1}
    Private Min As Integer = -1
    Private Max As Integer = -1
    Private nameModify As String = ""

    Private QuantiteInitiale As Integer = 0
    Private WithInitialQuantity As Boolean = False

    Private Sub AddArticle_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Size = New Size(Size.Width, 600)
        FormLoad = True
    End Sub

    Private Sub AddArticle_Nom_TXB_Enter(sender As Object, e As EventArgs) Handles AddArticle_Nom_TXB.Enter
        If Trim(AddArticle_Nom_TXB.Text) = "Nom de l'article" Then
            AddArticle_Nom_TXB.Text = ""
        End If
    End Sub

    Private Sub AddArticle_Nom_TXB_Leave(sender As Object, e As EventArgs) Handles AddArticle_Nom_TXB.Leave
        'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
        If Trim(AddArticle_Nom_TXB.Text) = "" Then
            AddArticle_Nom_TXB.Text = "Nom de l'article"
        End If
    End Sub

    Public Sub fill()
        'TODO FIGURE OUT ALL UNITIES OR UNITIES OF ALREADY CREATED ARTICLES
        ClearAll()
        Access.AddParam("@cabinet", SESSION.GetCabinet())
        Access.ExecQuery("select 
                        UNITE.UN_ID_UNITE as id,
                        UNITE.UN_NOM as line
                        from UNITE
                        order BY UNITE.UN_NOM asc")
        If NotEmpty(Access.Exception) Then MsgBox(Access.Exception) : Exit Sub
        i = 0
        For Each r As DataRow In Access.DBDT.Rows
            Orders_Unite_CBB.Items.Add(r.Item("line").ToString())
            Univect.Add(i, r.Item("id"))
            i = i + 1
        Next
        FillSuppliersList()
    End Sub

    Public Function FillToModify(id As Integer)
        IdModify = id
        fill()

        Access.ExecQuery("select DISTINCT fourni.FO_ID_FOURNI as id,
                    fourni.FO_PRIX as price,
                    article.AR_NOM as name,
                    fourni.FO_FOURNISSEUR as supplierid,
                    fournisseur.FRS_NOM & ' ' & fournisseur.FRS_PRENOM as frs,
                    UN_ID_UNITE as unite, 
                    article.AR_ALERTE_MAX as maax,
                    article.AR_ALERTE_MIN as miin
                    from (((fourni 
                    LEFT JOIN Fournisseur ON fournisseur.FRS_ID_FOURNISSEUR = fourni.FO_FOURNISSEUR)
                    LEFT JOIN ARTICLE ON ARTICLE.AR_ID_ARTICLE = fourni.FO_ARTICLE)
                    LEFT JOIN unite ON unite.UN_ID_UNITE = article.AR_UNITE)
                    where article.AR_ID_ARTICLE = " & IdModify & " and FO_FOURNISSEUR IS NOT NULL")
        If NotEmpty(Access.Exception) Then
            MsgBox(Access.Exception)
            Return 0
        End If
        i = 0
        For Each r As DataRow In Access.DBDT.Rows
            If Not Access.DBDT.Rows(i).Item("supplierid").Equals(DBNull.Value) Then
                IdFourn(i) = Access.DBDT.Rows(i).Item("supplierid")
                i += 1
            Else
                Return 0
            End If
        Next
        AddArticle_Nom_TXB.Text = Access.DBDT.Rows(0).Item("name")
        nameModify = Access.DBDT.Rows(0).Item("name")

        Alerte_Min.Text = Access.DBDT.Rows(0).Item("miin")
        Min = Access.DBDT.Rows(0).Item("miin")

        Alerte_Max.Text = Access.DBDT.Rows(0).Item("maax")
        Max = Access.DBDT.Rows(0).Item("maax")
        AddArticleButton.Text = "Annuler"
        AddAnotherArticleButton.Text = "Modifier"

        Orders_Unite_CBB.SelectedIndex = FirstValue(Univect, Access.DBDT.Rows(0).Item("unite"))
        i = 1
        For Each r In IdFourn
            If r <> -1 Then
                MsgBox("index of supplier = " & FirstValue(VectSup, r))
                CType(Controls("ListSuppliers" & i), ComboBox).Visible = True
                CType(Controls("SupplierPrice" & i), Bunifu.Framework.UI.BunifuMetroTextbox).Visible = True
                CType(Controls("Reset" & i), Bunifu.Framework.UI.BunifuImageButton).Visible = True
                CType(Controls("ListSuppliers" & i), ComboBox).SelectedIndex = FirstValue(VectSup, r)
                CType(Controls("SupplierPrice" & i), Bunifu.Framework.UI.BunifuMetroTextbox).Text = Access.DBDT.Rows(i - 1).Item("price")
                i += 1
            End If
            If i < 3 Then
                CType(Controls("ListSuppliers" & i), ComboBox).Visible = True
                CType(Controls("SupplierPrice" & i), Bunifu.Framework.UI.BunifuMetroTextbox).Visible = True
                CType(Controls("Reset" & i), Bunifu.Framework.UI.BunifuImageButton).Visible = True
            End If
        Next
        i = 1
        For Each r In IdFourn
            MsgBox("index : " & CType(Controls("ListSuppliers" & i), ComboBox).SelectedIndex)
            i += 1
        Next

        Return 1
    End Function

    Private Sub ClearAll()

        BunifuFlatButton1.Visible = True
        Univect.Clear()
        Orders_Unite_CBB.Items.Clear()
        Orders_Unite_CBB.SelectedIndex = -1
        ListSuppliers1.SelectedIndex = -1
        ListSuppliers2.SelectedIndex = -1
        ListSuppliers3.SelectedIndex = -1
    End Sub

    Private Function AddArticle()
        Dim ArticleId As Integer
        If Trim(AddArticle_Nom_TXB.Text) <> "" And Trim(AddArticle_Nom_TXB.Text) <> "Nom de l'article" Then
            If Orders_Unite_CBB.SelectedIndex <> -1 Then
                If Int(Alerte_Max.Text) > Int(Alerte_Min.Text) Then
                    If ListSuppliers1.SelectedIndex <> -1 Or ListSuppliers1.SelectedIndex <> -1 Or ListSuppliers1.SelectedIndex <> -1 Then
                        If ListSuppliers1.SelectedIndex <> -1 And ListSuppliers1.SelectedIndex <> ListSuppliers2.SelectedIndex And ListSuppliers1.SelectedIndex <> ListSuppliers3.SelectedIndex Then
                            If ListSuppliers2.SelectedIndex = -1 Or ListSuppliers3.SelectedIndex = -1 Or ListSuppliers2.SelectedIndex <> ListSuppliers3.SelectedIndex Then
                                'TODO CHECK IF NAME IS UNIQUE !!
                                Access.AddParam("@name", Trim(AddArticle_Nom_TXB.Text))
                                Access.ExecQuery("select * from Fourni 
                                                    LEFT JOIN article ON article.AR_ID_article = fourni.FO_article
                                                    where article.AR_NOM = @name and fourni.FO_CABINET = " & SESSION.GetCabinet())
                                If NotEmpty(Access.Exception) Then
                                    MsgBox(Access.Exception)
                                Else
                                    If Access.DBDT.Rows.Count < 1 Then

                                        Access.AddParam("@name", Trim(AddArticle_Nom_TXB.Text))
                                        Access.AddParam("@unite", Univect(Orders_Unite_CBB.SelectedIndex))
                                        Access.AddParam("@min", Int(Trim(Alerte_Min.Text)))
                                        Access.AddParam("@max", Int(Trim(Alerte_Max.Text)))
                                        Access.InsertQuery("insert into ARTICLE ([AR_NOM],[AR_UNITE],[AR_ALERTE_MIN],[AR_ALERTE_MAX]) values(@name,@unite,@min,@max)")
                                        ArticleId = Access.ID
                                        If NotEmpty(Access.Exception) Then
                                            MsgBox(Access.Exception)
                                        End If
                                        Access.InsertQuery("insert into FOURNI ([FO_FOURNISSEUR],[FO_ARTICLE],[FO_CABINET],[FO_PRIX]) values (NULL," & ArticleId & "," & SESSION.GetCabinet() & ",0)")
                                        'INSERTING


                                        Dim InsertQuery As String = "insert into FOURNI ([FO_FOURNISSEUR],[FO_ARTICLE],[FO_CABINET],[FO_PRIX]) values "

                                        If ListSuppliers1.SelectedIndex <> -1 Then
                                            'TODO CHECK IF SUPPLIER ALREADY BRINGS THAT ITEM ..
                                            Access.AddParam("@first", SupplierPrice1.Text)
                                            'MsgBox(InsertQuery)
                                            Access.ExecQuery(InsertQuery & "(" & VectSup(ListSuppliers1.SelectedIndex) & "," & ArticleId & "," & SESSION.GetCabinet() & ",@first)")
                                            If NotEmpty(Access.Exception) Then
                                                MsgBox(Access.Exception)
                                            End If
                                        End If
                                        'second
                                        If ListSuppliers2.SelectedIndex <> -1 Then
                                            If ListSuppliers2.SelectedIndex <> ListSuppliers1.SelectedIndex Then
                                                Access.AddParam("@second", SupplierPrice2.Text)
                                                'MsgBox(InsertQuery)
                                                Access.ExecQuery(InsertQuery & "(" & VectSup(ListSuppliers2.SelectedIndex) & "," & ArticleId & "," & SESSION.GetCabinet() & ",@second)")
                                                If NotEmpty(Access.Exception) Then
                                                    MsgBox(Access.Exception)
                                                End If
                                            End If
                                        End If
                                        'third
                                        If ListSuppliers3.SelectedIndex <> -1 Then
                                            If ListSuppliers3.SelectedIndex <> ListSuppliers2.SelectedIndex And ListSuppliers3.SelectedIndex <> ListSuppliers1.SelectedIndex Then
                                                Access.AddParam("@third", SupplierPrice3.Text)
                                                'MsgBox(InsertQuery)
                                                Access.ExecQuery(InsertQuery & "(" & VectSup(ListSuppliers3.SelectedIndex) & "," & ArticleId & "," & SESSION.GetCabinet() & ",@third)")
                                                If NotEmpty(Access.Exception) Then
                                                    MsgBox(Access.Exception)
                                                End If
                                            End If
                                        End If

                                        'INSERT INTO STOCK IF WithInitialQuantity IS TRUE !!!!
                                        If WithInitialQuantity Then
                                            Access.AddParam("quantite", InitialQuantity.Text)
                                            Access.ExecQuery("insert into STOCK (ST_ARTICLE, ST_CABINET, ST_QUANTITE) values(" & ArticleId & "," & SESSION.GetCabinet() & ", @quantite)")
                                        End If

                                        MainForm.BringIndicator(MainForm.SubMenuButton_4_BTN, New EventArgs)
                                        Return 1
                                    Else
                                        MsgBox("nom deja existe")
                                    End If

                                End If
                            Else
                                MsgBox("Vous avez choisi un meme fournisseur! first !")
                            End If
                        Else
                            MsgBox("Vous avez choisi un meme fournisseur! second !")
                        End If
                    Else
                        MsgBox("article needs a supplier")

                    End If
                Else
                    MsgBox("max > min ! ")
                End If
            Else
                MsgBox("selectionnez unite !")

            End If
        Else
            MsgBox("nom de l'article !")

                                    End If
        Return 0
    End Function

    Private Sub FillSuppliersList()
        ListSuppliers1.SelectedIndex = -1
        ListSuppliers2.SelectedIndex = -1
        ListSuppliers3.SelectedIndex = -1
        VectSup.Clear()
        Access.ExecQuery("select 
        fournisseur.FRS_ID_FOURNISSEUR as id,
        fournisseur.FRS_NOM & ' ' & fournisseur.FRS_PRENOM as line
        from (fourni 
        left join fournisseur on fournisseur.FRS_ID_FOURNISSEUR = fourni.FO_fournisseur)
        where FO_ARTICLE IS NULL and FO_CABINET = " & SESSION.GetCabinet())
        If NotEmpty(Access.Exception) Then MsgBox(Access.Exception) : Exit Sub
        Dim i As Integer = 0
        For Each r As DataRow In Access.DBDT.Rows
            If Not (r.Item("line").Equals(DBNull.Value) Or r.Item("id").Equals(DBNull.Value)) Then
                VectSup.Add(i, r.Item("id"))
                ListSuppliers1.Items.Add(r.Item("line"))
                ListSuppliers2.Items.Add(r.Item("line"))
                ListSuppliers3.Items.Add(r.Item("line"))
                i += 1
            End If
        Next r
    End Sub

    Private Sub AddAlerteMin_Click(sender As Object, e As EventArgs) Handles AddAlerteMin.Click
        Alerte_Min.Text = Alerte_Min.Text + 1
    End Sub

    Private Sub SubAlerteMin_Click(sender As Object, e As EventArgs) Handles SubAlerteMin.Click
        Alerte_Min.Text = Alerte_Min.Text - 1
    End Sub

    Private Sub AddAlerteMax_Click(sender As Object, e As EventArgs) Handles AddAlerteMax.Click
        Alerte_Max.Text = Alerte_Max.Text + 1
    End Sub

    Private Sub SubAlerteMax_Click(sender As Object, e As EventArgs) Handles SubAlerteMax.Click
        Alerte_Max.Text = Alerte_Max.Text - 1
    End Sub

    Private Sub Alerte_Min_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Alerte_Min.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Or Alerte_Min.Text.Length > 4 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub Alerte_Max_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Alerte_Max.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Or Alerte_Max.Text.Length > 4 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub Alerte_Min_OnValueChanged(sender As Object, e As EventArgs) Handles Alerte_Min.OnValueChanged
        If String.IsNullOrWhiteSpace(Alerte_Min.Text) Then
            Alerte_Min.Text = 1
        ElseIf (Int(Alerte_Min.Text)) < 1 Then
            Alerte_Min.Text = 1
        End If
        AlertMin = Alerte_Min.Text
    End Sub

    Private Sub Alerte_Max_OnValueChanged(sender As Object, e As EventArgs) Handles Alerte_Max.OnValueChanged
        If String.IsNullOrWhiteSpace(Alerte_Max.Text) Then
            Alerte_Max.Text = 1
        ElseIf (Int(Alerte_Max.Text)) < 1 Then
            Alerte_Max.Text = 1
        End If
        AlertMax = Alerte_Max.Text
    End Sub

    Private Sub AddAnotherArticleButton_Click(sender As Object, e As EventArgs) Handles AddAnotherArticleButton.Click
        If IdModify = -1 Then
            If AddArticle() = 1 Then
                ClearAll()
                fill()
                MainForm.BringIndicator(MainForm.SubMenuButton_4_BTN, New EventArgs)
                ClearAll()
            End If
        Else
            'TODO modify
            Access.AddParam("@name", Trim(AddArticle_Nom_TXB.Text))
            Access.AddParam("@min", Alerte_Min.Text)
            Access.AddParam("@max", Alerte_Max.Text)
            Access.AddParam("@unite", Univect(Orders_Unite_CBB.SelectedIndex))
            Access.ExecQuery("UPDATE article 
                            set 
                            AR_NOM = @name,
                            AR_ALERTE_MIN=@min,
                            AR_ALERTE_MAX=@max,
                            AR_UNITE = @unite
                            where article.AR_ID_ARTICLE = " & IdModify)
            If NotEmpty(Access.Exception) Then
                MsgBox(Access.Exception)
            End If
            MainForm.BringIndicator(MainForm.SubMenuButton_4_BTN, New EventArgs)
            Close()
        End If
    End Sub

    Private Sub AddArticleButton_Click(sender As Object, e As EventArgs) Handles AddArticleButton.Click
        If IdModify = -1 Then
            If AddArticle() = 1 Then
                MainForm.BringIndicator(MainForm.SubMenuButton_4_BTN, New EventArgs)
                Close()
            End If
        Else
            'TODO Annuler
            Close()
        End If
    End Sub

    Private Sub BunifuImageButton2_Click(sender As Object, e As EventArgs) Handles BunifuImageButton2.Click
        Close()
    End Sub

    Private Sub SupplierPrice1_OnValueChanged(sender As Object, e As EventArgs) Handles SupplierPrice1.OnValueChanged
        If String.IsNullOrWhiteSpace(SupplierPrice1.Text) Then
            SupplierPrice1.Text = 1
        ElseIf (ToSingle(SupplierPrice1.Text)) < 1 Then
            SupplierPrice1.Text = 1
        End If
    End Sub

    Private Sub SupplierPrice2_OnValueChanged(sender As Object, e As EventArgs) Handles SupplierPrice2.OnValueChanged
        If String.IsNullOrWhiteSpace(SupplierPrice2.Text) Then
            SupplierPrice2.Text = 1
        ElseIf (Int(SupplierPrice2.Text)) < 1 Then
            SupplierPrice2.Text = 1
        End If
    End Sub

    Private Sub SupplierPrice3_OnValueChanged(sender As Object, e As EventArgs) Handles SupplierPrice3.OnValueChanged
        If String.IsNullOrWhiteSpace(SupplierPrice3.Text) Then
            SupplierPrice3.Text = 1
        ElseIf (Int(SupplierPrice3.Text)) < 1 Then
            SupplierPrice3.Text = 1
        End If
    End Sub

    Private Sub ListSuppliers1_SelectedValueChanged(sender As Object, e As EventArgs) Handles ListSuppliers1.SelectedValueChanged
        If FormLoad Then
            ListSuppliers2.Visible = True
            SupplierPrice2.Visible = True
            Reset2.Visible = True
        End If
    End Sub

    Private Sub ListSuppliers2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListSuppliers2.SelectedIndexChanged
        If FormLoad Then
            ListSuppliers3.Visible = True
            SupplierPrice3.Visible = True
            Reset3.Visible = True
        End If
    End Sub

    Private Sub ResetFirst_Click(sender As Object, e As EventArgs) Handles Reset1.Click
        ListSuppliers1.SelectedIndex = -1
        SupplierPrice1.Text = 1

        If ListSuppliers3.SelectedIndex = -1 Then
            If ListSuppliers2.SelectedIndex = -1 Then
                ListSuppliers2.Visible = False
                SupplierPrice2.Visible = False
                Reset2.Visible = False

                ListSuppliers3.Visible = False
                SupplierPrice3.Visible = False
                Reset3.Visible = False
            End If
        End If
    End Sub

    Private Sub ResetSecond_Click(sender As Object, e As EventArgs) Handles Reset2.Click
        ListSuppliers2.SelectedIndex = -1
        SupplierPrice2.Text = 1

        If ListSuppliers3.SelectedIndex = -1 Then
            If ListSuppliers1.SelectedIndex = -1 Then
                ListSuppliers2.Visible = False
                SupplierPrice2.Visible = False
                Reset2.Visible = False
            End If
            ListSuppliers3.SelectedIndex = -1
            SupplierPrice3.Text = 1
            ListSuppliers3.Visible = False
            SupplierPrice3.Visible = False
            Reset3.Visible = False
        End If
    End Sub

    Private Sub ResetTthird_Click(sender As Object, e As EventArgs) Handles Reset3.Click
        ListSuppliers3.SelectedIndex = -1
        SupplierPrice3.Text = 1
        If ListSuppliers2.SelectedIndex = -1 Then
            ListSuppliers3.Visible = False
            SupplierPrice3.Visible = False
            Reset3.Visible = False
            If ListSuppliers1.SelectedIndex = -1 Then
                ListSuppliers2.Visible = False
                SupplierPrice2.Visible = False
                Reset2.Visible = False
            End If
        End If
    End Sub

    Private Sub SupplierPrice1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles SupplierPrice1.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Or SupplierPrice1.Text.Length > 4 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub SupplierPrice2_KeyPress(sender As Object, e As KeyPressEventArgs) Handles SupplierPrice2.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Or SupplierPrice2.Text.Length > 4 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub SupplierPrice3_KeyPress(sender As Object, e As KeyPressEventArgs) Handles SupplierPrice3.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Or SupplierPrice3.Text.Length > 4 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub BunifuFlatButton1_Click(sender As Object, e As EventArgs) Handles BunifuFlatButton1.Click
        Size = New Size(Size.Width, Size.Height + 70)
        BunifuFlatButton1.Visible = False
        WithInitialQuantity = True
    End Sub

    Private Sub AddInitialQuantity_Click(sender As Object, e As EventArgs) Handles AddInitialQuantity.Click
        InitialQuantity.Text = InitialQuantity.Text + 1
    End Sub

    Private Sub SubInitialQuantity_Click(sender As Object, e As EventArgs) Handles SubInitialQuantity.Click
        InitialQuantity.Text = InitialQuantity.Text - 1
    End Sub

    Private Sub InitialQuantity_KeyPress(sender As Object, e As KeyPressEventArgs) Handles InitialQuantity.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Or InitialQuantity.Text.Length > 4 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub InitialQuantity_OnValueChanged(sender As Object, e As EventArgs) Handles InitialQuantity.OnValueChanged
        If String.IsNullOrWhiteSpace(InitialQuantity.Text) Then
            InitialQuantity.Text = 1
        ElseIf (Int(InitialQuantity.Text)) < 1 Then
            InitialQuantity.Text = 1
        End If
        QuantiteInitiale = InitialQuantity.Text
    End Sub

    Private Sub BunifuImageButton3_Click(sender As Object, e As EventArgs) Handles BunifuImageButton3.Click
        WithInitialQuantity = False
        BunifuFlatButton1.Visible = True
        Size = New Size(Size.Width, Size.Height - 70)
    End Sub
End Class