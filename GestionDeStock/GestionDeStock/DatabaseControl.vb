﻿'THIS IS //TODO
Imports System.Data.OleDb
Imports System.IO.Directory
Public Class DatabaseControl


    ' CREATE YOUR DB CONNECTION
    Private DBCon As New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;" &
                                         "Data Source=" & Application.StartupPath & "\MedBoxUpdatedDatabase.accdb")
    'F:\Ali 2- home repo\GestionDeStock\MedBoxUpdatedDatabase.accdb")

    ' PREPARE DB COMMAND
    Private DBCmd As OleDbCommand

    ' DB DATA
    Public DBDA As OleDbDataAdapter
    Public DBDT As DataTable

    'FOR @@IDENTITY
    ' DB DATA
    Public DBDA2 As OleDbDataAdapter
    Public DBDT2 As DataTable
    ' PREPARE DB COMMAND
    Private DBCmd2 As OleDbCommand
    Private DBTrans As OleDbTransaction
    Public ID As Integer

    ' QUERY PARAMETERS
    Public Params As New List(Of OleDbParameter)

    ' QUERY STATISTICS
    Public RecordCount As Integer
    Public Exception As String

    Public Sub InsertQuery(Query As String)
        ' RESET QUERY STATS
        RecordCount = 0
        Exception = ""

        Try
            ID = -1
            ' OPEN A CONNECTION
            DBCon.Open()

            ' CREATE DB COMMAND
            DBCmd = New OleDbCommand(Query, DBCon)

            ' LOAD PARAMS INTO DB COMMAND
            Params.ForEach(Sub(p) DBCmd.Parameters.Add(p))

            ' CLEAR PARAMS LIST
            Params.Clear()

            ' EXECUTE COMMAND & FILL DATATABLE
            DBDT = New DataTable
            DBDA = New OleDbDataAdapter(DBCmd)
            RecordCount = DBDA.Fill(DBDT)

            'SECOND QUERY

            ' CREATE DB COMMAND
            DBCmd2 = New OleDbCommand("SELECT @@IDENTITY as id", DBCon)
            ' EXECUTE COMMAND & FILL DATATABLE
            DBDT2 = New DataTable
            ' BDDD = New DataView
            DBDA2 = New OleDbDataAdapter(DBCmd2)
            RecordCount = DBDA2.Fill(DBDT2)

            ' CLEAR PARAMS LIST

            ' GET THE FIRST ITEM IN FIRST ROW
            ID = DBDT2.Rows(0).Item("id")

        Catch ex As Exception
            Exception = ex.Message
        End Try

        ' CLOSE YOUR CONNECTION
        If DBCon.State = ConnectionState.Open Then DBCon.Close()
    End Sub

    Public Sub ExecQuery(Query As String)
        ' RESET QUERY STATS
        RecordCount = 0
        Exception = ""

        Try
            ' OPEN A CONNECTION
            DBCon.Open()

            ' CREATE DB COMMAND
            DBCmd = New OleDbCommand(Query, DBCon)

            ' LOAD PARAMS INTO DB COMMAND
            Params.ForEach(Sub(p) DBCmd.Parameters.Add(p))

            ' CLEAR PARAMS LIST
            Params.Clear()

            ' EXECUTE COMMAND & FILL DATATABLE
            DBDT = New DataTable
            DBDA = New OleDbDataAdapter(DBCmd)
            RecordCount = DBDA.Fill(DBDT)
        Catch ex As Exception
            Exception = ex.Message
        End Try

        ' CLOSE YOUR CONNECTION
        If DBCon.State = ConnectionState.Open Then DBCon.Close()
    End Sub

    ' INCLUDE QUERY & COMMAND PARAMETERS
    Public Sub AddParam(Name As String, Value As Object)
        Dim NewParam As New OleDbParameter(Name, Value)
        Params.Add(NewParam)
    End Sub
End Class
