﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class AddOrder
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AddOrder))
        Me.Order_Quantite = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.Orders_Nom_CBB = New System.Windows.Forms.ComboBox()
        Me.AddQuantityNumber = New Bunifu.Framework.UI.BunifuImageButton()
        Me.RemoveQuantityNumber = New Bunifu.Framework.UI.BunifuImageButton()
        Me.Orders_Fournisseur_CBB = New System.Windows.Forms.ComboBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CloseButton = New Bunifu.Framework.UI.BunifuImageButton()
        Me.BunifuCustomLabel1 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.AddOrderButton = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.AddAnotherOrderButton = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.BunifuImageButton1 = New Bunifu.Framework.UI.BunifuImageButton()
        Me.TotalPrice_LBL = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.BunifuCustomLabel2 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.BunifuCustomLabel3 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.ClearAndFillFrsAndArt = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.BunifuDragControl1 = New Bunifu.Framework.UI.BunifuDragControl(Me.components)
        CType(Me.AddQuantityNumber, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RemoveQuantityNumber, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.CloseButton, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BunifuImageButton1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Order_Quantite
        '
        Me.Order_Quantite.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Order_Quantite.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Order_Quantite.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Order_Quantite.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Order_Quantite.BorderThickness = 3
        Me.Order_Quantite.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Order_Quantite.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Order_Quantite.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.Order_Quantite.isPassword = False
        Me.Order_Quantite.Location = New System.Drawing.Point(147, 292)
        Me.Order_Quantite.Margin = New System.Windows.Forms.Padding(4)
        Me.Order_Quantite.Name = "Order_Quantite"
        Me.Order_Quantite.Size = New System.Drawing.Size(54, 44)
        Me.Order_Quantite.TabIndex = 3
        Me.Order_Quantite.Text = "1"
        Me.Order_Quantite.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Orders_Nom_CBB
        '
        Me.Orders_Nom_CBB.Enabled = False
        Me.Orders_Nom_CBB.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Orders_Nom_CBB.FormattingEnabled = True
        Me.Orders_Nom_CBB.ItemHeight = 22
        Me.Orders_Nom_CBB.Location = New System.Drawing.Point(66, 193)
        Me.Orders_Nom_CBB.Name = "Orders_Nom_CBB"
        Me.Orders_Nom_CBB.Size = New System.Drawing.Size(217, 30)
        Me.Orders_Nom_CBB.TabIndex = 5
        '
        'AddQuantityNumber
        '
        Me.AddQuantityNumber.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.AddQuantityNumber.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Add
        Me.AddQuantityNumber.ImageActive = Nothing
        Me.AddQuantityNumber.Location = New System.Drawing.Point(208, 302)
        Me.AddQuantityNumber.Name = "AddQuantityNumber"
        Me.AddQuantityNumber.Size = New System.Drawing.Size(25, 25)
        Me.AddQuantityNumber.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.AddQuantityNumber.TabIndex = 6
        Me.AddQuantityNumber.TabStop = False
        Me.AddQuantityNumber.Zoom = 10
        '
        'RemoveQuantityNumber
        '
        Me.RemoveQuantityNumber.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.RemoveQuantityNumber.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Cancel
        Me.RemoveQuantityNumber.ImageActive = Nothing
        Me.RemoveQuantityNumber.Location = New System.Drawing.Point(115, 302)
        Me.RemoveQuantityNumber.Name = "RemoveQuantityNumber"
        Me.RemoveQuantityNumber.Size = New System.Drawing.Size(25, 25)
        Me.RemoveQuantityNumber.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.RemoveQuantityNumber.TabIndex = 7
        Me.RemoveQuantityNumber.TabStop = False
        Me.RemoveQuantityNumber.Zoom = 10
        '
        'Orders_Fournisseur_CBB
        '
        Me.Orders_Fournisseur_CBB.BackColor = System.Drawing.SystemColors.Window
        Me.Orders_Fournisseur_CBB.Enabled = False
        Me.Orders_Fournisseur_CBB.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Orders_Fournisseur_CBB.FormattingEnabled = True
        Me.Orders_Fournisseur_CBB.ItemHeight = 22
        Me.Orders_Fournisseur_CBB.Location = New System.Drawing.Point(66, 125)
        Me.Orders_Fournisseur_CBB.Name = "Orders_Fournisseur_CBB"
        Me.Orders_Fournisseur_CBB.Size = New System.Drawing.Size(217, 30)
        Me.Orders_Fournisseur_CBB.TabIndex = 8
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Panel1.Controls.Add(Me.CloseButton)
        Me.Panel1.Controls.Add(Me.BunifuCustomLabel1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(360, 84)
        Me.Panel1.TabIndex = 9
        '
        'CloseButton
        '
        Me.CloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.CloseButton.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Delete
        Me.CloseButton.ImageActive = Nothing
        Me.CloseButton.Location = New System.Drawing.Point(330, 0)
        Me.CloseButton.Name = "CloseButton"
        Me.CloseButton.Size = New System.Drawing.Size(30, 27)
        Me.CloseButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.CloseButton.TabIndex = 16
        Me.CloseButton.TabStop = False
        Me.CloseButton.Zoom = 10
        '
        'BunifuCustomLabel1
        '
        Me.BunifuCustomLabel1.AutoSize = True
        Me.BunifuCustomLabel1.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel1.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.BunifuCustomLabel1.Location = New System.Drawing.Point(111, 30)
        Me.BunifuCustomLabel1.Name = "BunifuCustomLabel1"
        Me.BunifuCustomLabel1.Size = New System.Drawing.Size(144, 24)
        Me.BunifuCustomLabel1.TabIndex = 0
        Me.BunifuCustomLabel1.Text = "Commander"
        '
        'AddOrderButton
        '
        Me.AddOrderButton.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.AddOrderButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.AddOrderButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.AddOrderButton.BorderRadius = 0
        Me.AddOrderButton.ButtonText = "Ajouter et quitter"
        Me.AddOrderButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AddOrderButton.DisabledColor = System.Drawing.Color.Gray
        Me.AddOrderButton.Iconcolor = System.Drawing.Color.Transparent
        Me.AddOrderButton.Iconimage = CType(resources.GetObject("AddOrderButton.Iconimage"), System.Drawing.Image)
        Me.AddOrderButton.Iconimage_right = Nothing
        Me.AddOrderButton.Iconimage_right_Selected = Nothing
        Me.AddOrderButton.Iconimage_Selected = Nothing
        Me.AddOrderButton.IconMarginLeft = 0
        Me.AddOrderButton.IconMarginRight = 0
        Me.AddOrderButton.IconRightVisible = True
        Me.AddOrderButton.IconRightZoom = 0R
        Me.AddOrderButton.IconVisible = True
        Me.AddOrderButton.IconZoom = 90.0R
        Me.AddOrderButton.IsTab = False
        Me.AddOrderButton.Location = New System.Drawing.Point(190, 440)
        Me.AddOrderButton.Name = "AddOrderButton"
        Me.AddOrderButton.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.AddOrderButton.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.AddOrderButton.OnHoverTextColor = System.Drawing.Color.White
        Me.AddOrderButton.selected = False
        Me.AddOrderButton.Size = New System.Drawing.Size(158, 48)
        Me.AddOrderButton.TabIndex = 10
        Me.AddOrderButton.Text = "Ajouter et quitter"
        Me.AddOrderButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.AddOrderButton.Textcolor = System.Drawing.Color.White
        Me.AddOrderButton.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AddOrderButton.Visible = False
        '
        'AddAnotherOrderButton
        '
        Me.AddAnotherOrderButton.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.AddAnotherOrderButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.AddAnotherOrderButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.AddAnotherOrderButton.BorderRadius = 0
        Me.AddAnotherOrderButton.ButtonText = "ajouter et rester"
        Me.AddAnotherOrderButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AddAnotherOrderButton.DisabledColor = System.Drawing.Color.Gray
        Me.AddAnotherOrderButton.Iconcolor = System.Drawing.Color.Transparent
        Me.AddAnotherOrderButton.Iconimage = CType(resources.GetObject("AddAnotherOrderButton.Iconimage"), System.Drawing.Image)
        Me.AddAnotherOrderButton.Iconimage_right = Nothing
        Me.AddAnotherOrderButton.Iconimage_right_Selected = Nothing
        Me.AddAnotherOrderButton.Iconimage_Selected = Nothing
        Me.AddAnotherOrderButton.IconMarginLeft = 0
        Me.AddAnotherOrderButton.IconMarginRight = 0
        Me.AddAnotherOrderButton.IconRightVisible = True
        Me.AddAnotherOrderButton.IconRightZoom = 0R
        Me.AddAnotherOrderButton.IconVisible = True
        Me.AddAnotherOrderButton.IconZoom = 90.0R
        Me.AddAnotherOrderButton.IsTab = False
        Me.AddAnotherOrderButton.Location = New System.Drawing.Point(12, 440)
        Me.AddAnotherOrderButton.Name = "AddAnotherOrderButton"
        Me.AddAnotherOrderButton.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.AddAnotherOrderButton.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.AddAnotherOrderButton.OnHoverTextColor = System.Drawing.Color.White
        Me.AddAnotherOrderButton.selected = False
        Me.AddAnotherOrderButton.Size = New System.Drawing.Size(158, 48)
        Me.AddAnotherOrderButton.TabIndex = 11
        Me.AddAnotherOrderButton.Text = "ajouter et rester"
        Me.AddAnotherOrderButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.AddAnotherOrderButton.Textcolor = System.Drawing.Color.White
        Me.AddAnotherOrderButton.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AddAnotherOrderButton.Visible = False
        '
        'BunifuImageButton1
        '
        Me.BunifuImageButton1.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.BunifuImageButton1.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Refresh
        Me.BunifuImageButton1.ImageActive = Nothing
        Me.BunifuImageButton1.Location = New System.Drawing.Point(330, 90)
        Me.BunifuImageButton1.Name = "BunifuImageButton1"
        Me.BunifuImageButton1.Size = New System.Drawing.Size(21, 21)
        Me.BunifuImageButton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.BunifuImageButton1.TabIndex = 12
        Me.BunifuImageButton1.TabStop = False
        Me.BunifuImageButton1.Zoom = 10
        '
        'TotalPrice_LBL
        '
        Me.TotalPrice_LBL.AutoSize = True
        Me.TotalPrice_LBL.Location = New System.Drawing.Point(135, 378)
        Me.TotalPrice_LBL.Name = "TotalPrice_LBL"
        Me.TotalPrice_LBL.Size = New System.Drawing.Size(73, 13)
        Me.TotalPrice_LBL.TabIndex = 13
        Me.TotalPrice_LBL.Text = "Total : 0 MAD"
        '
        'BunifuCustomLabel2
        '
        Me.BunifuCustomLabel2.AutoSize = True
        Me.BunifuCustomLabel2.Location = New System.Drawing.Point(63, 109)
        Me.BunifuCustomLabel2.Name = "BunifuCustomLabel2"
        Me.BunifuCustomLabel2.Size = New System.Drawing.Size(72, 13)
        Me.BunifuCustomLabel2.TabIndex = 14
        Me.BunifuCustomLabel2.Text = "Fournisseurs :"
        '
        'BunifuCustomLabel3
        '
        Me.BunifuCustomLabel3.AutoSize = True
        Me.BunifuCustomLabel3.Location = New System.Drawing.Point(63, 177)
        Me.BunifuCustomLabel3.Name = "BunifuCustomLabel3"
        Me.BunifuCustomLabel3.Size = New System.Drawing.Size(47, 13)
        Me.BunifuCustomLabel3.TabIndex = 15
        Me.BunifuCustomLabel3.Text = "Articles :"
        '
        'ClearAndFillFrsAndArt
        '
        Me.ClearAndFillFrsAndArt.Activecolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.ClearAndFillFrsAndArt.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.ClearAndFillFrsAndArt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClearAndFillFrsAndArt.BorderRadius = 0
        Me.ClearAndFillFrsAndArt.ButtonText = "Modifier l'article ou le fournisseur"
        Me.ClearAndFillFrsAndArt.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ClearAndFillFrsAndArt.DisabledColor = System.Drawing.Color.Gray
        Me.ClearAndFillFrsAndArt.Iconcolor = System.Drawing.Color.Transparent
        Me.ClearAndFillFrsAndArt.Iconimage = Nothing
        Me.ClearAndFillFrsAndArt.Iconimage_right = Nothing
        Me.ClearAndFillFrsAndArt.Iconimage_right_Selected = Nothing
        Me.ClearAndFillFrsAndArt.Iconimage_Selected = Nothing
        Me.ClearAndFillFrsAndArt.IconMarginLeft = 0
        Me.ClearAndFillFrsAndArt.IconMarginRight = 0
        Me.ClearAndFillFrsAndArt.IconRightVisible = True
        Me.ClearAndFillFrsAndArt.IconRightZoom = 0R
        Me.ClearAndFillFrsAndArt.IconVisible = True
        Me.ClearAndFillFrsAndArt.IconZoom = 90.0R
        Me.ClearAndFillFrsAndArt.IsTab = False
        Me.ClearAndFillFrsAndArt.Location = New System.Drawing.Point(66, 240)
        Me.ClearAndFillFrsAndArt.Name = "ClearAndFillFrsAndArt"
        Me.ClearAndFillFrsAndArt.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.ClearAndFillFrsAndArt.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.ClearAndFillFrsAndArt.OnHoverTextColor = System.Drawing.Color.White
        Me.ClearAndFillFrsAndArt.selected = False
        Me.ClearAndFillFrsAndArt.Size = New System.Drawing.Size(217, 27)
        Me.ClearAndFillFrsAndArt.TabIndex = 16
        Me.ClearAndFillFrsAndArt.Text = "Modifier l'article ou le fournisseur"
        Me.ClearAndFillFrsAndArt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ClearAndFillFrsAndArt.Textcolor = System.Drawing.Color.White
        Me.ClearAndFillFrsAndArt.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClearAndFillFrsAndArt.Visible = False
        '
        'BunifuDragControl1
        '
        Me.BunifuDragControl1.Fixed = True
        Me.BunifuDragControl1.Horizontal = True
        Me.BunifuDragControl1.TargetControl = Me.Panel1
        Me.BunifuDragControl1.Vertical = True
        '
        'AddOrder
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(360, 500)
        Me.Controls.Add(Me.ClearAndFillFrsAndArt)
        Me.Controls.Add(Me.BunifuCustomLabel3)
        Me.Controls.Add(Me.BunifuCustomLabel2)
        Me.Controls.Add(Me.TotalPrice_LBL)
        Me.Controls.Add(Me.BunifuImageButton1)
        Me.Controls.Add(Me.AddAnotherOrderButton)
        Me.Controls.Add(Me.AddOrderButton)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Orders_Fournisseur_CBB)
        Me.Controls.Add(Me.RemoveQuantityNumber)
        Me.Controls.Add(Me.AddQuantityNumber)
        Me.Controls.Add(Me.Orders_Nom_CBB)
        Me.Controls.Add(Me.Order_Quantite)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "AddOrder"
        Me.Text = "AddForm"
        CType(Me.AddQuantityNumber, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RemoveQuantityNumber, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CloseButton, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BunifuImageButton1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Order_Quantite As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents Orders_Nom_CBB As ComboBox
    Friend WithEvents AddQuantityNumber As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents RemoveQuantityNumber As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents Orders_Fournisseur_CBB As ComboBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents BunifuCustomLabel1 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents AddOrderButton As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents AddAnotherOrderButton As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents BunifuImageButton1 As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents TotalPrice_LBL As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents BunifuCustomLabel2 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents BunifuCustomLabel3 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents CloseButton As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents ClearAndFillFrsAndArt As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents BunifuDragControl1 As Bunifu.Framework.UI.BunifuDragControl
End Class
