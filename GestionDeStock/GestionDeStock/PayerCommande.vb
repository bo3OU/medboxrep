﻿Public Class PayerCommande
    Private formload As Boolean = False
    Private PaymDay As String
    Private PayPercent As Single
    Private PriceLeft As Single
    Private PayQuantity As Integer
    Private PayPrice As Single
    Private PayArticle As String
    Private PayFrs As String
    Private idcommande As Integer
    Private ArticleId As Integer
    Private PayingPricepercent As Single
    Private ArticleName As String
    Private PayTypes As New Dictionary(Of Integer, Integer)
    'TODO
    Private Sub PayerCommande_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PayAll.Select()
        formload = True
    End Sub

    Public Function ShowAndFill(a As Integer)
        ' Access.AddParam("@id", a)
        PayTypes.Clear()
        PaymentTypes_CMB.Items.Clear()
        PaymentTypes_CMB.SelectedIndex = -1
        idcommande = a
        Access.ExecQuery("select 
                    DISTINCT
                    commande.CO_QUANTITE as quantity,
                    FRS_NOM & ' ' & FRS_PRENOM as [NometPrenom],
                    fourni.FO_PRIX as price,
                    ARTICLE.AR_NOM as Article,
                    article.AR_ID_ARTICLE as articleid
                    from ((((commande
                    LEFT JOIN fourni On fourni.FO_ID_FOURNI = commande.CO_FOURNI)
                    LEFT JOIN paiement ON paiement.PA_COMMANDE = commande.CO_ID_COMMANDE )
                    LEFT JOIN article On article.AR_ID_ARTICLE = fourni.FO_ARTICLE)
                    LEFT JOIN fournisseur on fournisseur.FRS_ID_Fournisseur = fourni.FO_fournisseur)
                    where commande.co_employe = ANY
                        (Select em_id_employe from employe where em_directeur = " & SESSION.GetDirecteur() & ")
                    and CO_ID_COMMANDE = " & a)
        If NotEmpty(Access.Exception) Then
            MsgBox(Access.Exception)
        Else
            If Access.DBDT.Rows.Count > 0 Then
                PayQuantity = Access.DBDT.Rows(0).Item("quantity")
                PayPrice = Access.DBDT.Rows(0).Item("price")
                PayArticle = Access.DBDT.Rows(0).Item("Article")
                ArticleId = Access.DBDT.Rows(0).Item("articleid")
                TotalPrice.Text = (PayPrice * PayQuantity).ToString()
                PayFrs = Access.DBDT.Rows(0).Item("NometPrenom")
                CommandeDetails.Text = " Article : " & PayArticle & ", Fournisseur :  " & PayFrs

                ' GET LIST OF PAYMENTS METHODS

                Access.ExecQuery("select 
                                    SUM(PA_POURCENTAGE) as pourcent
                                    from paiement
                                    where  PA_COMMANDE = " & a)
                If NotEmpty(Access.Exception) Then
                    MsgBox(Access.Exception)
                End If
                If Access.DBDT.Rows.Count > 0 Then
                    If Access.DBDT.Rows(0).IsNull("pourcent") Then
                        PriceLeft = PayPrice * PayQuantity
                    ElseIf Access.DBDT.Rows(0).Item("pourcent") = 100 Then
                        Payer.Visible = False
                        AlreadyPaid.Visible = True
                    Else
                        PayPercent = Access.DBDT.Rows(0).Item("pourcent")
                        PriceLeft = ((100.0 - PayPercent) / 100.0) * PayPrice * PayQuantity
                    End If
                    PriceLeft_LBL.Text = PriceLeft & " MAD "
                    ' FILL THE CMB
                    Access.ExecQuery("select 
                                    TY_ID_TYPE as id,
                                    TY_NOM as line
                                    from TYPE_PAIEMENT")
                    If NotEmpty(Access.Exception) Then
                        MsgBox(Access.Exception)
                    Else
                        Dim i As Integer = 0
                        For Each r As DataRow In Access.DBDT.Rows
                            PayTypes.Add(i, r.Item("id"))
                            PaymentTypes_CMB.Items.Add(r.Item("line"))
                            i += 1
                        Next

                        Return 1
                    End If

                End If
            Else
                MsgBox("Aucune commande n'est selectionne !")
            End If
        End If
        Return 0
    End Function

    Private Sub Payer_Click(sender As Object, e As EventArgs) Handles Payer.Click
        'TODO REDO ALL OF THIS TO ACCTUALLY INSERT THE ROW IN STOCK TABLE
        If PaymentTypes_CMB.SelectedIndex < 0 Then
            MsgBox("choisissez une methode de paiement !")
        Else


            Dim replaced As String = PayingPricepercent.ToString()
            replaced.Replace(",", "s")

            Access.ExecQuery("insert into PAIEMENT
                (PA_COMMANDE,PA_TYPE,PA_DATE_PAIEMENT,PA_POURCENTAGE)
                values(" & idcommande & "," & PayTypes(PaymentTypes_CMB.SelectedIndex) & ",date(),'" & replaced & "')")
            If NotEmpty(Access.Exception) Then
                MsgBox(Access.Exception)
            Else
                Access.ExecQuery("select DISTINCT 
                                stock.ST_ID_STOCK as id,
                                stock.ST_QUANTITE as qte
                                from stock 
                                left join article on article.AR_ID_article = stock.ST_article
                                where stock.ST_CABINET = " & SESSION.GetCabinet() & "
                                and article.AR_NOM = '" & PayArticle & "'
                                and stock.ST_QUANTITE >0 ")
                If NotEmpty(Access.Exception) Then
                    MsgBox(Access.Exception)
                Else
                    Dim qte As Integer
                    Dim id As Integer
                    If PayAll.Checked = True Or PriceLeft - MoneyToPay.Text = 0 Then
                        If Access.DBDT.Rows.Count > 0 Then

                            id = Access.DBDT.Rows(0).Item("id")
                            qte = Access.DBDT.Rows(0).Item("qte")
                            'IF ONE ALREADY EXISTS THEN ADD THE CURRENT QUANTITY TO THE THE OLDER ORDER AND CHANGE OLDER ORDER'S STATE TO 6
                            Access.ExecQuery("update commande 
                            set CO_ETAT = 6 
                            where commande.CO_ID_COMMANDE =" & idcommande)
                            If NotEmpty(Access.Exception) Then
                                MsgBox(Access.Exception)
                            End If
                            'TODO HERE I SHOULD INSERT INTO STOCK
                            Access.AddParam("@quantity", (PayQuantity + qte))
                            Access.ExecQuery("update stock 
                            set ST_QUANTITE = @quantity
                             where stock.ST_ID_STOCK =" & id)
                            If NotEmpty(Access.Exception) Then
                                MsgBox(Access.Exception)
                            Else
                                MainForm.BringIndicator(MainForm.SubMenuButton_1_BTN, New EventArgs)
                                Close()
                            End If
                        Else

                            'IF IT DOESN'T EXIST THEN FUCKING CHANGE ITS STATE TO 4
                            Access.ExecQuery("update commande 
                            set CO_ETAT = 4
                            where commande.CO_ID_COMMANDE =" & idcommande)
                            If NotEmpty(Access.Exception) Then
                                MsgBox(Access.Exception)
                            End If
                            Access.ExecQuery("insert into stock
                                         (ST_ARTICLE,ST_CABINET,ST_QUANTITE)
                                         values(" & ArticleId & "," & SESSION.GetCabinet() & ", " & PayQuantity & ")")
                            If NotEmpty(Access.Exception) Then
                                MsgBox(Access.Exception)
                            Else
                                MainForm.BringIndicator(MainForm.SubMenuButton_1_BTN, New EventArgs)
                                Close()
                            End If
                        End If
                    End If
                End If
            End If
        End If

    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As Object, e As EventArgs) Handles PayAll.CheckedChanged
        If PayAll.Checked = True Then
            MoneyToPay.Enabled = False
            AddQuantityNumber.Enabled = False
            RemoveQuantityNumber.Enabled = False
            PayingPricepercent = (PriceLeft / (PayPrice * PayQuantity)) * 100
        Else
            MoneyToPay.Enabled = True
            AddQuantityNumber.Enabled = True
            RemoveQuantityNumber.Enabled = True
        End If
    End Sub

    Private Sub Order_Quantite_OnValueChanged(sender As Object, e As EventArgs) Handles MoneyToPay.OnValueChanged
        If formload Then
            If String.IsNullOrWhiteSpace(MoneyToPay.Text) Then
                MoneyToPay.Text = 1
            ElseIf (Int(MoneyToPay.Text)) < 1 Then
                MoneyToPay.Text = 1
            ElseIf (Int(MoneyToPay.Text)) >= PriceLeft - 1 Then
                MoneyToPay.Text = PriceLeft
            End If
            PayingPricepercent = (MoneyToPay.Text / (PayPrice * PayQuantity)) * 100
        End If
    End Sub

    Private Sub MoneyToPay_KeyPress(sender As Object, e As KeyPressEventArgs) Handles MoneyToPay.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Or MoneyToPay.Text.Length > 4 Then
                e.Handled = True
            End If
            Dim a As String = MoneyToPay.Text
        End If
    End Sub

    Private Sub CloseThisForm_Click(sender As Object, e As EventArgs) Handles CloseThisForm.Click
        Close()
    End Sub

    Private Sub AddQuantityNumber_Click(sender As Object, e As EventArgs) Handles AddQuantityNumber.Click
        MoneyToPay.Text = MoneyToPay.Text + 1
    End Sub

    Private Sub RemoveQuantityNumber_Click(sender As Object, e As EventArgs) Handles RemoveQuantityNumber.Click
        MoneyToPay.Text = MoneyToPay.Text - 1
    End Sub

End Class