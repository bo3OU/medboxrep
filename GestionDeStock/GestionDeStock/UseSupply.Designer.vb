﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UseSupply
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(UseSupply))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.BunifuImageButton2 = New Bunifu.Framework.UI.BunifuImageButton()
        Me.BunifuCustomLabel1 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.SubUseNumber = New Bunifu.Framework.UI.BunifuImageButton()
        Me.AddUseNumber = New Bunifu.Framework.UI.BunifuImageButton()
        Me.UseNumber = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.SubLeftNumber = New Bunifu.Framework.UI.BunifuImageButton()
        Me.AddLeftNumber = New Bunifu.Framework.UI.BunifuImageButton()
        Me.LeftNumber = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.UseRadio = New System.Windows.Forms.RadioButton()
        Me.LeftRadio = New System.Windows.Forms.RadioButton()
        Me.QuantityLeft = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.ArticleInfo = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.Modifier = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.CancelButton_BTN = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.QuantityBefore = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.BunifuDragControl1 = New Bunifu.Framework.UI.BunifuDragControl(Me.components)
        Me.Panel1.SuspendLayout()
        CType(Me.BunifuImageButton2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SubUseNumber, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AddUseNumber, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SubLeftNumber, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AddLeftNumber, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Panel1.Controls.Add(Me.BunifuImageButton2)
        Me.Panel1.Controls.Add(Me.BunifuCustomLabel1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.ForeColor = System.Drawing.Color.White
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(360, 100)
        Me.Panel1.TabIndex = 0
        '
        'BunifuImageButton2
        '
        Me.BunifuImageButton2.BackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.BunifuImageButton2.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Delete
        Me.BunifuImageButton2.ImageActive = Nothing
        Me.BunifuImageButton2.Location = New System.Drawing.Point(330, 0)
        Me.BunifuImageButton2.Name = "BunifuImageButton2"
        Me.BunifuImageButton2.Size = New System.Drawing.Size(30, 27)
        Me.BunifuImageButton2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.BunifuImageButton2.TabIndex = 17
        Me.BunifuImageButton2.TabStop = False
        Me.BunifuImageButton2.Zoom = 10
        '
        'BunifuCustomLabel1
        '
        Me.BunifuCustomLabel1.AutoSize = True
        Me.BunifuCustomLabel1.Font = New System.Drawing.Font("Century Gothic", 15.75!)
        Me.BunifuCustomLabel1.Location = New System.Drawing.Point(43, 34)
        Me.BunifuCustomLabel1.Name = "BunifuCustomLabel1"
        Me.BunifuCustomLabel1.Size = New System.Drawing.Size(281, 24)
        Me.BunifuCustomLabel1.TabIndex = 0
        Me.BunifuCustomLabel1.Text = "Utiliser des articles en stock"
        '
        'SubUseNumber
        '
        Me.SubUseNumber.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.SubUseNumber.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Cancel
        Me.SubUseNumber.ImageActive = Nothing
        Me.SubUseNumber.Location = New System.Drawing.Point(176, 214)
        Me.SubUseNumber.Name = "SubUseNumber"
        Me.SubUseNumber.Size = New System.Drawing.Size(25, 25)
        Me.SubUseNumber.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.SubUseNumber.TabIndex = 10
        Me.SubUseNumber.TabStop = False
        Me.SubUseNumber.Zoom = 10
        '
        'AddUseNumber
        '
        Me.AddUseNumber.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.AddUseNumber.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Add
        Me.AddUseNumber.ImageActive = Nothing
        Me.AddUseNumber.Location = New System.Drawing.Point(269, 214)
        Me.AddUseNumber.Name = "AddUseNumber"
        Me.AddUseNumber.Size = New System.Drawing.Size(25, 25)
        Me.AddUseNumber.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.AddUseNumber.TabIndex = 9
        Me.AddUseNumber.TabStop = False
        Me.AddUseNumber.Zoom = 10
        '
        'UseNumber
        '
        Me.UseNumber.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.UseNumber.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.UseNumber.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.UseNumber.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.UseNumber.BorderThickness = 3
        Me.UseNumber.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.UseNumber.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UseNumber.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.UseNumber.isPassword = False
        Me.UseNumber.Location = New System.Drawing.Point(208, 204)
        Me.UseNumber.Margin = New System.Windows.Forms.Padding(4)
        Me.UseNumber.Name = "UseNumber"
        Me.UseNumber.Size = New System.Drawing.Size(54, 44)
        Me.UseNumber.TabIndex = 8
        Me.UseNumber.Text = "1"
        Me.UseNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'SubLeftNumber
        '
        Me.SubLeftNumber.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.SubLeftNumber.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Cancel
        Me.SubLeftNumber.ImageActive = Nothing
        Me.SubLeftNumber.Location = New System.Drawing.Point(176, 300)
        Me.SubLeftNumber.Name = "SubLeftNumber"
        Me.SubLeftNumber.Size = New System.Drawing.Size(25, 25)
        Me.SubLeftNumber.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.SubLeftNumber.TabIndex = 13
        Me.SubLeftNumber.TabStop = False
        Me.SubLeftNumber.Zoom = 10
        '
        'AddLeftNumber
        '
        Me.AddLeftNumber.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.AddLeftNumber.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Add
        Me.AddLeftNumber.ImageActive = Nothing
        Me.AddLeftNumber.Location = New System.Drawing.Point(269, 300)
        Me.AddLeftNumber.Name = "AddLeftNumber"
        Me.AddLeftNumber.Size = New System.Drawing.Size(25, 25)
        Me.AddLeftNumber.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.AddLeftNumber.TabIndex = 12
        Me.AddLeftNumber.TabStop = False
        Me.AddLeftNumber.Zoom = 10
        '
        'LeftNumber
        '
        Me.LeftNumber.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.LeftNumber.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.LeftNumber.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.LeftNumber.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.LeftNumber.BorderThickness = 3
        Me.LeftNumber.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.LeftNumber.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LeftNumber.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.LeftNumber.isPassword = False
        Me.LeftNumber.Location = New System.Drawing.Point(208, 290)
        Me.LeftNumber.Margin = New System.Windows.Forms.Padding(4)
        Me.LeftNumber.Name = "LeftNumber"
        Me.LeftNumber.Size = New System.Drawing.Size(54, 44)
        Me.LeftNumber.TabIndex = 11
        Me.LeftNumber.Text = "0"
        Me.LeftNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'UseRadio
        '
        Me.UseRadio.AutoSize = True
        Me.UseRadio.Checked = True
        Me.UseRadio.Location = New System.Drawing.Point(61, 218)
        Me.UseRadio.Name = "UseRadio"
        Me.UseRadio.Size = New System.Drawing.Size(56, 17)
        Me.UseRadio.TabIndex = 16
        Me.UseRadio.TabStop = True
        Me.UseRadio.Text = "Utiliser"
        Me.UseRadio.UseVisualStyleBackColor = True
        '
        'LeftRadio
        '
        Me.LeftRadio.AutoSize = True
        Me.LeftRadio.Location = New System.Drawing.Point(61, 304)
        Me.LeftRadio.Name = "LeftRadio"
        Me.LeftRadio.Size = New System.Drawing.Size(59, 17)
        Me.LeftRadio.TabIndex = 17
        Me.LeftRadio.Text = "Reste :"
        Me.LeftRadio.UseVisualStyleBackColor = True
        '
        'QuantityLeft
        '
        Me.QuantityLeft.AutoSize = True
        Me.QuantityLeft.Location = New System.Drawing.Point(58, 397)
        Me.QuantityLeft.Name = "QuantityLeft"
        Me.QuantityLeft.Size = New System.Drawing.Size(143, 13)
        Me.QuantityLeft.TabIndex = 18
        Me.QuantityLeft.Text = "Quantité Apres Modification :"
        '
        'ArticleInfo
        '
        Me.ArticleInfo.AutoSize = True
        Me.ArticleInfo.Location = New System.Drawing.Point(142, 113)
        Me.ArticleInfo.Name = "ArticleInfo"
        Me.ArticleInfo.Size = New System.Drawing.Size(73, 13)
        Me.ArticleInfo.TabIndex = 19
        Me.ArticleInfo.Text = "Article : NULL"
        '
        'Modifier
        '
        Me.Modifier.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.Modifier.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.Modifier.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Modifier.BorderRadius = 0
        Me.Modifier.ButtonText = "Modifier"
        Me.Modifier.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Modifier.DisabledColor = System.Drawing.Color.Gray
        Me.Modifier.Iconcolor = System.Drawing.Color.Transparent
        Me.Modifier.Iconimage = CType(resources.GetObject("Modifier.Iconimage"), System.Drawing.Image)
        Me.Modifier.Iconimage_right = Nothing
        Me.Modifier.Iconimage_right_Selected = Nothing
        Me.Modifier.Iconimage_Selected = Nothing
        Me.Modifier.IconMarginLeft = 0
        Me.Modifier.IconMarginRight = 0
        Me.Modifier.IconRightVisible = True
        Me.Modifier.IconRightZoom = 0R
        Me.Modifier.IconVisible = True
        Me.Modifier.IconZoom = 90.0R
        Me.Modifier.IsTab = False
        Me.Modifier.Location = New System.Drawing.Point(226, 448)
        Me.Modifier.Name = "Modifier"
        Me.Modifier.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.Modifier.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.Modifier.OnHoverTextColor = System.Drawing.Color.White
        Me.Modifier.selected = False
        Me.Modifier.Size = New System.Drawing.Size(122, 40)
        Me.Modifier.TabIndex = 20
        Me.Modifier.Text = "Modifier"
        Me.Modifier.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Modifier.Textcolor = System.Drawing.Color.White
        Me.Modifier.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'CancelButton_BTN
        '
        Me.CancelButton_BTN.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.CancelButton_BTN.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.CancelButton_BTN.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.CancelButton_BTN.BorderRadius = 0
        Me.CancelButton_BTN.ButtonText = "Annuler"
        Me.CancelButton_BTN.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CancelButton_BTN.DisabledColor = System.Drawing.Color.Gray
        Me.CancelButton_BTN.Iconcolor = System.Drawing.Color.Transparent
        Me.CancelButton_BTN.Iconimage = CType(resources.GetObject("CancelButton_BTN.Iconimage"), System.Drawing.Image)
        Me.CancelButton_BTN.Iconimage_right = Nothing
        Me.CancelButton_BTN.Iconimage_right_Selected = Nothing
        Me.CancelButton_BTN.Iconimage_Selected = Nothing
        Me.CancelButton_BTN.IconMarginLeft = 0
        Me.CancelButton_BTN.IconMarginRight = 0
        Me.CancelButton_BTN.IconRightVisible = True
        Me.CancelButton_BTN.IconRightZoom = 0R
        Me.CancelButton_BTN.IconVisible = True
        Me.CancelButton_BTN.IconZoom = 90.0R
        Me.CancelButton_BTN.IsTab = False
        Me.CancelButton_BTN.Location = New System.Drawing.Point(98, 448)
        Me.CancelButton_BTN.Name = "CancelButton_BTN"
        Me.CancelButton_BTN.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.CancelButton_BTN.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.CancelButton_BTN.OnHoverTextColor = System.Drawing.Color.White
        Me.CancelButton_BTN.selected = False
        Me.CancelButton_BTN.Size = New System.Drawing.Size(122, 40)
        Me.CancelButton_BTN.TabIndex = 21
        Me.CancelButton_BTN.Text = "Annuler"
        Me.CancelButton_BTN.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CancelButton_BTN.Textcolor = System.Drawing.Color.White
        Me.CancelButton_BTN.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'QuantityBefore
        '
        Me.QuantityBefore.AutoSize = True
        Me.QuantityBefore.Location = New System.Drawing.Point(58, 362)
        Me.QuantityBefore.Name = "QuantityBefore"
        Me.QuantityBefore.Size = New System.Drawing.Size(147, 13)
        Me.QuantityBefore.TabIndex = 22
        Me.QuantityBefore.Text = "Quantite Avant Modification : "
        '
        'BunifuDragControl1
        '
        Me.BunifuDragControl1.Fixed = True
        Me.BunifuDragControl1.Horizontal = True
        Me.BunifuDragControl1.TargetControl = Me.Panel1
        Me.BunifuDragControl1.Vertical = True
        '
        'UseSupply
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(360, 500)
        Me.Controls.Add(Me.QuantityBefore)
        Me.Controls.Add(Me.CancelButton_BTN)
        Me.Controls.Add(Me.Modifier)
        Me.Controls.Add(Me.ArticleInfo)
        Me.Controls.Add(Me.QuantityLeft)
        Me.Controls.Add(Me.LeftRadio)
        Me.Controls.Add(Me.UseRadio)
        Me.Controls.Add(Me.SubLeftNumber)
        Me.Controls.Add(Me.AddLeftNumber)
        Me.Controls.Add(Me.LeftNumber)
        Me.Controls.Add(Me.SubUseNumber)
        Me.Controls.Add(Me.AddUseNumber)
        Me.Controls.Add(Me.UseNumber)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "UseSupply"
        Me.Text = "UseSupply"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.BunifuImageButton2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SubUseNumber, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AddUseNumber, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SubLeftNumber, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AddLeftNumber, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents BunifuCustomLabel1 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents BunifuImageButton2 As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents SubUseNumber As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents AddUseNumber As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents UseNumber As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents SubLeftNumber As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents AddLeftNumber As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents LeftNumber As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents UseRadio As RadioButton
    Friend WithEvents LeftRadio As RadioButton
    Friend WithEvents QuantityLeft As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents ArticleInfo As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents Modifier As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents CancelButton_BTN As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents QuantityBefore As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents BunifuDragControl1 As Bunifu.Framework.UI.BunifuDragControl
End Class
