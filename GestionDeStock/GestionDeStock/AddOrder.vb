﻿Imports System.Convert
Imports System.Collections.Generic
Public Class AddOrder
    'HOLD THE IDs OF THE LINES IN THE COMBOBOXES selectedindex -> id of the supplier/article
    Private frsvect As New Dictionary(Of Integer, Integer)
    Private artvect As New Dictionary(Of Integer, Integer)
    Private price As Single = 0
    Private id As Integer = -1

    'trick to disable launching changeevents while the form/panel is loading 
    Private FormLoad As Boolean = False
    Private Ready As Boolean = False
    'TODO i think those two are useless when i made the enable / disable 
    Private SupplierChange As Boolean = False
    Private ArticleChange As Boolean = False

    'For Modifying
    Private ModifyId As Integer = -1
    Private ModifyQuantite As Integer
    Private ModifyArticle As Integer
    Private ModifyFournisseur As Integer
    Private ModifyPrice As Integer
    Private ClickedModify As Boolean = False

    Private Sub AddForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Filling()
        'ENABLE CHANGE EVENTS TO BE ACTIVE
        FormLoad = True
    End Sub

    Private Sub AddQuantityNumber_Click(sender As Object, e As EventArgs) Handles AddQuantityNumber.Click
        Order_Quantite.Text = Order_Quantite.Text + 1
    End Sub

    Private Sub AddOrder_Quantite_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Order_Quantite.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Or Order_Quantite.Text.Length > 4 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub RemoveQuantityNumber_Click(sender As Object, e As EventArgs) Handles RemoveQuantityNumber.Click
        Order_Quantite.Text = Order_Quantite.Text - 1
    End Sub

    Private Sub AddOrder_Quantite_OnValueChanged(sender As Object, e As EventArgs) Handles Order_Quantite.OnValueChanged
        If String.IsNullOrWhiteSpace(Order_Quantite.Text) Then
            Order_Quantite.Text = 1
        ElseIf (ToInt64(Order_Quantite.Text)) < 1 Then
            Order_Quantite.Text = 1
        End If
        TotalPrice_LBL.Text = "Total : " & (String.Format("{0:0.00}", price) * Int(Order_Quantite.Text)).ToString & " MAD"

    End Sub

    Private Sub Orders_Fournisseur_CBB_SelectedIndexChanged(sender As Object, e As EventArgs) Handles Orders_Fournisseur_CBB.SelectedIndexChanged
        'FILL THE ARTICLES LIST DEPENDING ON THE CHOSEN SUPPLIER 
        If FormLoad And Ready Then
            'FORM IS LOADED, WE CAN CHANGE EVENT
            If Not (ArticleChange) Then
                'COMBOBOX Articles changed, we need to fill THE frs ONLY IF FRS DIDN'T CHANGE
                Access.AddParam("@Cabinet", SESSION.GetCabinet())
                Access.AddParam("@fournisseur", (frsvect(Orders_Fournisseur_CBB.SelectedIndex)))
                Access.ExecQuery("Select DISTINCT article.AR_ID_ARTICLE as id, article.AR_NOM as line 
                          from (Fourni
                          LEFT JOIN ARTICLE ON article.AR_ID_ARTICLE = fourni.FO_ARTICLE)
                          where fourni.FO_CABINET = @Cabinet AND fourni.FO_FOURNISSEUR = @fournisseur and fourni.FO_article IS NOT NULL")
                Dim i As Integer = 0
                Orders_Nom_CBB.Items.Clear()
                artvect.Clear()
                For Each r As DataRow In Access.DBDT.Rows
                    'FILL ARTICLE TEXTBOX
                    Orders_Nom_CBB.Items.Add(r.Item("line"))
                    artvect.Add(i, r.Item("id"))
                    i = i + 1
                Next
                Orders_Fournisseur_CBB.Enabled = False
            End If
            SupplierChange = True
            GetPriceAndId()
        End If
    End Sub

    Private Sub BunifuImageButton1_Click(sender As Object, e As EventArgs) Handles BunifuImageButton1.Click
        Filling()
    End Sub

    Private Sub Orders_Nom_CBB_SelectedIndexChanged(sender As Object, e As EventArgs) Handles Orders_Nom_CBB.SelectedIndexChanged
        If FormLoad And Ready Then
            'FORM IS LOADED, WE CAN CHANGE EVENT
            If Not (SupplierChange) Then
                ' COMBOBOX Articles changed, we need to fill THE frs ONLY IF FRS DIDN'T CHANGE
                Access.AddParam("@Cabinet", SESSION.GetCabinet())
                Access.AddParam("@article", (artvect(Orders_Nom_CBB.SelectedIndex)))
                Access.ExecQuery("Select DISTINCT fournisseur.FRS_ID_FOURNISSEUR as id, Fournisseur.FRS_SOCIETE & ' : ' & Fournisseur.FRS_NOM & ' ' & Fournisseur.FRS_PRENOM as line, fourni.FO_ID_FOURNI as id_fo
                          from (Fourni
                          LEFT JOIN FOURNISSEUR ON fournisseur.FRS_ID_FOURNISSEUR = fourni.FO_FOURNISSEUR)
                          where fourni.FO_CABINET = @Cabinet AND fourni.FO_ARTICLE = @article and FO_fournisseur IS NOT NULL")
                Dim i As Integer = 0
                Orders_Fournisseur_CBB.Items.Clear()
                frsvect.Clear()
                For Each r As DataRow In Access.DBDT.Rows
                    'FILL ARTICLE TEXTBOX
                    Orders_Fournisseur_CBB.Items.Add(r.Item("line"))
                    frsvect.Add(i, r.Item("id"))
                    i = i + 1
                Next
                Orders_Nom_CBB.Enabled = False
            End If
            ArticleChange = True
            GetPriceAndId()
            'IF THE VALUES ARE BOTH IN INE ARTICLE NAME AND IN SUPPLIER I WILL FUCKING NEED TO GET THE PRICE AND THE FOURNI ID THIS IS HOW TO SOLVE THIS ISSUE 
        End If
    End Sub

    Public Sub Filling()
        Orders_Fournisseur_CBB.AutoCompleteMode = AutoCompleteMode.Append
        Orders_Fournisseur_CBB.DropDownStyle = ComboBoxStyle.DropDown
        Orders_Fournisseur_CBB.AutoCompleteSource = AutoCompleteSource.ListItems
        Orders_Nom_CBB.AutoCompleteMode = AutoCompleteMode.Append
        Orders_Nom_CBB.DropDownStyle = ComboBoxStyle.DropDown
        Orders_Nom_CBB.AutoCompleteSource = AutoCompleteSource.ListItems
        SupplierChange = False
        ArticleChange = False
        Orders_Fournisseur_CBB.Enabled = True
        Orders_Nom_CBB.Enabled = True
        Orders_Fournisseur_CBB.Items.Clear()
        Orders_Nom_CBB.Items.Clear()
        frsvect.Clear()
        artvect.Clear()
        Orders_Fournisseur_CBB.SelectedIndex = -1
        Orders_Nom_CBB.SelectedIndex = -1
        Ready = True
        TotalPrice_LBL.Text = "Total :  0 MAD"
        AddOrderButton.Visible = False
        AddAnotherOrderButton.Visible = False
        Access.AddParam("@Cabinet", SESSION.GetCabinet())
        'FILL THE SUPPLIERS' LIST
        Access.ExecQuery("Select DISTINCT fournisseur.FRS_ID_FOURNISSEUR as id, Fournisseur.FRS_NOM & ' ' & Fournisseur.FRS_PRENOM as line
                          from (Fourni
                          LEFT JOIN FOURNISSEUR ON fournisseur.FRS_ID_FOURNISSEUR = fourni.FO_FOURNISSEUR)
                          where FO_CABINET = @Cabinet and FO_ARTICLE IS NOT NULL")
        If NotEmpty(Access.Exception) Then MsgBox(Access.Exception) : Exit Sub
        Dim i As Integer = 0

        For Each r As DataRow In Access.DBDT.Rows
            If Not (r.Item("line").Equals(DBNull.value) Or r.Item("id").Equals(DBNull.value)) Then
                Orders_Fournisseur_CBB.Items.Add(r.Item("line"))
                frsvect.Add(i, r.Item("id"))
                i = i + 1
            End If

        Next r
        'FILL THE ARTICLES
        Access.AddParam("@Cabinet", SESSION.GetCabinet())
        Access.ExecQuery("Select DISTINCT article.AR_ID_ARTICLE as id, article.AR_NOM as line
                          from (Fourni
                          LEFT JOIN ARTICLE ON article.AR_ID_ARTICLE = fourni.FO_ARTICLE)
                          where FO_CABINET = @Cabinet and FO_FOURNISSEUR IS NOT NULL")
        If NotEmpty(Access.Exception) Then MsgBox(Access.Exception) : Exit Sub
        i = 0
        For Each r As DataRow In Access.DBDT.Rows
            If Not (r.Item("line").Equals(DBNull.value) Or r.Item("id").Equals(DBNull.value)) Then
                Orders_Nom_CBB.Items.Add(r.Item("line"))
                artvect.Add(i, r.Item("id"))
                i = i + 1
            End If
        Next r

        'NOW BOTH LISTS ARE FULL BUT WHEN EVENT CHANGE HAPPENS WE NEED TO FILTER DATA ONCE AGAIN
        'TO SHOW ONLY ARTICLES THAT A SUPPLIER CAN SUPPLY
        'OR ONLY SUPPLIERS WHO CAN SUPPLY A GIVEN ARTICLE
    End Sub

    Private Sub AddAnotherOrderButton_Click(sender As Object, e As EventArgs) Handles AddAnotherOrderButton.Click
        If ModifyId <> -1 Then
            Close()
        Else
            If InsertOrder() = 1 Then
                MainForm.BringIndicator(MainForm.SubMenuButton_1_BTN, New EventArgs)
                Filling()
            End If
        End If
    End Sub

    Private Sub BunifuFlatButton1_Click(sender As Object, e As EventArgs) Handles AddOrderButton.Click

        If ModifyId <> -1 Then
            If UpdateOrder() = 1 Then
                MainForm.BringIndicator(MainForm.SubMenuButton_1_BTN, New EventArgs)
                Close()
            End If
        Else
            If InsertOrder() = 1 Then
                MainForm.BringIndicator(MainForm.SubMenuButton_1_BTN, New EventArgs)
                Close()
            End If
        End If
        'TODO close this form
    End Sub

    Private Function InsertOrder()
        'adds order in database, not parametered :/
        Access.AddParam("@fourni", id)
        Access.AddParam("@employe", SESSION.GetUser())
        Access.AddParam("@quantity", Int(Order_Quantite.Text))
        Access.ExecQuery("INSERT INTO COMMANDE (CO_FOURNI, CO_EMPLOYE, Co_ETAT, CO_QUANTITE, Co_DATE_COMMANDE)
                        VALUES(@fourni, @employe, 1, @quantity, date())")
        If NotEmpty(Access.Exception) Then
            MsgBox(Access.Exception)
        Else
            Return 1
        End If
        Return 0
    End Function

    Private Function UpdateOrder()
        If Orders_Nom_CBB.Enabled = True Or Orders_Fournisseur_CBB.Enabled = True Then
            Access.AddParam("@fournisseur", frsvect(Orders_Fournisseur_CBB.SelectedIndex))
            Access.AddParam("@article", artvect(Orders_Nom_CBB.SelectedIndex))
            Access.ExecQuery("select  fourni.FO_ID_FOURNI as id
                            from fourni
                            where fourni.FO_FOURNISSEUR = @fournisseur
                            and fourni.FO_ARTICLE = @article and fourni.FO_cabinet = " & SESSION.GetCabinet())
            If NotEmpty(Access.Exception) Then
                MsgBox(Access.Exception)
            Else
                If Access.DBDT.Rows.Count > 0 Then

                    Access.AddParam("@fourni", Access.DBDT.Rows(0).Item("id"))
                    Access.ExecQuery("update Commande 
                            set commande.CO_FOURNI = @fourni
                            where Commande.CO_ID_COMMANDE= " & ModifyId)
                    If NotEmpty(Access.Exception) Then
                        MsgBox(Access.Exception)
                    Else
                        Return 1
                    End If
                End If
            End If
        End If
        Access.ExecQuery("update Commande 
                        set commande.CO_QUANTITE =    
                        where Commande.CO_ID_COMMANDE = " & ModifyId)
        If NotEmpty(Access.Exception) Then
            MsgBox(Access.Exception)
        End If
        Return 0
    End Function

    'WHEN BOTH ARTICLE AND SUPPLIER ARE SELECTED , FIRE THIS SUB
    Private Sub GetPriceAndId()
        If ArticleChange And SupplierChange Then
            'GET THE FOURNI AND THE PRICE FGS SO I CAN INSERT
            Access.AddParam("@Cabinet", SESSION.GetCabinet())
            Access.AddParam("@article", (artvect(Orders_Nom_CBB.SelectedIndex)))
            Access.AddParam("@fournisseur", (frsvect(Orders_Fournisseur_CBB.SelectedIndex)))
            Access.ExecQuery("Select DISTINCT FOURNI.FO_ID_FOURNI as id, FOURNI.FO_PRIX as price
                                from FOURNI
                                where fourni.FO_CABINET = @Cabinet 
                                AND fourni.FO_ARTICLE = @article 
                                and fourni.FO_FOURNISSEUR = @fournisseur ")
            If NotEmpty(Access.Exception) Then MsgBox(Access.Exception) : Exit Sub
            id = Access.DBDT.Rows(0).Item("id")

            price = Access.DBDT.Rows(0).Item("price").ToString
            price = String.Format("{0:0.00}", price)
            AddOrderButton.Visible = True
            AddAnotherOrderButton.Visible = True
            TotalPrice_LBL.Text = "Total : " & (String.Format("{0:0.00}", price) * Int(Order_Quantite.Text)).ToString & " MAD"

        End If
    End Sub

    Private Sub CloseButton_Click(sender As Object, e As EventArgs) Handles CloseButton.Click
        Close()
    End Sub

    Public Function FillToModify(id As Integer)
        ModifyId = id
        AddOrderButton.Visible = True
        AddAnotherOrderButton.Visible = True
        ClearAndFillFrsAndArt.Visible = True
        Orders_Fournisseur_CBB.AutoCompleteMode = AutoCompleteMode.Append
        Orders_Fournisseur_CBB.DropDownStyle = ComboBoxStyle.DropDown
        Orders_Fournisseur_CBB.AutoCompleteSource = AutoCompleteSource.ListItems
        Orders_Nom_CBB.AutoCompleteMode = AutoCompleteMode.Append
        Orders_Nom_CBB.DropDownStyle = ComboBoxStyle.DropDown
        Orders_Nom_CBB.AutoCompleteSource = AutoCompleteSource.ListItems
        SupplierChange = False
        ArticleChange = False
        AddOrderButton.Text = "Modifier"
        AddAnotherOrderButton.Text = "Annuler"
        Orders_Fournisseur_CBB.Items.Clear()
        Orders_Nom_CBB.Items.Clear()
        frsvect.Clear()
        artvect.Clear()
        Orders_Fournisseur_CBB.SelectedIndex = -1
        Orders_Nom_CBB.SelectedIndex = -1
        Access.ExecQuery("select 
                        article.AR_ID_ARTICLE as articleid,
                        article.AR_NOM as articlename,
                        fournisseur.FRS_ID_FOURNISSEUR as fournisseurid,
                        fournisseur.FRS_NOM & ' ' & fournisseur.FRS_PRENOM as fournisseurname,
                        commande.CO_QUANTITE as quantite,
                        fourni.FO_PRIX as price
                        from (((commande
                        LEFT JOIN fourni ON fourni.FO_ID_FOURNI = commande.CO_FOURNI)
                        LEFT JOIN fournisseur ON fournisseur.FRS_ID_FOURNISSEUR = fourni.FO_FOURNISSEUR)
                        LEFT JOIN article ON article.AR_ID_ARTICLE = fourni.FO_ARTICLE)
                        where CO_ID_COMMANDE =" & ModifyId)
        If NotEmpty(Access.Exception) Then
            MsgBox(Access.Exception)
            Return 0
        Else
            If Access.DBDT.Rows.Count > 0 Then
                ModifyQuantite = Access.DBDT.Rows(0).Item("quantite")

                ModifyFournisseur = Access.DBDT.Rows(0).Item("fournisseurid")


                ModifyArticle = Access.DBDT.Rows(0).Item("articleid")
                ModifyPrice = Access.DBDT.Rows(0).Item("price")
                price = ModifyPrice
            End If
        End If
        Order_Quantite.Text = ModifyQuantite
        TotalPrice_LBL.Text = "Total : " & ModifyQuantite * ModifyPrice & "Mad"
        Orders_Fournisseur_CBB.Text = Access.DBDT.Rows(0).Item("fournisseurname")
        Orders_Nom_CBB.Text = Access.DBDT.Rows(0).Item("articlename")
        Ready = True
        Return 1
    End Function

    Private Sub ClearAndFillFrsAndArt_Click_1(sender As Object, e As EventArgs) Handles ClearAndFillFrsAndArt.Click
        If ClickedModify = False Then
            'FILL THE ARTICLES
            AddOrderButton.Visible = False
            Access.AddParam("@Cabinet", SESSION.GetCabinet())
            Access.ExecQuery("Select DISTINCT article.AR_ID_ARTICLE as id, article.AR_NOM as line
                          from (Fourni
                          LEFT JOIN ARTICLE ON article.AR_ID_ARTICLE = fourni.FO_ARTICLE)
                          where FO_CABINET = @Cabinet and FO_FOURNISSEUR IS NOT NULL")
            If NotEmpty(Access.Exception) Then
                MsgBox(Access.Exception)
            End If
            Dim i As Integer = 0
            For Each r As DataRow In Access.DBDT.Rows
                If Not (r.Item("line").Equals(DBNull.value) Or r.Item("id").Equals(DBNull.value)) Then
                    Orders_Nom_CBB.Items.Add(r.Item("line"))
                    artvect.Add(i, r.Item("id"))
                    i = i + 1
                End If
            Next r
            Access.AddParam("@Cabinet", SESSION.GetCabinet())
            Access.ExecQuery("Select DISTINCT fournisseur.FRS_ID_FOURNISSEUR as id, 
                            Fournisseur.FRS_NOM & ' ' & Fournisseur.FRS_PRENOM as line
                            from (Fourni
                            LEFT JOIN FOURNISSEUR ON fournisseur.FRS_ID_FOURNISSEUR = fourni.FO_FOURNISSEUR)
                            where FO_CABINET = @Cabinet and FO_ARTICLE IS NOT NULL")
            If NotEmpty(Access.Exception) Then
                MsgBox(Access.Exception)
            End If
            i = 0
            For Each r As DataRow In Access.DBDT.Rows
                If Not (r.Item("line").Equals(DBNull.value) Or r.Item("id").Equals(DBNull.value)) Then
                    Orders_Fournisseur_CBB.Items.Add(r.Item("line"))
                    frsvect.Add(i, r.Item("id"))
                    i = i + 1
                End If
            Next r
            Orders_Fournisseur_CBB.Enabled = True
            Orders_Nom_CBB.Enabled = True
            ClickedModify = True
        End If
    End Sub

    Private Function keyof(a As Dictionary(Of Integer, Integer), word As Integer)
        Dim i As Integer = 0
        For Each k As Integer In a.Keys
            If a(k) = word Then
                Return k
            End If
        Next
        Return -1
    End Function

End Class