﻿Module GloabalStaticVariables
    'chosen Menu is the LeftMainMenuPanel
    Public ChosenMenu As Integer = 0

    'wether look up, add , modify or delete
    Public ChosenAction As Integer = -1

    'is the form loaded or not
    'bad trick to not let (onchange)events run
    'but also the easiest and fastest one .. weird world we're living in

    'Public FormAddOrderLoad As Boolean = False

    'a function to check if access.dbdt is empty and a small object of databasecontrol
    'Class to use inside the program
    Public Access As New DatabaseControl()
    Public Function NotEmpty(text As String) As Boolean
        Return Not String.IsNullOrEmpty(text)
    End Function

    'THIS IS A GLOBAL SESSIONHANDLER VARIABALE
    Public SESSION As New SessionHandler()

    'this is the panel showing in the main screen, whatever panel shows up is here  !
    Public ActivePanel As New Panel()


    Public Function FirstValue(a As Dictionary(Of Integer, Integer), value As Integer)
        For Each r In a.Keys
            If value = a(r) Then
                Return r
            End If
        Next
        Return -1
    End Function

End Module
