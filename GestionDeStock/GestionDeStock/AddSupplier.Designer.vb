﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AddSupplier
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AddSupplier))
        Me.PrenomFournisseur_TXB = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.NomFournisseur_TXB = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.SocieteFournisseur_TXB = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.AdresseFournisseur_TXB = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.EmailFournisseur_TXB = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.TelephoneFournisseur_TXB = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.VilleFournisseur_TXB = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.BunifuImageButton2 = New Bunifu.Framework.UI.BunifuImageButton()
        Me.BunifuCustomLabel4 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.AddAnotherSupplier = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.AddModSupplier = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.BunifuImageButton1 = New Bunifu.Framework.UI.BunifuImageButton()
        Me.BunifuDragControl1 = New Bunifu.Framework.UI.BunifuDragControl(Me.components)
        Me.Panel1.SuspendLayout()
        CType(Me.BunifuImageButton2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BunifuImageButton1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PrenomFournisseur_TXB
        '
        Me.PrenomFournisseur_TXB.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.PrenomFournisseur_TXB.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.PrenomFournisseur_TXB.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.PrenomFournisseur_TXB.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.PrenomFournisseur_TXB.BorderThickness = 3
        Me.PrenomFournisseur_TXB.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.PrenomFournisseur_TXB.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PrenomFournisseur_TXB.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.PrenomFournisseur_TXB.isPassword = False
        Me.PrenomFournisseur_TXB.Location = New System.Drawing.Point(47, 119)
        Me.PrenomFournisseur_TXB.Margin = New System.Windows.Forms.Padding(4)
        Me.PrenomFournisseur_TXB.Name = "PrenomFournisseur_TXB"
        Me.PrenomFournisseur_TXB.Size = New System.Drawing.Size(271, 36)
        Me.PrenomFournisseur_TXB.TabIndex = 29
        Me.PrenomFournisseur_TXB.Tag = "0"
        Me.PrenomFournisseur_TXB.Text = "Prenom du Fournisseur"
        Me.PrenomFournisseur_TXB.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'NomFournisseur_TXB
        '
        Me.NomFournisseur_TXB.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.NomFournisseur_TXB.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.NomFournisseur_TXB.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.NomFournisseur_TXB.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.NomFournisseur_TXB.BorderThickness = 3
        Me.NomFournisseur_TXB.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.NomFournisseur_TXB.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NomFournisseur_TXB.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.NomFournisseur_TXB.isPassword = False
        Me.NomFournisseur_TXB.Location = New System.Drawing.Point(47, 163)
        Me.NomFournisseur_TXB.Margin = New System.Windows.Forms.Padding(4)
        Me.NomFournisseur_TXB.Name = "NomFournisseur_TXB"
        Me.NomFournisseur_TXB.Size = New System.Drawing.Size(271, 36)
        Me.NomFournisseur_TXB.TabIndex = 30
        Me.NomFournisseur_TXB.Tag = "1"
        Me.NomFournisseur_TXB.Text = "Nom du Fournisseur"
        Me.NomFournisseur_TXB.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'SocieteFournisseur_TXB
        '
        Me.SocieteFournisseur_TXB.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SocieteFournisseur_TXB.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SocieteFournisseur_TXB.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SocieteFournisseur_TXB.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SocieteFournisseur_TXB.BorderThickness = 3
        Me.SocieteFournisseur_TXB.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.SocieteFournisseur_TXB.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SocieteFournisseur_TXB.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.SocieteFournisseur_TXB.isPassword = False
        Me.SocieteFournisseur_TXB.Location = New System.Drawing.Point(47, 207)
        Me.SocieteFournisseur_TXB.Margin = New System.Windows.Forms.Padding(4)
        Me.SocieteFournisseur_TXB.Name = "SocieteFournisseur_TXB"
        Me.SocieteFournisseur_TXB.Size = New System.Drawing.Size(271, 36)
        Me.SocieteFournisseur_TXB.TabIndex = 31
        Me.SocieteFournisseur_TXB.Tag = "2"
        Me.SocieteFournisseur_TXB.Text = "Societe"
        Me.SocieteFournisseur_TXB.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'AdresseFournisseur_TXB
        '
        Me.AdresseFournisseur_TXB.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.AdresseFournisseur_TXB.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.AdresseFournisseur_TXB.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.AdresseFournisseur_TXB.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.AdresseFournisseur_TXB.BorderThickness = 3
        Me.AdresseFournisseur_TXB.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.AdresseFournisseur_TXB.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AdresseFournisseur_TXB.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.AdresseFournisseur_TXB.isPassword = False
        Me.AdresseFournisseur_TXB.Location = New System.Drawing.Point(47, 295)
        Me.AdresseFournisseur_TXB.Margin = New System.Windows.Forms.Padding(4)
        Me.AdresseFournisseur_TXB.Name = "AdresseFournisseur_TXB"
        Me.AdresseFournisseur_TXB.Size = New System.Drawing.Size(271, 36)
        Me.AdresseFournisseur_TXB.TabIndex = 33
        Me.AdresseFournisseur_TXB.Tag = "4"
        Me.AdresseFournisseur_TXB.Text = "Adresse du Fournisseur"
        Me.AdresseFournisseur_TXB.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'EmailFournisseur_TXB
        '
        Me.EmailFournisseur_TXB.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.EmailFournisseur_TXB.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.EmailFournisseur_TXB.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.EmailFournisseur_TXB.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.EmailFournisseur_TXB.BorderThickness = 3
        Me.EmailFournisseur_TXB.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.EmailFournisseur_TXB.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EmailFournisseur_TXB.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.EmailFournisseur_TXB.isPassword = False
        Me.EmailFournisseur_TXB.Location = New System.Drawing.Point(47, 339)
        Me.EmailFournisseur_TXB.Margin = New System.Windows.Forms.Padding(4)
        Me.EmailFournisseur_TXB.Name = "EmailFournisseur_TXB"
        Me.EmailFournisseur_TXB.Size = New System.Drawing.Size(271, 36)
        Me.EmailFournisseur_TXB.TabIndex = 34
        Me.EmailFournisseur_TXB.Tag = "5"
        Me.EmailFournisseur_TXB.Text = "Email du Fournisseur"
        Me.EmailFournisseur_TXB.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'TelephoneFournisseur_TXB
        '
        Me.TelephoneFournisseur_TXB.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.TelephoneFournisseur_TXB.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.TelephoneFournisseur_TXB.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.TelephoneFournisseur_TXB.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.TelephoneFournisseur_TXB.BorderThickness = 3
        Me.TelephoneFournisseur_TXB.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TelephoneFournisseur_TXB.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TelephoneFournisseur_TXB.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.TelephoneFournisseur_TXB.isPassword = False
        Me.TelephoneFournisseur_TXB.Location = New System.Drawing.Point(47, 383)
        Me.TelephoneFournisseur_TXB.Margin = New System.Windows.Forms.Padding(4)
        Me.TelephoneFournisseur_TXB.Name = "TelephoneFournisseur_TXB"
        Me.TelephoneFournisseur_TXB.Size = New System.Drawing.Size(271, 36)
        Me.TelephoneFournisseur_TXB.TabIndex = 35
        Me.TelephoneFournisseur_TXB.Tag = "6"
        Me.TelephoneFournisseur_TXB.Text = "N° de Telephone"
        Me.TelephoneFournisseur_TXB.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'VilleFournisseur_TXB
        '
        Me.VilleFournisseur_TXB.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.VilleFournisseur_TXB.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.VilleFournisseur_TXB.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.VilleFournisseur_TXB.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.VilleFournisseur_TXB.BorderThickness = 3
        Me.VilleFournisseur_TXB.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.VilleFournisseur_TXB.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VilleFournisseur_TXB.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.VilleFournisseur_TXB.isPassword = False
        Me.VilleFournisseur_TXB.Location = New System.Drawing.Point(47, 251)
        Me.VilleFournisseur_TXB.Margin = New System.Windows.Forms.Padding(4)
        Me.VilleFournisseur_TXB.Name = "VilleFournisseur_TXB"
        Me.VilleFournisseur_TXB.Size = New System.Drawing.Size(271, 36)
        Me.VilleFournisseur_TXB.TabIndex = 32
        Me.VilleFournisseur_TXB.Tag = "3"
        Me.VilleFournisseur_TXB.Text = "Ville du Fournisseur"
        Me.VilleFournisseur_TXB.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Panel1.Controls.Add(Me.BunifuImageButton2)
        Me.Panel1.Controls.Add(Me.BunifuCustomLabel4)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(360, 94)
        Me.Panel1.TabIndex = 38
        '
        'BunifuImageButton2
        '
        Me.BunifuImageButton2.BackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.BunifuImageButton2.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Delete
        Me.BunifuImageButton2.ImageActive = Nothing
        Me.BunifuImageButton2.Location = New System.Drawing.Point(330, 0)
        Me.BunifuImageButton2.Name = "BunifuImageButton2"
        Me.BunifuImageButton2.Size = New System.Drawing.Size(30, 27)
        Me.BunifuImageButton2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.BunifuImageButton2.TabIndex = 2
        Me.BunifuImageButton2.TabStop = False
        Me.BunifuImageButton2.Zoom = 10
        '
        'BunifuCustomLabel4
        '
        Me.BunifuCustomLabel4.AutoSize = True
        Me.BunifuCustomLabel4.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel4.ForeColor = System.Drawing.Color.White
        Me.BunifuCustomLabel4.Location = New System.Drawing.Point(91, 34)
        Me.BunifuCustomLabel4.Name = "BunifuCustomLabel4"
        Me.BunifuCustomLabel4.Size = New System.Drawing.Size(203, 25)
        Me.BunifuCustomLabel4.TabIndex = 1
        Me.BunifuCustomLabel4.Text = "Ajouter Fournisseur"
        '
        'AddAnotherSupplier
        '
        Me.AddAnotherSupplier.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.AddAnotherSupplier.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.AddAnotherSupplier.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.AddAnotherSupplier.BorderRadius = 0
        Me.AddAnotherSupplier.ButtonText = "ajouter et rester"
        Me.AddAnotherSupplier.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AddAnotherSupplier.DisabledColor = System.Drawing.Color.Gray
        Me.AddAnotherSupplier.Iconcolor = System.Drawing.Color.Transparent
        Me.AddAnotherSupplier.Iconimage = CType(resources.GetObject("AddAnotherSupplier.Iconimage"), System.Drawing.Image)
        Me.AddAnotherSupplier.Iconimage_right = Nothing
        Me.AddAnotherSupplier.Iconimage_right_Selected = Nothing
        Me.AddAnotherSupplier.Iconimage_Selected = Nothing
        Me.AddAnotherSupplier.IconMarginLeft = 0
        Me.AddAnotherSupplier.IconMarginRight = 0
        Me.AddAnotherSupplier.IconRightVisible = True
        Me.AddAnotherSupplier.IconRightZoom = 0R
        Me.AddAnotherSupplier.IconVisible = True
        Me.AddAnotherSupplier.IconZoom = 90.0R
        Me.AddAnotherSupplier.IsTab = False
        Me.AddAnotherSupplier.Location = New System.Drawing.Point(12, 440)
        Me.AddAnotherSupplier.Name = "AddAnotherSupplier"
        Me.AddAnotherSupplier.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.AddAnotherSupplier.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.AddAnotherSupplier.OnHoverTextColor = System.Drawing.Color.White
        Me.AddAnotherSupplier.selected = False
        Me.AddAnotherSupplier.Size = New System.Drawing.Size(158, 48)
        Me.AddAnotherSupplier.TabIndex = 37
        Me.AddAnotherSupplier.Text = "ajouter et rester"
        Me.AddAnotherSupplier.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.AddAnotherSupplier.Textcolor = System.Drawing.Color.White
        Me.AddAnotherSupplier.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'AddModSupplier
        '
        Me.AddModSupplier.Activecolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.AddModSupplier.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.AddModSupplier.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.AddModSupplier.BorderRadius = 0
        Me.AddModSupplier.ButtonText = "Ajouter et quitter"
        Me.AddModSupplier.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AddModSupplier.DisabledColor = System.Drawing.Color.Gray
        Me.AddModSupplier.Iconcolor = System.Drawing.Color.Transparent
        Me.AddModSupplier.Iconimage = CType(resources.GetObject("AddModSupplier.Iconimage"), System.Drawing.Image)
        Me.AddModSupplier.Iconimage_right = Nothing
        Me.AddModSupplier.Iconimage_right_Selected = Nothing
        Me.AddModSupplier.Iconimage_Selected = Nothing
        Me.AddModSupplier.IconMarginLeft = 0
        Me.AddModSupplier.IconMarginRight = 0
        Me.AddModSupplier.IconRightVisible = True
        Me.AddModSupplier.IconRightZoom = 0R
        Me.AddModSupplier.IconVisible = True
        Me.AddModSupplier.IconZoom = 90.0R
        Me.AddModSupplier.IsTab = False
        Me.AddModSupplier.Location = New System.Drawing.Point(190, 440)
        Me.AddModSupplier.Name = "AddModSupplier"
        Me.AddModSupplier.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.AddModSupplier.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.AddModSupplier.OnHoverTextColor = System.Drawing.Color.White
        Me.AddModSupplier.selected = False
        Me.AddModSupplier.Size = New System.Drawing.Size(158, 48)
        Me.AddModSupplier.TabIndex = 36
        Me.AddModSupplier.Text = "Ajouter et quitter"
        Me.AddModSupplier.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.AddModSupplier.Textcolor = System.Drawing.Color.White
        Me.AddModSupplier.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'BunifuImageButton1
        '
        Me.BunifuImageButton1.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.BunifuImageButton1.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Refresh
        Me.BunifuImageButton1.ImageActive = Nothing
        Me.BunifuImageButton1.Location = New System.Drawing.Point(327, 100)
        Me.BunifuImageButton1.Name = "BunifuImageButton1"
        Me.BunifuImageButton1.Size = New System.Drawing.Size(21, 21)
        Me.BunifuImageButton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.BunifuImageButton1.TabIndex = 28
        Me.BunifuImageButton1.TabStop = False
        Me.BunifuImageButton1.Zoom = 10
        '
        'BunifuDragControl1
        '
        Me.BunifuDragControl1.Fixed = True
        Me.BunifuDragControl1.Horizontal = True
        Me.BunifuDragControl1.TargetControl = Me.Panel1
        Me.BunifuDragControl1.Vertical = True
        '
        'AddSupplier
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(360, 500)
        Me.Controls.Add(Me.AddAnotherSupplier)
        Me.Controls.Add(Me.AddModSupplier)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.VilleFournisseur_TXB)
        Me.Controls.Add(Me.TelephoneFournisseur_TXB)
        Me.Controls.Add(Me.EmailFournisseur_TXB)
        Me.Controls.Add(Me.AdresseFournisseur_TXB)
        Me.Controls.Add(Me.SocieteFournisseur_TXB)
        Me.Controls.Add(Me.NomFournisseur_TXB)
        Me.Controls.Add(Me.PrenomFournisseur_TXB)
        Me.Controls.Add(Me.BunifuImageButton1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "AddSupplier"
        Me.Text = "AddSupplier"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.BunifuImageButton2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BunifuImageButton1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents PrenomFournisseur_TXB As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents BunifuImageButton1 As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents NomFournisseur_TXB As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents SocieteFournisseur_TXB As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents AdresseFournisseur_TXB As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents EmailFournisseur_TXB As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents TelephoneFournisseur_TXB As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents VilleFournisseur_TXB As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents BunifuCustomLabel4 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents AddAnotherSupplier As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents AddModSupplier As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents BunifuImageButton2 As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents BunifuDragControl1 As Bunifu.Framework.UI.BunifuDragControl
End Class
