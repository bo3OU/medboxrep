﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PayerCommande
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CloseThisForm = New Bunifu.Framework.UI.BunifuImageButton()
        Me.BunifuCustomLabel1 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.BunifuCustomLabel2 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.PaymentTypes_CMB = New System.Windows.Forms.ComboBox()
        Me.PayAll = New System.Windows.Forms.RadioButton()
        Me.PayPartial = New System.Windows.Forms.RadioButton()
        Me.RemoveQuantityNumber = New Bunifu.Framework.UI.BunifuImageButton()
        Me.AddQuantityNumber = New Bunifu.Framework.UI.BunifuImageButton()
        Me.MoneyToPay = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.Payer = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.PriceLeft_LBL = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.TotalPrice = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.CommandeDetails = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.AlreadyPaid = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.BunifuDragControl1 = New Bunifu.Framework.UI.BunifuDragControl(Me.components)
        Me.Panel1.SuspendLayout()
        CType(Me.CloseThisForm, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RemoveQuantityNumber, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AddQuantityNumber, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Panel1.Controls.Add(Me.CloseThisForm)
        Me.Panel1.Controls.Add(Me.BunifuCustomLabel1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(360, 100)
        Me.Panel1.TabIndex = 0
        '
        'CloseThisForm
        '
        Me.CloseThisForm.BackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.CloseThisForm.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Delete
        Me.CloseThisForm.ImageActive = Nothing
        Me.CloseThisForm.Location = New System.Drawing.Point(330, 0)
        Me.CloseThisForm.Name = "CloseThisForm"
        Me.CloseThisForm.Size = New System.Drawing.Size(30, 27)
        Me.CloseThisForm.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.CloseThisForm.TabIndex = 17
        Me.CloseThisForm.TabStop = False
        Me.CloseThisForm.Zoom = 10
        '
        'BunifuCustomLabel1
        '
        Me.BunifuCustomLabel1.AutoSize = True
        Me.BunifuCustomLabel1.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel1.ForeColor = System.Drawing.Color.White
        Me.BunifuCustomLabel1.Location = New System.Drawing.Point(92, 40)
        Me.BunifuCustomLabel1.Name = "BunifuCustomLabel1"
        Me.BunifuCustomLabel1.Size = New System.Drawing.Size(180, 23)
        Me.BunifuCustomLabel1.TabIndex = 0
        Me.BunifuCustomLabel1.Text = "Payer Commande"
        '
        'BunifuCustomLabel2
        '
        Me.BunifuCustomLabel2.AutoSize = True
        Me.BunifuCustomLabel2.Location = New System.Drawing.Point(65, 191)
        Me.BunifuCustomLabel2.Name = "BunifuCustomLabel2"
        Me.BunifuCustomLabel2.Size = New System.Drawing.Size(120, 13)
        Me.BunifuCustomLabel2.TabIndex = 19
        Me.BunifuCustomLabel2.Text = "Methode de Payement :"
        '
        'PaymentTypes_CMB
        '
        Me.PaymentTypes_CMB.BackColor = System.Drawing.SystemColors.Window
        Me.PaymentTypes_CMB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.PaymentTypes_CMB.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PaymentTypes_CMB.FormattingEnabled = True
        Me.PaymentTypes_CMB.ItemHeight = 22
        Me.PaymentTypes_CMB.Location = New System.Drawing.Point(68, 216)
        Me.PaymentTypes_CMB.Name = "PaymentTypes_CMB"
        Me.PaymentTypes_CMB.Size = New System.Drawing.Size(217, 30)
        Me.PaymentTypes_CMB.TabIndex = 18
        '
        'PayAll
        '
        Me.PayAll.AutoSize = True
        Me.PayAll.Location = New System.Drawing.Point(107, 281)
        Me.PayAll.Name = "PayAll"
        Me.PayAll.Size = New System.Drawing.Size(78, 17)
        Me.PayAll.TabIndex = 20
        Me.PayAll.TabStop = True
        Me.PayAll.Text = "Tous payer"
        Me.PayAll.UseVisualStyleBackColor = True
        '
        'PayPartial
        '
        Me.PayPartial.AutoSize = True
        Me.PayPartial.Location = New System.Drawing.Point(107, 333)
        Me.PayPartial.Name = "PayPartial"
        Me.PayPartial.Size = New System.Drawing.Size(14, 13)
        Me.PayPartial.TabIndex = 21
        Me.PayPartial.TabStop = True
        Me.PayPartial.UseVisualStyleBackColor = True
        '
        'RemoveQuantityNumber
        '
        Me.RemoveQuantityNumber.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.RemoveQuantityNumber.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Cancel
        Me.RemoveQuantityNumber.ImageActive = Nothing
        Me.RemoveQuantityNumber.Location = New System.Drawing.Point(136, 328)
        Me.RemoveQuantityNumber.Name = "RemoveQuantityNumber"
        Me.RemoveQuantityNumber.Size = New System.Drawing.Size(25, 25)
        Me.RemoveQuantityNumber.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.RemoveQuantityNumber.TabIndex = 24
        Me.RemoveQuantityNumber.TabStop = False
        Me.RemoveQuantityNumber.Zoom = 10
        '
        'AddQuantityNumber
        '
        Me.AddQuantityNumber.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.AddQuantityNumber.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Add
        Me.AddQuantityNumber.ImageActive = Nothing
        Me.AddQuantityNumber.Location = New System.Drawing.Point(229, 328)
        Me.AddQuantityNumber.Name = "AddQuantityNumber"
        Me.AddQuantityNumber.Size = New System.Drawing.Size(25, 25)
        Me.AddQuantityNumber.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.AddQuantityNumber.TabIndex = 23
        Me.AddQuantityNumber.TabStop = False
        Me.AddQuantityNumber.Zoom = 10
        '
        'MoneyToPay
        '
        Me.MoneyToPay.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.MoneyToPay.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.MoneyToPay.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.MoneyToPay.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.MoneyToPay.BorderThickness = 3
        Me.MoneyToPay.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.MoneyToPay.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MoneyToPay.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.MoneyToPay.isPassword = False
        Me.MoneyToPay.Location = New System.Drawing.Point(168, 318)
        Me.MoneyToPay.Margin = New System.Windows.Forms.Padding(4)
        Me.MoneyToPay.Name = "MoneyToPay"
        Me.MoneyToPay.Size = New System.Drawing.Size(54, 44)
        Me.MoneyToPay.TabIndex = 22
        Me.MoneyToPay.Text = "1"
        Me.MoneyToPay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Payer
        '
        Me.Payer.Activecolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.Payer.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.Payer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Payer.BorderRadius = 0
        Me.Payer.ButtonText = "Payer"
        Me.Payer.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Payer.DisabledColor = System.Drawing.Color.Gray
        Me.Payer.Iconcolor = System.Drawing.Color.Transparent
        Me.Payer.Iconimage = Global.GestionDeStock.My.Resources.Resources.icons8_Shopping_Cart
        Me.Payer.Iconimage_right = Nothing
        Me.Payer.Iconimage_right_Selected = Nothing
        Me.Payer.Iconimage_Selected = Nothing
        Me.Payer.IconMarginLeft = 0
        Me.Payer.IconMarginRight = 0
        Me.Payer.IconRightVisible = True
        Me.Payer.IconRightZoom = 0R
        Me.Payer.IconVisible = True
        Me.Payer.IconZoom = 90.0R
        Me.Payer.IsTab = False
        Me.Payer.Location = New System.Drawing.Point(245, 440)
        Me.Payer.Name = "Payer"
        Me.Payer.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.Payer.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.Payer.OnHoverTextColor = System.Drawing.Color.White
        Me.Payer.selected = False
        Me.Payer.Size = New System.Drawing.Size(103, 48)
        Me.Payer.TabIndex = 25
        Me.Payer.Text = "Payer"
        Me.Payer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Payer.Textcolor = System.Drawing.Color.White
        Me.Payer.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'PriceLeft_LBL
        '
        Me.PriceLeft_LBL.AutoSize = True
        Me.PriceLeft_LBL.Location = New System.Drawing.Point(154, 398)
        Me.PriceLeft_LBL.Name = "PriceLeft_LBL"
        Me.PriceLeft_LBL.Size = New System.Drawing.Size(44, 13)
        Me.PriceLeft_LBL.TabIndex = 26
        Me.PriceLeft_LBL.Text = "Restant"
        Me.PriceLeft_LBL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TotalPrice
        '
        Me.TotalPrice.AutoSize = True
        Me.TotalPrice.Location = New System.Drawing.Point(301, 103)
        Me.TotalPrice.Name = "TotalPrice"
        Me.TotalPrice.Size = New System.Drawing.Size(47, 13)
        Me.TotalPrice.TabIndex = 27
        Me.TotalPrice.Text = "Prix total"
        Me.TotalPrice.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CommandeDetails
        '
        Me.CommandeDetails.AutoSize = True
        Me.CommandeDetails.Location = New System.Drawing.Point(12, 117)
        Me.CommandeDetails.Name = "CommandeDetails"
        Me.CommandeDetails.Size = New System.Drawing.Size(120, 13)
        Me.CommandeDetails.TabIndex = 28
        Me.CommandeDetails.Text = "Details de la commande"
        '
        'AlreadyPaid
        '
        Me.AlreadyPaid.AutoSize = True
        Me.AlreadyPaid.Location = New System.Drawing.Point(145, 456)
        Me.AlreadyPaid.Name = "AlreadyPaid"
        Me.AlreadyPaid.Size = New System.Drawing.Size(62, 13)
        Me.AlreadyPaid.TabIndex = 29
        Me.AlreadyPaid.Text = "Deja Payé !"
        Me.AlreadyPaid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AlreadyPaid.Visible = False
        '
        'BunifuDragControl1
        '
        Me.BunifuDragControl1.Fixed = True
        Me.BunifuDragControl1.Horizontal = True
        Me.BunifuDragControl1.TargetControl = Me.Panel1
        Me.BunifuDragControl1.Vertical = True
        '
        'PayerCommande
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(360, 500)
        Me.Controls.Add(Me.AlreadyPaid)
        Me.Controls.Add(Me.CommandeDetails)
        Me.Controls.Add(Me.TotalPrice)
        Me.Controls.Add(Me.PriceLeft_LBL)
        Me.Controls.Add(Me.Payer)
        Me.Controls.Add(Me.RemoveQuantityNumber)
        Me.Controls.Add(Me.AddQuantityNumber)
        Me.Controls.Add(Me.MoneyToPay)
        Me.Controls.Add(Me.PayPartial)
        Me.Controls.Add(Me.PayAll)
        Me.Controls.Add(Me.BunifuCustomLabel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.PaymentTypes_CMB)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "PayerCommande"
        Me.Text = "PayerCommande"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CloseThisForm, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RemoveQuantityNumber, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AddQuantityNumber, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents BunifuCustomLabel1 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents CloseThisForm As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents BunifuCustomLabel2 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents PaymentTypes_CMB As ComboBox
    Friend WithEvents PayAll As RadioButton
    Friend WithEvents PayPartial As RadioButton
    Friend WithEvents RemoveQuantityNumber As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents AddQuantityNumber As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents MoneyToPay As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents Payer As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents PriceLeft_LBL As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents TotalPrice As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents CommandeDetails As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents AlreadyPaid As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents BunifuDragControl1 As Bunifu.Framework.UI.BunifuDragControl
End Class
