﻿Imports System.Globalization

Public Class MainForm
    Private i As Integer
    Private FormLoad As Boolean = False
    Private allowCoolMove As Boolean = False
    Private myCoolPoint As New Point

    'COMMANDES
    Private itemsvect As New Dictionary(Of Integer, String)
    Private MainDataTable As New DataTable()
    Private FilterParams As New Dictionary(Of String, String)
    Private datebool As Boolean = False
    Private MaxPrice As Integer
    'STOCK
    Friend ViewHandler

    'ARTICLES

    'ORDONNANCE
    Private IdInsertedPrescription As Integer = -1
    Private Typesvect As New Dictionary(Of Integer, Integer)
    Private Posologievect As New Dictionary(Of Integer, Integer)
    Private medsvect As New Dictionary(Of Integer, Integer)
    Private NumberOfMeds As Integer = 1
    Private NumberOfPosologie As Integer = -1
    Private chosenmeds As New Dictionary(Of Integer, Integer)
    Private CurrentMPID As Integer
    Private MPIDvect As New Dictionary(Of Integer, Integer)

    Private possi As Integer
    Private medsi As Integer


    Private Structure MedLine
        Public MedId As Integer
        Public PosoId As Integer
        Public Med As String
        Public Poso As String
    End Structure

    Private Meds As New List(Of MedLine)
    Private OrdonnanceId As Integer = -1


    Public Sub BringIndicator(sender As Object, e As EventArgs) Handles SubMenuButton_1_BTN.Click, SubMenuButton_2_BTN.Click, SubMenuButton_5_BTN.Click, SubMenuButton_4_BTN.Click, SubMenuButton_3_BTN.Click
        'BRINGS THE LITTLE MENU INDICATOR ( WHITE BOX ) UNDER TITLE
        Indicator.Location = New Point(DirectCast(sender, Bunifu.Framework.UI.BunifuTileButton).Location.X, DirectCast(sender, Bunifu.Framework.UI.BunifuTileButton).Location.Y + 82)
        'SHOW APPROPRIATE PANEL
        ChosenAction = DirectCast(sender, Bunifu.Framework.UI.BunifuTileButton).Tag
        Select Case ChosenMenu
            Case 0

            Case 1
#Region "STOCK MAIN MENU"


                'STOCK MENU
                Select Case ChosenAction
                    Case 0
#Region "SUB MENU ORDERS"
                        'ORDERS
                        FilterParams.Clear()
                        'FOR DATAGRIDVIEW
                        Access.AddParam("@directeur", SESSION.GetDirecteur())
                        Access.ExecQuery("select DISTINCT 
                                        commande.CO_ID_COMMANDE as id,
                                        EMPLOYE.EM_PRENOM & ' ' & EMPLOYE.EM_NOM as Employe,
                                        ARTICLE.AR_NOM as Article,
                                        commande.CO_QUANTITE &' '& UNITE.UN_NOM as Quantite, 
                                        ETAT.ET_NOM as Etat,
                                        commande.CO_DATE_COMMANDE as [Date],
                                        FOURNISSEUR.FRS_PRENOM & ' ' & FOURNISSEUR.FRS_NOM as Fournisseur,
                                        fourni.FO_PRIX & 'MAD' as Prix,
                                        fourni.FO_PRIX * commande.CO_QUANTITE & 'MAD' as [Montant] 
                                        from ((((((commande 
                                        Left JOIN etat On commande.CO_ETAT = etat.ET_ID_ETAT ) 
                                        Left JOIN employe On employe.EM_ID_EMPLOYE = commande.CO_EMPLOYE) 
                                        LEFT JOIN fourni On fourni.FO_ID_FOURNI = commande.CO_FOURNI )
                                        LEFT JOIN fournisseur On fournisseur.FRS_ID_FOURNISSEUR = fourni.FO_FOURNISSEUR ) 
                                        LEFT JOIN article On article.AR_ID_ARTICLE = fourni.FO_ARTICLE ) 
                                        LEFT JOIN unite On unite.UN_ID_UNITE = article.AR_UNITE ) 
                                        where  commande.co_employe = ANY (Select em_id_employe from employe where em_directeur = @directeur)
                                        And CO_etat = 1
                                        ORDER BY commande.co_DATE_COMMANDE ASC")
                        If NotEmpty(Access.Exception) Then MsgBox(Access.Exception) : Exit Sub
                        'Maybe add line by line so i can store Ids
                        MainDataTable = Access.DBDT
                        dgv1.DataSource = Access.DBDT

                        'For Each r As DataGridViewRow In dgv1.Rows
                        '    MsgBox(r.Cells(5).Value)
                        '    Dim oDate As DateTime = Date.ParseExact(r.Cells(5).Value, "dd/MM/yyyy", Nothing)
                        '    r.Cells(5).Value = oDate.ToShortDateString()
                        '    If oDate.CompareTo(oDate) = 1 Then
                        '        dgv1.DataSource.RowFilter = "#" + De_DTP.ToString("dd/MM/yyyy") + "# < EndDate OR EndDate = #1/1/1900#";
                        '            FilterParams.Add("", "")
                        '    End If
                        'Next

                        dgv1.Columns("id").Visible = False
                        'TODO date filter
                        FilterParams.Add("Article", "  Like '%'")
                        FilterParams.Add("Fournisseur", " LIKE '%'")
                        De_DTP.MaxDate = Date.Today
                        Au_DTP.MaxDate = Date.Today
                        FilterParams.Add("date_debut", "")
                        FilterParams.Add("date_fin", "")
                        datebool = True


                        'check how to make the % of the date
                        'TODO find a way to inverse the mm/dd/yyyy to dd/mm/yyyy !



                        'TODO for the price range;, range doesn't work! maybe slider


                        'Clearing the dropdown items and indexes for new entry 
                        Rechercher_Fournisseur_CMB.Items.Clear()
                        Rechercher_Fournisseur_CMB.SelectedIndex = -1
                        'FILLING Suppliers Dropdown
                        Access.AddParam("@Cabinet", SESSION.GetCabinet())
                        Access.ExecQuery("Select DISTINCT fournisseur.FRS_ID_FOURNISSEUR As id, Fournisseur.FRS_PRENOM & ' ' & Fournisseur.FRS_NOM as line
                            from ((commande
                            LEFT JOIN fourni ON fourni.FO_ID_FOURNI = commande.CO_FOURNI)
                            Left JOIN FOURNISSEUR ON fournisseur.FRS_ID_FOURNISSEUR = fourni.FO_FOURNISSEUR)
                            where FO_CABINET = @cabinet")

                        If NotEmpty(Access.Exception) Then MsgBox(Access.Exception) : Exit Sub
                        Rechercher_Fournisseur_CMB.Items.Add("Fournisseurs : Tous")
                        i = 1
                        For Each r As DataRow In Access.DBDT.Rows
                            Rechercher_Fournisseur_CMB.Items.Add(r.Item("line"))
                            i += 1
                        Next

                        'FOR DATETIMEPICKER
                        'TODO

                        'TODO dashboard data 
                        Access.ExecQuery("
                            select  sum(fourni.FO_PRIX*commande.CO_QUANTITE) - sum((Paiement.PA_pourcentage/100)* fourni.FO_PRIX*CO_QUANTITE) as line
                            from ((commande 
                            left join FOURNI on fourni.FO_ID_FOURNI = commande.CO_FOURNI)
                            left join paiement on paiement.PA_COMMANDE = commande.CO_ID_COMMANDE)
                            where fourni.FO_CABINET = " & SESSION.GetCabinet())
                        If NotEmpty(Access.Exception) Then
                            MsgBox(Access.Exception)
                        Else
                            If Access.DBDT.Rows.Count > 0 Then
                                TotalMoneyToSuppliers.Text = Access.DBDT.Rows(0).Item("line") & " MAD"
                                UnpaidOrders.Text = " a payer aux fournisseurs."
                            Else
                                TotalMoneyToSuppliers.Text = "0 MAD"
                                UnpaidOrders.Text = " a payer aux fournisseurs"
                            End If
                        End If

                        'SHOW PANEL
                        SwitchPanels(PNL_Stock_Commande)

#End Region
                    Case 2
#Region "SUB MENU STOCK"
                        FilterParams.Clear()
                        'STOCK
                        Access.AddParam("@cabinet", SESSION.GetCabinet())
                        Access.ExecQuery("select DISTINCT 
                                        stock.ST_ID_STOCK as id,
                                        IIF(stock.ST_QUANTITE <= ARTICLE.AR_ALERTE_MIN
                                                ,0 
                                                ,IIF(stock.ST_QUANTITE >= ARTICLE.AR_ALERTE_MAX
                                                    ,2
                                                    ,1)) as [alert],
                                        article.AR_NOM as Article,
                                        stock.ST_QUANTITE &' '& UNITE.UN_NOM as Quantite,
                                        ARTICLE.AR_ALERTE_MIN as [Alerte Minimum],
                                        ARTICLE.AR_ALERTE_MAX as [Alerte Maximum]
                                        from ((STOCK
                                        LEFT JOIN ARTICLE on article.AR_ID_ARTICLE = stock.ST_ARTICLE)
                                        LEFT JOIN UNITE on UNITE.UN_ID_UNITE = ARTICLE.AR_UNITE)
                                        where stock.ST_CABINET =" & SESSION.GetCabinet() & " 
                                        and stock.ST_QUANTITE <> 0")
                        If NotEmpty(Access.Exception) Then MsgBox(Access.Exception) : Exit Sub
                        Stock_DGV.DataSource = Access.DBDT
                        Stock_DGV.Columns(0).Visible = False
                        Stock_DGV.Columns(1).Visible = False
                        Stock_DGV.Columns(4).Visible = False
                        Stock_DGV.Columns(5).Visible = False

                        i = 0
                        'for stock dashboard
                        Dim low As Integer = 0
                        Dim high As Integer = 0


                        For Each r As DataGridViewRow In Stock_DGV.Rows
                            If Val(r.Cells("alert").Value) = 1 Then
                                r.DefaultCellStyle.BackColor = Color.White
                            ElseIf Val(r.Cells("alert").Value) = 0 Then
                                low += 1
                                r.DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 204)
                            Else
                                high += 1
                                r.DefaultCellStyle.BackColor = Color.FromArgb(255, 153, 153)
                            End If
                            i += 1
                        Next
                        FilterParams.Add("Article", " Like '%'")
                        Low_LBL.Text = low
                        High_LBL.Text = high
                        SwitchPanels(PNL_Stock_Stock)

#End Region
                    Case 3
#Region "SUB MENU ARTICLES"

                        FilterParams.Clear()
                        'ARTICLES
                        Access.AddParam("@cabinet", SESSION.GetCabinet())
                        Access.ExecQuery("select DISTINCT 
                                        fourni.FO_ARTICLE as ID,
                                        article.AR_NOM as Article,
                                        UNITE.UN_NOM as Unite,
                                        article.AR_ALERTE_MIN as [Alerte Min],
                                        article.AR_ALERTE_MAX as [Alerte Max]
                                        from ((FOURNI
                                        LEFT JOIN article ON fourni.FO_ARTICLE = article.AR_ID_ARTICLE)
                                        LEFT JOIN unite ON unite.UN_ID_UNITE = article.AR_UNITE)
                                        where fourni.FO_CABINET = @cabinet
                                        and fourni.FO_FOURNISSEUR IS NULL")
                        If NotEmpty(Access.Exception) Then MsgBox(Access.Exception) : Exit Sub
                        MainDataTable = Access.DBDT
                        ListOfArticles_DGV.DataSource = Access.DBDT
                        ListOfArticles_DGV.Columns(0).Visible = False

                        FilterParams.Add("Article", " LIKE '%'")

                        SwitchPanels(PNL_Stock_Articles)
#End Region
                    Case 4
#Region "SUB MENU SUPPLIER"

                        FilterParams.Clear()

                        'ARTICLES
                        Access.AddParam("@cabinet", SESSION.GetCabinet())
                        Access.ExecQuery("select DISTINCT 
                                        fourni.FO_FOURNISSEUR as id,
                                        FRS_PRENOM as Prenom,
                                        FRS_NOM as Nom,
                                        FRS_ADRESSE as Adresse,
                                        FRS_VILLE as Ville,
                                        FRS_TEL as Telephone, 
                                        FRS_EMAIL as [E-mail]
                                        from FOURNI
                                        LEFT JOIN FOURNISSEUR ON fournisseur.FRS_ID_FOURNISSEUR = fourni.FO_FOURNISSEUR
                                        where FO_CABINET = @cabinet
                                        and fourni.FO_ARTICLE IS NULL
                                        ")
                        If NotEmpty(Access.Exception) Then MsgBox(Access.Exception) : Exit Sub

                        MainDataTable = Access.DBDT
                        SuppliersList_DGV.DataSource = Access.DBDT
                        SuppliersList_DGV.Columns(0).Visible = False

                        FilterParams.Add("Nom", " LIKE '%'")
                        FilterParams.Add("Prenom", " LIKE '%'")
                        FilterParams.Add("Telephone", " LIKE '%'")
                        FilterParams.Add("Ville", " LIKE '%'")
                        FilterParams.Add("Adresse", " LIKE '%'")


                        SwitchPanels(PNL_Stock_Suppliers)
#End Region

                End Select
#End Region
            Case 2
#Region "ORDONNANCE MAIN MENU"
                If SESSION.IsDoctor() = True Then
                    'for types
                    ListeType_CMB.SelectedIndex = -1
                    ListeMedicament_CMB.SelectedIndex = -1
                    ListePosologie_CMB.SelectedIndex = -1
                    ListeType_CMB.Items.Clear()
                    ListeMedicament_CMB.Items.Clear()
                    ListePosologie_CMB.Items.Clear()
                    Typesvect.Clear()
                    MPIDvect.Clear()
                    Access.ExecQuery("select DISTINCT TY_ID as ID,
                                    TY_NOM as name
                                    from ((MEDICAMENT_POSOLOGIE 
                                    LEFT JOIN Medicament ON MEDICAMENT.ME_ID_MEDICAMENT = MEDICAMENT_POSOLOGIE.MP_MEDICAMENT)
                                    LEFT JOIN type_medicament ON TYPE_MEDICAMENT.TY_ID = MEDICAMENT.ME_TYPE_MEDICAMENT)
                                    where MP_Docteur = " & SESSION.GetUser() & " and
                                    MP_POSOLOGIE IS NULL")
                    If NotEmpty(Access.Exception) Then
                        MsgBox(Access.Exception)
                    End If
                    i = 0
                    For Each r As DataRow In Access.DBDT.Rows
                        If Not r.Item("ID").Equals(DBNull.Value) And Not r.Item("name").Equals(DBNull.Value) Then
                            Typesvect.Add(i, r.Item("ID"))
                            ListeType_CMB.Items.Add(r.Item("name"))
                            i += 1
                        End If
                    Next
                    SwitchPanels(PNL_Ordonnance)
                Else
                    MsgBox("vous n'avez pas assez de permissions pour creer des ordonnances !")
                End If
#End Region
            Case 3
            Case 4
            Case 5
            Case 6
        End Select
    End Sub




#Region "ORDERS AREA"
    'commandes : search section
    Private Sub Rechercher_Code_TXB_OnValueChanged(sender As Object, e As EventArgs) Handles Rechercher_Code_TXB.OnValueChanged
        If FormLoad Then
            If Trim(Rechercher_Code_TXB.Text) <> "Nom D'Article" Then
                FilterParams("Article") = " Like '%" & Trim(Rechercher_Code_TXB.Text) & "%'"
                filterData(dgv1, FilterParams)
            End If
        End If
    End Sub

    Private Sub Rechercher_Fournisseur_CMB_SelectedIndexChanged(sender As Object, e As EventArgs) Handles Rechercher_Fournisseur_CMB.SelectedIndexChanged
        If FormLoad Then
            If Rechercher_Fournisseur_CMB.Text = "Fournisseurs : Tous" Then
                FilterParams("Fournisseur") = " Like '%'"
            Else
                FilterParams("Fournisseur") = " Like '%" & Trim(Rechercher_Fournisseur_CMB.Text) & "%'"
            End If
            'MsgBox(FilterParams("Fournisseur").ToString)
            filterData(dgv1, FilterParams)
        End If
    End Sub

    Private Sub UseDateInSearch_Click(sender As Object, e As EventArgs) Handles UseDateInSearch.Click
        If UseDateInSearch.Value = False Then
            FilterParams("date_debut") = ""
            FilterParams("date_fin") = ""
            filterData(dgv1, FilterParams)
        End If
    End Sub

    Private Sub De_DTP_ValueChanged(sender As Object, e As EventArgs) Handles De_DTP.ValueChanged
        If FormLoad And datebool Then
            Dim date_debut As New DateTime
            date_debut = DateTime.ParseExact(De_DTP.Value, "dd/MM/yyyy", New CultureInfo("fr-FR"))

            If UseDateInSearch.Value = False Then
                'TODO check if this works
                FilterParams("date_debut") = ""
            Else
                FilterParams("date_debut") = " > #" & date_debut.ToShortDateString() & "#"
            End If
            filterData(dgv1, FilterParams)
        End If
    End Sub

    Private Sub Au_DTP_ValueChanged(sender As Object, e As EventArgs) Handles Au_DTP.ValueChanged
        If FormLoad And datebool Then
            Dim date_fin As New DateTime
            date_fin = DateTime.ParseExact(Au_DTP.Value, "dd/MM/yyyy", New CultureInfo("fr-FR"))

            If UseDateInSearch.Value = False Then
                'TODO check if this works
                FilterParams("date_debut") = ""
            Else
                FilterParams("date_debut") = " > #" & date_fin.ToShortDateString() & "#"
            End If
            filterData(dgv1, FilterParams)
        End If
    End Sub

    Private Sub Delete_Order_Orders_BTN_Click(sender As Object, e As EventArgs) Handles Delete_Order_Oders_BTN.Click
        If dgv1.SelectedRows.Count > 0 Then
            Dim result As Integer = MessageBox.Show("ATTENTION ! Cette action est irreversible," &
                Environment.NewLine & "voulez vous vraiment supprimer cette Commande?" &
                Environment.NewLine & Environment.NewLine & "Ceci supprimera tous les paiements concernant cette commande!",
                "caption", MessageBoxButtons.YesNo)
            If result = DialogResult.Yes Then
                'TODO REVIEW
                Access.AddParam("@id", dgv1.Rows(dgv1.SelectedRows(0).Index).Cells(0).Value)
                Access.ExecQuery("delete * from commande where CO_ID_COMMANDE = @id")
                BringIndicator(SubMenuButton_1_BTN, New EventArgs)
            End If
        End If

    End Sub

    'add button in commandes
    Private Sub BunifuImageButton1_Click_1(sender As Object, e As EventArgs) Handles Add_Order_Oders_BTN.Click
        Dim add As New AddOrder()
        add.Filling()
        add.ShowDialog()
    End Sub

    Private Sub Rechercher_Fournisseur_TXB_Enter(sender As Object, e As EventArgs) Handles Rechercher_Code_TXB.Enter
        'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
        If Trim(Rechercher_Code_TXB.Text) = "Nom D'Article" Then
            Rechercher_Code_TXB.Text = ""
        End If
    End Sub




#End Region

#Region "STOCK AREA"
    'Stock : search section
    Private Sub article_name_TXB_OnValueChanged(sender As Object, e As EventArgs) Handles article_name_TXB.OnValueChanged
        If FormLoad Then
            If Trim(article_name_TXB.Text) <> "Nom D'Article" Then
                FilterParams("article") = " Like '%" & Trim(article_name_TXB.Text) & "%'"
                filterData(Stock_DGV, FilterParams)
            End If
        End If
    End Sub

    Private Sub Stock_DeleteOrder_Click(sender As Object, e As EventArgs)
        BringIndicator(SubMenuButton_3_BTN, New EventArgs)
    End Sub

#End Region

#Region "ARTICLES AREA"
    'Articles
    Private Sub Articles_DeleteArticle_Click(sender As Object, e As EventArgs)
        'TODO review this
        Access.AddParam("@id", ListOfArticles_DGV.Rows(ListOfArticles_DGV.SelectedRows(0).Index).Cells(0).Value)
        Access.ExecQuery("delete * from ARTICLE where AR_ID_ARTICLE= @id")
        BringIndicator(SubMenuButton_4_BTN, New EventArgs)
    End Sub

    Private Sub Modify_Article_BTN_Click(sender As Object, e As EventArgs) Handles Modify_Article_BTN.Click
        Dim a As New AddArticle()
        If a.FillToModify(ListOfArticles_DGV.Rows(ListOfArticles_DGV.SelectedRows(0).Index).Cells(0).Value) Then
            a.ShowDialog()
        Else
            MsgBox("erreur !")
        End If

    End Sub

    Private Sub Stock_Articles_Articles_TXB_OnValueChanged(sender As Object, e As EventArgs) Handles Stock_Articles_Articles_TXB.OnValueChanged
        If FormLoad Then
            If Trim(Stock_Articles_Articles_TXB.Text) <> "Nom D'Article" Then
                FilterParams("Article") = " Like '%" & Trim(Stock_Articles_Articles_TXB.Text) & "%'"
                filterData(ListOfArticles_DGV, FilterParams)
            End If
        End If
    End Sub
#End Region

#Region "SUPPLIER AREA"

    Private Sub Suppliers_DeleteSupplierClick(sender As Object, e As EventArgs) Handles Suppliers_DeleteSupplier.Click
        'TODO review this
        If SuppliersList_DGV.SelectedRows.Count > 0 Then
            Dim result As Integer = MessageBox.Show("ATTENTION ! Cette action est irreversible," &
                Environment.NewLine & "voulez vous vraiment supprimer cette Commande?" &
                Environment.NewLine & Environment.NewLine & "Ceci supprimera toutes les commandes, produits en stock, et paiements en relation avec ce fournisseur!",
                "caption", MessageBoxButtons.YesNo)
            If result = DialogResult.Yes Then
                'TODO REVIEW
                Access.AddParam("@id", SuppliersList_DGV.Rows(SuppliersList_DGV.SelectedRows(0).Index).Cells(0).Value)
                Access.ExecQuery("delete * from FOURNISSEUR where FRS_ID_FOURNISSEUR = @id")
                BringIndicator(SubMenuButton_5_BTN, New EventArgs)
            ElseIf result = DialogResult.No Then

            End If
        End If


    End Sub

    '1
    Private Sub SupplierName_Enter(sender As Object, e As EventArgs) Handles SupplierName.Enter
        If Trim(SupplierName.Text) = "Prenom du Fournisseur" Then
            SupplierName.Text = ""
        End If
    End Sub

    Private Sub SupplierName_Leave(sender As Object, e As EventArgs) Handles SupplierName.Leave
        'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
        If Trim(SupplierName.Text) = "" Then
            SupplierName.Text = "Prenom du Fournisseur"
        End If
    End Sub

    Private Sub SupplierName_OnValueChanged(sender As Object, e As EventArgs) Handles SupplierName.OnValueChanged
        If FormLoad Then
            If Trim(SupplierName.Text) <> "Prenom du Fournisseur" Then
                FilterParams("Prenom") = " Like '%" & Trim(SupplierName.Text) & "%'"
                filterData(SuppliersList_DGV, FilterParams)
            End If
        End If
    End Sub
    '2
    Private Sub SupplierLastNameEnter(sender As Object, e As EventArgs) Handles SupplierLastName.Enter
        If Trim(SupplierLastName.Text) = "Nom du Fournisseur" Then
            SupplierLastName.Text = ""
        End If
    End Sub

    Private Sub SupplierLastNameLeave(sender As Object, e As EventArgs) Handles SupplierLastName.Leave
        'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
        If Trim(SupplierLastName.Text) = "" Then
            SupplierLastName.Text = "Nom du Fournisseur"
        End If
    End Sub

    Private Sub SupplierLastNameOnValueChanged(sender As Object, e As EventArgs) Handles SupplierLastName.OnValueChanged
        If FormLoad Then
            If Trim(SupplierLastName.Text) <> "Nom du Fournisseur" Then
                FilterParams("Nom") = " Like '%" & Trim(SupplierLastName.Text) & "%'"
                filterData(SuppliersList_DGV, FilterParams)
            End If
        End If
    End Sub
    '3
    Private Sub SupplierAddresseEnter(sender As Object, e As EventArgs) Handles SupplierAddresse.Enter
        If Trim(SupplierAddresse.Text) = "Adresse du Founisseur" Then
            SupplierAddresse.Text = ""
        End If
    End Sub

    Private Sub SupplierAddresseLeave(sender As Object, e As EventArgs) Handles SupplierAddresse.Leave
        'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
        If Trim(SupplierAddresse.Text) = "" Then
            SupplierAddresse.Text = "Adresse du Founisseur"
        End If
    End Sub

    Private Sub SupplierAddresseOnValueChanged(sender As Object, e As EventArgs) Handles SupplierAddresse.OnValueChanged
        If FormLoad Then
            If Trim(SupplierAddresse.Text) <> "Adresse du Founisseur" Then
                FilterParams("adresse") = " Like '%" & Trim(SupplierAddresse.Text) & "%'"
                filterData(SuppliersList_DGV, FilterParams)
            End If
        End If
    End Sub
    '4
    Private Sub SuppliercityEnter(sender As Object, e As EventArgs) Handles Suppliercity.Enter
        If Trim(Suppliercity.Text) = "Ville du Founisseur" Then
            Suppliercity.Text = ""
        End If
    End Sub

    Private Sub SuppliercityLeave(sender As Object, e As EventArgs) Handles Suppliercity.Leave
        'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
        If Trim(Suppliercity.Text) = "" Then
            Suppliercity.Text = "Ville du Founisseur"
        End If
    End Sub

    Private Sub SuppliercityOnValueChanged(sender As Object, e As EventArgs) Handles Suppliercity.OnValueChanged
        If FormLoad Then

            If Trim(Suppliercity.Text) <> "Ville du Founisseur" Then
                FilterParams("Ville") = " Like '%" & Trim(Suppliercity.Text) & "%'"
                filterData(SuppliersList_DGV, FilterParams)
            End If
        End If
    End Sub

    '5
    Private Sub SupplierPhoneEnter(sender As Object, e As EventArgs) Handles SupplierPhone.Enter
        If Trim(SupplierPhone.Text) = "Telephone du Fournisseur" Then
            SupplierPhone.Text = ""
        End If
    End Sub

    Private Sub SupplierPhoneLeave(sender As Object, e As EventArgs) Handles SupplierPhone.Leave
        'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
        If Trim(SupplierPhone.Text) = "" Then
            SupplierPhone.Text = "Telephone du Fournisseur"
        End If
    End Sub

    Private Sub SupplierPhoneOnValueChanged(sender As Object, e As EventArgs) Handles SupplierPhone.OnValueChanged
        If FormLoad Then

            If Trim(SupplierPhone.Text) <> "Telephone du Fournisseur" Then
                FilterParams("Telephone") = " Like '%" & Trim(SupplierPhone.Text) & "%'"
                filterData(SuppliersList_DGV, FilterParams)
            End If
        End If
    End Sub

#End Region

#Region "ORDONNANCE AREA"

    Private Sub ClearTXBs(sender As Object, e As EventArgs) Handles ClearBTN.Click
        ListeType_CMB.Items.Clear()
        ListeType_CMB.SelectedIndex = -1
        ListeType_CMB.Text = ""
        ListeMedicament_CMB.Items.Clear()
        ListeMedicament_CMB.SelectedIndex = -1
        ListeMedicament_CMB.Text = ""
        ListePosologie_CMB.Items.Clear()
        ListePosologie_CMB.SelectedIndex = -1
        ListePosologie_CMB.Text = ""
        Typesvect.Clear()
        MPIDvect.Clear()
        Posologievect.Clear()
        medsvect.Clear()

        BringIndicator(SubMenuButton_1_BTN, New EventArgs)
    End Sub

    Private Sub ListeType_CMB_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListeType_CMB.SelectedIndexChanged
        If FormLoad = True And ListeType_CMB.SelectedIndex <> -1 Then
            ListeMedicament_CMB.SelectedIndex = -1
            ListeMedicament_CMB.Items.Clear()
            medsvect.Clear()
            Access.ExecQuery("select DISTINCT ME_ID_MEDICAMENT as ID, ME_NOM as name
                            from ((MEDICAMENT_POSOLOGIE 
                            LEFT JOIN Medicament ON MEDICAMENT.ME_ID_MEDICAMENT = MEDICAMENT_POSOLOGIE.MP_MEDICAMENT)
                            LEFT JOIN type_medicament ON TYPE_MEDICAMENT.TY_ID = MEDICAMENT.ME_TYPE_MEDICAMENT)
                            where MP_Docteur = " & SESSION.GetUser() & " and
                            ME_TYPE_MEDICAMENT = " & Typesvect(ListeType_CMB.SelectedIndex) & " and
                            MP_POSOLOGIE IS NULL")
            If NotEmpty(Access.Exception) Then
                MsgBox(Access.Exception)
            End If
            i = 0
            For Each r As DataRow In Access.DBDT.Rows
                If Not r.Item("ID").Equals(DBNull.Value) And Not r.Item("name").Equals(DBNull.Value) Then
                    medsvect.Add(i, r.Item("ID"))
                    ListeMedicament_CMB.Items.Add(r.Item("name"))
                    i += 1
                End If
            Next
        End If
    End Sub

    Private Sub ListeMedicament_CMB_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListeMedicament_CMB.SelectedIndexChanged
        If FormLoad = True And ListeMedicament_CMB.SelectedIndex <> -1 Then
            ListePosologie_CMB.SelectedIndex = -1
            ListePosologie_CMB.Items.Clear()
            MPIDvect.Clear()
            Posologievect.Clear()
            Access.ExecQuery("select DISTINCT MP_ID as MPID,PO_ID_POSOLOGIE as ID, PO_NOM as name
                            from (((MEDICAMENT_POSOLOGIE 
                            LEFT JOIN Medicament ON MEDICAMENT.ME_ID_MEDICAMENT = MEDICAMENT_POSOLOGIE.MP_MEDICAMENT)
                            LEFT JOIN type_medicament ON TYPE_MEDICAMENT.TY_ID = MEDICAMENT.ME_TYPE_MEDICAMENT)
                            LEFT JOIN posologie ON posologie.PO_ID_POSOLOGIE = MEDICAMENT_POSOLOGIE.MP_POSOLOGIE)
                            where MP_Docteur = " & SESSION.GetUser() & " and
                            ME_TYPE_MEDICAMENT = " & Typesvect(ListeType_CMB.SelectedIndex) & " and
                            MP_MEDICAMENT  = " & medsvect(ListeMedicament_CMB.SelectedIndex))
            If NotEmpty(Access.Exception) Then
                MsgBox(Access.Exception)
            End If
            i = 0
            For Each r As DataRow In Access.DBDT.Rows
                If Not r.Item("ID").Equals(DBNull.Value) And Not r.Item("name").Equals(DBNull.Value) Then
                    Posologievect.Add(i, r.Item("ID"))
                    MPIDvect.Add(i, r.Item("MPID"))
                    ListePosologie_CMB.Items.Add(r.Item("name"))
                    i += 1
                End If
            Next
        End If
    End Sub

    Private Sub AjouterMed_Click(sender As Object, e As EventArgs) Handles AjouterMed.Click
        If ListePosologie_CMB.SelectedIndex <> -1 And Not chosenmeds.ContainsKey(medsvect(ListeMedicament_CMB.SelectedIndex)) Then
            For Each r As Control In MedListPanel.Controls
                If r.Name.Contains("Medicament") Then
                    If r.Name.Contains(NumberOfMeds.ToString()) Then
                        r.Text = (ListeMedicament_CMB.Items(ListeMedicament_CMB.SelectedIndex))
                        NumberOfMeds += 1
                    End If
                End If
                If r.Name.Contains("Posologie") Then
                    If r.Name.Contains(NumberOfMeds.ToString()) Then
                        r.Text = (ListePosologie_CMB.Items(ListePosologie_CMB.SelectedIndex))
                        NumberOfPosologie += 1
                    End If
                End If
            Next

            'the med needs to be unique, can't add 2 duplicated keys of the same med !
            chosenmeds.Add(medsvect(ListeMedicament_CMB.SelectedIndex), Posologievect(ListePosologie_CMB.SelectedIndex))

            medsi = ListeMedicament_CMB.SelectedIndex
            possi = ListePosologie_CMB.SelectedIndex
            ListeMedicament_CMB.SelectedIndex = -1
            ListeMedicament_CMB.Text = ""
            ListePosologie_CMB.SelectedIndex = -1
            ListePosologie_CMB.Text = ""
            ListeType_CMB.SelectedIndex = -1
            ListeType_CMB.Text = ""
        End If
    End Sub

    Private Sub OrdonnanceSave_Click(sender As Object, e As EventArgs) Handles Ordonnance1.Click, Ordonnance5.Click, Ordonnance4.Click, Ordonnance3.Click, Ordonnance2.Click

        ' get the saved prescription, if 1 exists then ask to override, if yes then 
        ' insert if not inserted


        If NumberOfMeds > 1 Then
            'update the slot in database
            Access.AddParam("@saved", DirectCast(sender, Bunifu.Framework.UI.BunifuImageButton).Tag.ToString())
            Access.ExecQuery("Select OR_ID_ORDONNANCE as id from ordonnance where saved = @saved")
            If NotEmpty(Access.Exception) Then
                MsgBox(Access.Exception)
            End If
            If Access.DBDT.Rows.Count > 0 Then
                Dim result As Integer = MessageBox.Show("Il y'a une ordonnance ici, voulez vous l'ecraser?",
            "caption", MessageBoxButtons.YesNo)
                If result = DialogResult.Yes Then
                    Access.AddParam("@saved", Access.DBDT.Rows(0).Item("id"))
                    Access.ExecQuery("Update Ordonnance set saved = 0 where saved = @saved")
                    If NotEmpty(Access.Exception) Then
                        MsgBox(Access.Exception)
                    End If
                    Access.ExecQuery("Update Ordonnance set saved = 0 where saved = @saved")
                End If

                Access.AddParam("@id", Access.DBDT.Rows(0).Item("id"))
                Access.ExecQuery("")
                If NotEmpty(Access.Exception) Then
                    MsgBox(Access.Exception)
                End If
                Access.ExecQuery("")
                If NotEmpty(Access.Exception) Then
                    MsgBox(Access.Exception)
                End If
            End If
        End If
    End Sub

    Private Sub AjouterMed_Click() Handles AjouterMed.Click
        'should create an empty posologie on click then fill it one by one, so that the user has the ability to
        'save in one of the 5 templates 
        If ListeType_CMB.SelectedIndex > -1 Then
            If ListeMedicament_CMB.SelectedIndex > -1 Then
                If ListePosologie_CMB.SelectedIndex > -1 Then
                    AjouterMedicament(ListeMedicament_CMB.SelectedIndex, ListePosologie_CMB.SelectedIndex)
                End If
            End If
        End If
    End Sub

    Private Sub SupprimerMedicament(indice As Integer)
        'deletes the med from the given "indice" of the med inputs
        Meds.RemoveAt(indice)
        RefreshList()
    End Sub

    Private Sub AjouterMedicament(MedId As Integer, PosId As Integer)
        Dim a As New MedLine
        a.MedId = MedId
        a.PosoId = PosId
        a.Med = medsvect(MedId)
        a.Poso = Posologievect(PosId)
        Meds.Add(a)
        RefreshList()
    End Sub

    Private Sub RefreshList()
        'add all meds in the list to the textboxes

        For Each r As MedLine In Meds

        Next
    End Sub

    Private Sub Medicament_Enter(sender As Object, e As EventArgs) Handles Medicament1.Enter, Medicament2.Enter, Medicament3.Enter, Medicament4.Enter, Medicament5.Enter
        'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
        If Trim(DirectCast(sender, Bunifu.Framework.UI.BunifuMetroTextbox).Text) = "Medicament" Then
            DirectCast(sender, Bunifu.Framework.UI.BunifuMetroTextbox).Text = ""
        End If
    End Sub

    Private Sub Medicament_Leave(sender As Object, e As EventArgs) Handles Medicament1.Leave, Medicament2.Leave, Medicament3.Leave, Medicament4.Leave, Medicament5.Leave
        'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
        If Trim(DirectCast(sender, Bunifu.Framework.UI.BunifuMetroTextbox).Text) = "" Then
            DirectCast(sender, Bunifu.Framework.UI.BunifuMetroTextbox).Text = "Medicament"
        End If
    End Sub

    Private Sub Posologie_Enter(sender As Object, e As EventArgs) Handles Posologie1.Enter, Posologie2.Enter, Posologie3.Enter, Posologie4.Enter, Posologie5.Enter
        'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
        If Trim(DirectCast(sender, Bunifu.Framework.UI.BunifuMetroTextbox).Text) = "Posologie" Then
            DirectCast(sender, Bunifu.Framework.UI.BunifuMetroTextbox).Text = ""
        End If
    End Sub

    Private Sub Posologie_Leave(sender As Object, e As EventArgs) Handles Posologie1.Leave, Posologie2.Leave, Posologie3.Leave, Posologie4.Leave, Posologie5.Leave
        'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
        If Trim(DirectCast(sender, Bunifu.Framework.UI.BunifuMetroTextbox).Text) = "" Then
            DirectCast(sender, Bunifu.Framework.UI.BunifuMetroTextbox).Text = "Posologie"
        End If
    End Sub


#End Region

#Region "GLOBAL FUNCTIONS"
    'the filterstringparams have to be ready before running this function 
    Private Sub filterData(dgv As DataGridView, filterstringparams As Dictionary(Of String, String))
        Dim filterstring As String
        filterstring = filterstringparams.Keys(0).ToString & filterstringparams(filterstringparams.Keys(0).ToString).ToString & " "
        i = 0
        For Each r In filterstringparams.Keys
            If i <> 0 Then
                If r = "date_debut" Then
                    If filterstringparams(r) <> "" Then
                        filterstring = filterstring & " And " & " Date" & filterstringparams(r).ToString & " "
                    Else
                        'remove it from string 
                        filterstring.Replace("And date_debut", "")
                    End If
                ElseIf r = "date_fin" Then
                    If filterstringparams(r) <> "" Then
                        filterstring = filterstring & " And " & " Date" & filterstringparams(r).ToString & " "
                    Else
                        'remove it from string 
                        filterstring.Replace("And date_fin", "")
                    End If
                Else
                    filterstring = filterstring & " And " & r.ToString & filterstringparams(r).ToString & " "
                End If
            End If
            i += 1
        Next
        Dim MainDataview = New DataView(MainDataTable)
        MainDataview.RowFilter = filterstring
        dgv.DataSource = MainDataview.ToTable()
    End Sub

    Private Sub SwitchPanels(paneltoshow As Panel)
        'HIDES ALL PANELS AND SHOWS THIS ONE 
        ActivePanel.Visible = False
        ActivePanel = paneltoshow

        'SET LOCATONS AND SIZES
        paneltoshow.Visible = True
        paneltoshow.Dock = DockStyle.Fill
    End Sub
#End Region
    'menu buttons click 
#Region "   MENUS"


    'this is to show submenus
    Private Sub Dashboard_Click(sender As Object, e As EventArgs) Handles MainMenuDashboard_BTN.Click
        ChosenMenu = 0
        OpenAppropriateMenu(ChosenMenu)
    End Sub

    Private Sub Stock_Click(sender As Object, e As EventArgs) Handles MainMenuStock_B_BTN.Click
        ChosenMenu = 1
        OpenAppropriateMenu(ChosenMenu)
    End Sub

    Private Sub Ordonnance_Click(sender As Object, e As EventArgs) Handles MainMenuOrdonnance_B_BTN.Click
        ChosenMenu = 2
        OpenAppropriateMenu(ChosenMenu)
    End Sub

    Private Sub Laboratoire_Click(sender As Object, e As EventArgs) Handles MainMenuLaboratoire_B_BTN.Click
        ChosenMenu = 3
        OpenAppropriateMenu(ChosenMenu)
    End Sub

    Private Sub Tracabilite_Click(sender As Object, e As EventArgs) Handles MainMenuTracabilite_B_BTN.Click
        ChosenMenu = 4
        OpenAppropriateMenu(ChosenMenu)
    End Sub

    Private Sub Mutuelle_Click(sender As Object, e As EventArgs) Handles MainMenuMutuelle_B_BTN.Click
        ChosenMenu = 5
        OpenAppropriateMenu(ChosenMenu)
    End Sub
#End Region



    'some other stuff on form1 
    Private Sub BunifuImageButton1_Click(sender As Object, e As EventArgs) Handles Close_BTN.Click
        'CLOSES PROGRAM ( X BUTTON)
        Me.Close()
    End Sub



    Private Sub Rechercher_Fournisseur_TXB_Leave(sender As Object, e As EventArgs) Handles Rechercher_Code_TXB.Leave
        'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
        If Trim(Rechercher_Code_TXB.Text) = "" Then
            Rechercher_Code_TXB.Text = "Nom D'Article"
        End If
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'FORM1 LOAD, ALL early stuff 

        SESSION.Open("bo3OU", "bo3OU")
        ' Dashboard_Click(MainMenuDashboard_BTN, New EventArgs)
        OpenAppropriateMenu(0)
        FormLoad = True

    End Sub

    Private Sub article_name_TXB_Enter(sender As Object, e As EventArgs) Handles article_name_TXB.Enter
        'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
        If Trim(article_name_TXB.Text) = "Nom D'Article" Then
            article_name_TXB.Text = ""
        End If
    End Sub

    Private Sub article_name_TXB_Leave(sender As Object, e As EventArgs) Handles article_name_TXB.Leave
        'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
        If Trim(Rechercher_Code_TXB.Text) = "" Then
            Rechercher_Code_TXB.Text = "Nom D'Article"
        End If
    End Sub

    Private Sub Stock_Articles_Articles_TXB_Enter(sender As Object, e As EventArgs) Handles Stock_Articles_Articles_TXB.Enter
        'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
        If Trim(Stock_Articles_Articles_TXB.Text) = "Nom D'Article" Then
            Stock_Articles_Articles_TXB.Text = ""
        End If
    End Sub

    Private Sub Stock_Articles_Articles_TXB_Leave(sender As Object, e As EventArgs) Handles Stock_Articles_Articles_TXB.Leave

        'SIMULATES THE HTML'S PLACEHOLDER BEHAVIOR
        If Trim(Rechercher_Code_TXB.Text) = "" Then
            Rechercher_Code_TXB.Text = "Nom D'Article"
        End If
    End Sub



    Private Sub Stock_AddArticle_Click(sender As Object, e As EventArgs) Handles Stock_AddArticle.Click
        Dim add As New AddArticle()
        add.fill()
        add.ShowDialog()
    End Sub

    Private Sub Stock_AddSupplier_BTN_Click(sender As Object, e As EventArgs) Handles Stock_AddSupplier_BTN.Click
        Dim add As New AddSupplier()
        add.ShowDialog()
    End Sub

    Private Sub Stock_ModifySupplier_BTN_Click(sender As Object, e As EventArgs) Handles Stock_ModifySupplier_BTN.Click
        Dim modify As New AddSupplier()
        modify.ShowAndFill(SuppliersList_DGV.Rows(SuppliersList_DGV.SelectedRows(0).Index).Cells(0).Value)
    End Sub

    Private Sub BunifuImageButton1_Click_2(sender As Object, e As EventArgs) Handles PayOrder_BTN.Click
        Dim pay As New PayerCommande()
        If dgv1.Rows.Count > 0 Then
            If pay.ShowAndFill(dgv1.Rows(dgv1.SelectedRows(0).Index).Cells(0).Value) = 1 Then
                pay.ShowDialog()
            End If
        End If
    End Sub

    Private Sub Modify_Order_Oders_BTN_Click(sender As Object, e As EventArgs) Handles Modify_Order_Oders_BTN.Click
        If dgv1.SelectedRows.Count > 0 Then
            Dim DeleteId As Integer = dgv1.CurrentRow.Cells(0).Value
            Dim ModifyForm As New AddOrder()
            If ModifyForm.FillToModify(DeleteId) = 1 Then
                ModifyForm.ShowDialog()
            Else
                MsgBox("error loading the form ! ")
                'update to annullé .. do not delete 
                'Access.ExecQuery("delete * from COMMANDE where commande.CO_ID_COMMANDE = " & DeleteId)
                'If NotEmpty(Access.Exception) Then
                '    MsgBox(Access.Exception)
                'Else
                '    BringIndicator(SubMenuButton_1_BTN, New EventArgs)
                'End If
            End If
        End If
    End Sub

    Private Sub UseSupply_Click(sender As Object, e As EventArgs) Handles UseSupply.Click

        If Stock_DGV.SelectedRows.Count > 0 Then
            Dim FormToShow As New UseSupply()
            If FormToShow.fill(Stock_DGV.CurrentRow.Cells(0).Value) = 1 Then
                FormToShow.ShowDialog()
            Else
                MsgBox("erreur!")
            End If

        End If
    End Sub

    Private Sub DeleteArticle_Click(sender As Object, e As EventArgs) Handles DeleteArticle.Click
        If (ListOfArticles_DGV.SelectedRows.Count) > 0 Then

            Dim result As Integer = MessageBox.Show("ATTENTION ! cette action est irreversible," &
                Environment.NewLine & "voulez vous vraiment supprimer cet article?" &
                Environment.NewLine & Environment.NewLine & "Ceci supprimera toutes les commandes et paiement concernant cet article!",
                "caption", MessageBoxButtons.YesNo)
            If result = DialogResult.Yes Then
                Dim DeleteId As Integer = ListOfArticles_DGV.CurrentRow.Cells(0).Value
                Access.AddParam("@id", DeleteId)
                Access.ExecQuery("delete * from article where article.AR_ID_ARTICLE = @id")
                If NotEmpty(Access.Exception) Then
                    MsgBox(Access.Exception)
                Else
                    BringIndicator(SubMenuButton_4_BTN, New EventArgs)
                End If
            ElseIf result = DialogResult.No Then
            End If
        End If
    End Sub

    Private Sub Save1_Click(sender As Object, e As EventArgs) Handles Save1.Click, Save5.Click, Save4.Click, Save3.Click, Save2.Click
        Access.InsertQuery("insert into Ordonnance (OR_DOCTEUR,OR_SAVED) values(" & SESSION.GetUser() & ", 0)")
        OrdonnanceId = Access.ID
        For Each row In chosenmeds.Keys
            Access.ExecQuery("insert into MEDICAMENT_ORDONNANCE (MO_MP,MO_ORDONNANCE) values( " & MPIDvect(possi) & " , " & OrdonnanceId & " )")
        Next

        Access.ExecQuery("update Ordonnance set PO_SAVED = 0 where PO_SAVED = " & DirectCast(sender, Bunifu.Framework.UI.BunifuImageButton).Tag)
        Access.ExecQuery("update Ordonnance set PO_SAVED  = " & DirectCast(sender, Bunifu.Framework.UI.BunifuImageButton).Tag & "  where OR_ID_ORDONNANCE = " & OrdonnanceId)
        MsgBox("sauvegardé !")
    End Sub

    Private Sub ListePosologie_CMB_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListePosologie_CMB.SelectedIndexChanged

    End Sub

    Private Sub RemoveMed_Click(sender As Object, e As EventArgs) Handles RemoveMed1.Click, RemoveMed5.Click, RemoveMed4.Click, RemoveMed3.Click, RemoveMed2.Click
        CType(Controls("Medicament" & CType(sender, Bunifu.Framework.UI.BunifuImageButton).Tag), Bunifu.Framework.UI.BunifuMetroTextbox).Text = "Medicament"
        CType(Controls("Posologie" & CType(sender, Bunifu.Framework.UI.BunifuImageButton).Tag), Bunifu.Framework.UI.BunifuMetroTextbox).Text = "Posologie"
    End Sub
End Class