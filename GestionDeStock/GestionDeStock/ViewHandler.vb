﻿'THIS IS //TODO

Module ViewHandler
    Public Flag_CantClick = False

    Public Sub OpenAppropriateMenu(Menu As Integer)
        'EVERY BUTTON IN MAIN MENU HAS A TAG, FROM 0 to 6, THIS CALLS SHOWSUBMENUBUTTON WITH THAT (INTEGER)
        'DASHBOARD
        Select Case Menu
            Case 0

                If SESSION.State() = False Then
                    'THERE IS NO STATE
                    'redirect to login Menu! 

                ElseIf Flag_CantClick = True Then
                    'CANT CLICK ON THIS FORM

                Else
                    'SUB BUTTONS VISIBILITY
                    MainForm.SubMenuButton_1_BTN.Visible = False
                    MainForm.SubMenuButton_2_BTN.Visible = False
                    MainForm.SubMenuButton_3_BTN.Visible = False
                    MainForm.SubMenuButton_4_BTN.Visible = False
                    MainForm.SubMenuButton_5_BTN.Visible = False
                    MainForm.Indicator.Visible = False

                    'SET WHICH PANEL IS VISIBLE AND HIDE THE ONE THAT WAS PREVIOUSLY VISIBLE 
                    ActivePanel.Visible = False
                    ActivePanel = MainForm.PNL_Dashboard

                    'PAINT THE APPROPRIATE PANEL FOR DASHBOARD
                    MainForm.PNL_Dashboard.Visible = True
                    MainForm.PNL_Dashboard.Dock = DockStyle.Fill
                    MainForm.Dashboard_First_PNL.Location = New Point(60, 20)

                End If
            Case 1

                If SESSION.State() = False Then
                    'REDIRECT TO CONNECT
                Else
                    'SHOW STOCK PANEL AND HIDE THE PREVIOUS ONE 
                    MainForm.SubMenuButton_1_BTN.LabelText = "Commandes"
                    'MainForm.SubMenuButton_2_BTN.LabelText = "Receptions"
                    MainForm.SubMenuButton_3_BTN.LabelText = "Stock"
                    MainForm.SubMenuButton_4_BTN.LabelText = "Articles"
                    MainForm.SubMenuButton_5_BTN.LabelText = "Fournisseurs"

                    MainForm.SubMenuButton_1_BTN.Visible = True
                    'MainForm.SubMenuButton_2_BTN.Visible = True
                    MainForm.SubMenuButton_3_BTN.Visible = True
                    MainForm.SubMenuButton_4_BTN.Visible = True
                    MainForm.SubMenuButton_5_BTN.Visible = True
                    MainForm.Indicator.Visible = True

                    'RAMENE L'INDIATEUR SOUS LE PREMIER SOUS-BOUTON
                    MainForm.BringIndicator(MainForm.SubMenuButton_1_BTN, New EventArgs)

                    'MainForm.SubMenuButton_1_BTN.Image = GestionDeStock.My.Resources.
                    'MainForm.SubMenuButton_2_BTN.Image = GestionDeStock.My.Resources.
                    'MainForm.SubMenuButton_3_BTN.Image = GestionDeStock.My.Resources.
                    'MainForm.SubMenuButton_4_BTN.Image = GestionDeStock.My.Resources.
                    'MainForm.SubMenuButton_5_BTN.Image = GestionDeStock.My.Resources.
                End If
            Case 2
                If SESSION.State() = False Then
                Else
                    MainForm.SubMenuButton_1_BTN.Visible = False
                    MainForm.SubMenuButton_2_BTN.Visible = False
                    MainForm.SubMenuButton_3_BTN.Visible = False
                    MainForm.SubMenuButton_4_BTN.Visible = False
                    MainForm.SubMenuButton_5_BTN.Visible = False
                    MainForm.Indicator.Visible = False

                    MainForm.BringIndicator(MainForm.SubMenuButton_1_BTN, New EventArgs)

                End If
        End Select

    End Sub
End Module
