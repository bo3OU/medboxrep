﻿'THIS HANDLES EVERYTHING LOGIN,PASSWORD,CABINET,USER 

Public Class SessionHandler
    Private User As Integer = -1
    Private Cabinet As Integer = -1
    Private Directeur As Integer = -1
    Private doctor As Boolean = False
    Private username As String = ""
    Public Function GetCabinet()
        Return Cabinet
    End Function
    Public Function GetDirecteur()
        Return Directeur
    End Function
    Public Function GetUser()
        Return User
    End Function
    Public Function IsDoctor()
        Return doctor
    End Function
    Private Function GetUsername()
        Return Username
    End Function

    Public Function Open(login As String, Password As String)
        Access.AddParam("@login", login)
        Access.AddParam("@mdp", Password)
        Access.ExecQuery("SELECT EM_ID_EMPLOYE,EM_DIRECTEUR,EM_DOCTEUR,EM_NOM as name FROM EMPLOYE where EM_LOGIN = @login and EM_MDP = @mdp ")

        'test if an exception was raised when query was run 
        If NotEmpty(Access.Exception) Then
            MsgBox(Access.Exception)
            Return False
            Exit Function
        ElseIf Access.DBDT.Rows.Count < 1 Then
            MsgBox("mot de passe incorrect")
            Return False
        Else
            Directeur = Access.DBDT.Rows(0).Item("EM_DIRECTEUR")
            User = Access.DBDT.Rows(0).Item("EM_ID_EMPLOYE")
            doctor = Access.DBDT.Rows(0).Item("EM_DOCTEUR")
            username = Access.DBDT.Rows(0).Item("name")
            Access.Params.Clear()
            Access.DBDT.Clear()
            Access.AddParam("@directeur", Directeur)
            Access.ExecQuery("SELECT CA_ID_CABINET FROM CABINET where CA_GERANT = @directeur")
            Cabinet = Access.DBDT.Rows(0).Item("CA_ID_CABINET")
            If NotEmpty(Access.Exception) Then
                MsgBox(Access.Exception)

                Return False
                Exit Function
            Else
                MainForm.ConnectedUserName.Text = MainForm.ConnectedUserName.Text & SESSION.GetUsername()
                Return True
            End If
        End If
        Return False

    End Function
    Public Sub Close()
        Cabinet = -1
        User = -1
        Directeur = -1
    End Sub
    Public Function State()
        If (Cabinet + User + Directeur) = -3 Then
            Return False
        Else
            Return True
        End If
    End Function
End Class
