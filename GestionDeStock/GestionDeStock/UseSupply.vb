﻿Imports System.Convert
Public Class UseSupply
    Private FormLoad As Boolean = False
    Private QuantityAvailable As Integer
    Private ArticleId As Integer = -1
    Private unite As String
    Private Sub UseSupply_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        FormLoad = True
    End Sub

    Public Function fill(id As Integer)
        ArticleId = id
        Access.ExecQuery("Select
ST_quantite as quantity,
article.AR_NOM as name,
unite.UN_NOM as unite
from ((STOCK
LEFT JOIN article on article.AR_ID_ARTICLE = STOCK.ST_ARTICLE )
LEFT JOIN unite on unite.UN_ID_UNITE = article.AR_UNITE)
where stock.ST_ID_STOCK =" & id)
        If NotEmpty(Access.Exception) Then
            MsgBox(Access.Exception)
        Else
            If Access.DBDT.Rows.Count > 0 Then
                ArticleInfo.Text = "Article : " & Access.DBDT.Rows(0).Item("name")
                QuantityAvailable = Access.DBDT.Rows(0).Item("quantity")
                unite = Access.DBDT.Rows(0).Item("unite")
                QuantityLeft.Text = "Quantite Apres Modification : " & QuantityAvailable - 1 & " " & unite
                QuantityBefore.Text = "Quantite Avant Modification : " & QuantityAvailable & " " & unite
                Return 1
            End If
        End If
        Return 0
    End Function

    Private Sub AddUseNumber_Click(sender As Object, e As EventArgs) Handles AddUseNumber.Click
        UseNumber.Text = UseNumber.Text + 1
    End Sub

    Private Sub SubUseNumber_Click(sender As Object, e As EventArgs) Handles SubUseNumber.Click
        UseNumber.Text = UseNumber.Text - 1
    End Sub

    Private Sub AddLeftNumber_Click(sender As Object, e As EventArgs) Handles AddLeftNumber.Click
        LeftNumber.Text = LeftNumber.Text + 1
    End Sub

    Private Sub SubLeftNumber_Click(sender As Object, e As EventArgs) Handles SubLeftNumber.Click
        LeftNumber.Text = LeftNumber.Text - 1
    End Sub

    Private Sub UseNumber_OnValueChanged(sender As Object, e As EventArgs) Handles UseNumber.OnValueChanged
        If FormLoad Then
            If String.IsNullOrWhiteSpace(UseNumber.Text) Then
                UseNumber.Text = 1
            ElseIf (ToInt64(UseNumber.Text)) < 1 Then
                UseNumber.Text = 1
            ElseIf (ToInt64(UseNumber.Text)) > QuantityAvailable Then
                UseNumber.Text = QuantityAvailable
        End If
            QuantityLeft.Text = "Quantite restante : " & (QuantityAvailable - UseNumber.Text).ToString() & " " & unite
        End If
    End Sub

    Private Sub LeftNumber_OnValueChanged(sender As Object, e As EventArgs) Handles LeftNumber.OnValueChanged
        If FormLoad Then
            If String.IsNullOrWhiteSpace(LeftNumber.Text) Then
                LeftNumber.Text = 0
            ElseIf (ToInt64(LeftNumber.Text)) < 0 Then
                LeftNumber.Text = 0
            ElseIf (ToInt64(LeftNumber.Text)) >= QuantityAvailable Then
                LeftNumber.Text = QuantityAvailable - 1
            End If
            QuantityLeft.Text = "Quantite restante : " & LeftNumber.Text & " " & unite
        End If
    End Sub

    Private Sub LeftNumber_KeyPress(sender As Object, e As KeyPressEventArgs) Handles LeftNumber.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Or LeftNumber.Text.Length > 4 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub UseNumber_KeyPress(sender As Object, e As KeyPressEventArgs) Handles UseNumber.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Or UseNumber.Text.Length > 4 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub UseRadio_CheckedChanged(sender As Object, e As EventArgs) Handles UseRadio.CheckedChanged
        If UseRadio.Checked = True Then
            UseNumber.Enabled = True
            SubUseNumber.Enabled = True
            AddUseNumber.Enabled = True
            AddLeftNumber.Enabled = False
            SubLeftNumber.Enabled = False
            LeftNumber.Enabled = False
            QuantityLeft.Text = "Quantite restante : " & (QuantityAvailable - UseNumber.Text).ToString() & " " & unite

        End If
    End Sub

    Private Sub LeftRadio_CheckedChanged(sender As Object, e As EventArgs) Handles LeftRadio.CheckedChanged
        If LeftRadio.Checked = True Then
            UseNumber.Enabled = False
            SubUseNumber.Enabled = False
            AddUseNumber.Enabled = False
            AddLeftNumber.Enabled = True
            SubLeftNumber.Enabled = True
            LeftNumber.Enabled = True
            QuantityLeft.Text = "Quantite restante : " & LeftNumber.Text & " " & unite
        End If
    End Sub

    Private Sub Close_Click(sender As Object, e As EventArgs) Handles BunifuImageButton2.Click
        Close()
    End Sub

    Private Sub Modifier_Click(sender As Object, e As EventArgs) Handles Modifier.Click
        Dim ModifiedQuantity As Integer
        If UseRadio.Checked = True Then
            ModifiedQuantity = (QuantityAvailable - UseNumber.Text)
        Else
            ModifiedQuantity = LeftNumber.Text
        End If
        Access.ExecQuery("Update Stock set ST_QUANTITE = " & ModifiedQuantity & " where ST_ID_STOCK = " & ArticleId)
        If NotEmpty(Access.Exception) Then
            MsgBox(Access.Exception)
        Else
            If ModifiedQuantity = 0 Then
                ' Access.ExecQuery("Update Commande set CO_ETAT = 5 where CO_ID_COMMANDE = " & ArticleId)
                If NotEmpty(Access.Exception) Then
                    MsgBox(Access.Exception)
                Else
                    MainForm.BringIndicator(MainForm.SubMenuButton_3_BTN, New EventArgs)
                    Close()
                End If
            End If
            MainForm.BringIndicator(MainForm.SubMenuButton_3_BTN, New EventArgs)
            Close()
        End If
    End Sub

    Private Sub CloseForm(sender As Object, e As EventArgs) Handles CancelButton_BTN.Click
        Close()
    End Sub
End Class