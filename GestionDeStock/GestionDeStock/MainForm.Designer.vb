﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle28 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle30 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle31 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle32 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle33 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle34 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle35 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle36 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle37 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle38 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle39 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.LeftPanel = New System.Windows.Forms.Panel()
        Me.ConnectedUserName = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.Logout_BTN = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.MainMenuParametre_B_BTN = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.MainMenuMutuelle_B_BTN = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.MainMenuTracabilite_B_BTN = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.MainMenuLaboratoire_B_BTN = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.MainMenuOrdonnance_B_BTN = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.MainMenuStock_B_BTN = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.SlideMenuRight_IMG = New Bunifu.Framework.UI.BunifuImageButton()
        Me.MainMenuDashboard_BTN = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.TopPanel = New System.Windows.Forms.Panel()
        Me.SubMenuButton_5_BTN = New Bunifu.Framework.UI.BunifuTileButton()
        Me.SubMenuButton_4_BTN = New Bunifu.Framework.UI.BunifuTileButton()
        Me.SubMenuButton_3_BTN = New Bunifu.Framework.UI.BunifuTileButton()
        Me.SubMenuButton_2_BTN = New Bunifu.Framework.UI.BunifuTileButton()
        Me.Indicator = New System.Windows.Forms.PictureBox()
        Me.SubMenuButton_1_BTN = New Bunifu.Framework.UI.BunifuTileButton()
        Me.PanelDragger = New System.Windows.Forms.Panel()
        Me.Close_BTN = New Bunifu.Framework.UI.BunifuImageButton()
        Me.NDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UniteDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PNL_Stock_Commande = New System.Windows.Forms.Panel()
        Me.Commande_GeneralData1 = New System.Windows.Forms.Panel()
        Me.TotalMoneyToSuppliers = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.UnpaidOrders = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.ListOfOrders = New System.Windows.Forms.Panel()
        Me.PayOrder_BTN = New Bunifu.Framework.UI.BunifuImageButton()
        Me.Delete_Order_Oders_BTN = New Bunifu.Framework.UI.BunifuImageButton()
        Me.Modify_Order_Oders_BTN = New Bunifu.Framework.UI.BunifuImageButton()
        Me.Add_Order_Oders_BTN = New Bunifu.Framework.UI.BunifuImageButton()
        Me.ListeDeCommande_LBL = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.dgv1 = New Bunifu.Framework.UI.BunifuCustomDataGrid()
        Me.SearchAreaOrders = New System.Windows.Forms.Panel()
        Me.Au_DTP = New System.Windows.Forms.DateTimePicker()
        Me.De_DTP = New System.Windows.Forms.DateTimePicker()
        Me.BunifuCustomLabel7 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.Rechercher_Fournisseur_CMB = New System.Windows.Forms.ComboBox()
        Me.BunifuCustomLabel4 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.UseDateInSearch = New Bunifu.Framework.UI.BunifuSwitch()
        Me.BunifuCustomLabel2 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.Rechercher_Code_TXB = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.PNL_Stock_Stock = New System.Windows.Forms.Panel()
        Me.HighPanel = New System.Windows.Forms.Panel()
        Me.Low_LBL = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.HighAlert = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.Stock_SearchArea = New System.Windows.Forms.Panel()
        Me.article_name_TXB = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.BunifuCustomLabel14 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.LowPanel = New System.Windows.Forms.Panel()
        Me.High_LBL = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.LowAlert = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.ListOfArticlesInStock = New System.Windows.Forms.Panel()
        Me.UseSupply = New Bunifu.Framework.UI.BunifuImageButton()
        Me.Stock_DGV = New Bunifu.Framework.UI.BunifuCustomDataGrid()
        Me.Stock_LBL = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.PNL_Login = New System.Windows.Forms.Panel()
        Me.BunifuCustomLabel10 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.PNL_Login_Authentification = New System.Windows.Forms.Panel()
        Me.Password_TXB = New Bunifu.Framework.UI.BunifuMaterialTextbox()
        Me.Authentificate_BTN = New Bunifu.Framework.UI.BunifuThinButton2()
        Me.Login_TXB = New Bunifu.Framework.UI.BunifuMaterialTextbox()
        Me.PNL_Dashboard = New System.Windows.Forms.Panel()
        Me.BunifuCustomLabel11 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.Dashboard_First_PNL = New System.Windows.Forms.Panel()
        Me.BunifuCustomLabel5 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.PNL_Stock_Articles = New System.Windows.Forms.Panel()
        Me.SearchArticleInStock = New System.Windows.Forms.Panel()
        Me.Stock_Articles_Articles_TXB = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.BunifuCustomLabel16 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.ListOfStock = New System.Windows.Forms.Panel()
        Me.DeleteArticle = New Bunifu.Framework.UI.BunifuImageButton()
        Me.Modify_Article_BTN = New Bunifu.Framework.UI.BunifuImageButton()
        Me.Stock_AddArticle = New Bunifu.Framework.UI.BunifuImageButton()
        Me.ListOfArticles_DGV = New Bunifu.Framework.UI.BunifuCustomDataGrid()
        Me.BunifuCustomLabel18 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.PNL_Stock_Suppliers = New System.Windows.Forms.Panel()
        Me.Stock_Suppliers_SearchArea = New System.Windows.Forms.Panel()
        Me.Suppliercity = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.SupplierPhone = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.SupplierAddresse = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.SupplierLastName = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.SupplierName = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.BunifuCustomLabel28 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.ListOfSuppliers = New System.Windows.Forms.Panel()
        Me.Suppliers_DeleteSupplier = New Bunifu.Framework.UI.BunifuImageButton()
        Me.Stock_ModifySupplier_BTN = New Bunifu.Framework.UI.BunifuImageButton()
        Me.Stock_AddSupplier_BTN = New Bunifu.Framework.UI.BunifuImageButton()
        Me.SuppliersList_DGV = New Bunifu.Framework.UI.BunifuCustomDataGrid()
        Me.BunifuCustomLabel30 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.PNL_Ordonnance = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.BunifuCustomLabel3 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.ListeMedicament = New System.Windows.Forms.Panel()
        Me.AjouterMed = New Bunifu.Framework.UI.BunifuThinButton2()
        Me.TypeLabel = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.ListeType_CMB = New System.Windows.Forms.ComboBox()
        Me.ListePosologie_CMB = New System.Windows.Forms.ComboBox()
        Me.ClearBTN = New Bunifu.Framework.UI.BunifuImageButton()
        Me.PosologieLabel = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.MedicamentLabel = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.ListeMedicament_CMB = New System.Windows.Forms.ComboBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Ordonnance1 = New Bunifu.Framework.UI.BunifuImageButton()
        Me.Ordonnance2 = New Bunifu.Framework.UI.BunifuImageButton()
        Me.Ordonnance3 = New Bunifu.Framework.UI.BunifuImageButton()
        Me.Ordonnance4 = New Bunifu.Framework.UI.BunifuImageButton()
        Me.Ordonnance5 = New Bunifu.Framework.UI.BunifuImageButton()
        Me.Save1 = New Bunifu.Framework.UI.BunifuImageButton()
        Me.Save5 = New Bunifu.Framework.UI.BunifuImageButton()
        Me.Save2 = New Bunifu.Framework.UI.BunifuImageButton()
        Me.Save4 = New Bunifu.Framework.UI.BunifuImageButton()
        Me.Save3 = New Bunifu.Framework.UI.BunifuImageButton()
        Me.MedListPanel = New System.Windows.Forms.Panel()
        Me.Posologie5 = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.Medicament5 = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.RemoveMed5 = New Bunifu.Framework.UI.BunifuImageButton()
        Me.Posologie4 = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.Medicament4 = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.RemoveMed4 = New Bunifu.Framework.UI.BunifuImageButton()
        Me.Posologie3 = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.Posologie2 = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.Medicament3 = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.Medicament2 = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.Posologie1 = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.Medicament1 = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.NomPatient = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.RemoveMed3 = New Bunifu.Framework.UI.BunifuImageButton()
        Me.RemoveMed2 = New Bunifu.Framework.UI.BunifuImageButton()
        Me.RemoveMed1 = New Bunifu.Framework.UI.BunifuImageButton()
        Me.BunifuDragControl1 = New Bunifu.Framework.UI.BunifuDragControl(Me.components)
        Me.BunifuDragControl2 = New Bunifu.Framework.UI.BunifuDragControl(Me.components)
        Me.LeftPanel.SuspendLayout()
        CType(Me.SlideMenuRight_IMG, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TopPanel.SuspendLayout()
        CType(Me.Indicator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelDragger.SuspendLayout()
        CType(Me.Close_BTN, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PNL_Stock_Commande.SuspendLayout()
        Me.Commande_GeneralData1.SuspendLayout()
        Me.ListOfOrders.SuspendLayout()
        CType(Me.PayOrder_BTN, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Delete_Order_Oders_BTN, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Modify_Order_Oders_BTN, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Add_Order_Oders_BTN, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SearchAreaOrders.SuspendLayout()
        Me.PNL_Stock_Stock.SuspendLayout()
        Me.HighPanel.SuspendLayout()
        Me.Stock_SearchArea.SuspendLayout()
        Me.LowPanel.SuspendLayout()
        Me.ListOfArticlesInStock.SuspendLayout()
        CType(Me.UseSupply, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Stock_DGV, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PNL_Login.SuspendLayout()
        Me.PNL_Login_Authentification.SuspendLayout()
        Me.PNL_Dashboard.SuspendLayout()
        Me.Dashboard_First_PNL.SuspendLayout()
        Me.PNL_Stock_Articles.SuspendLayout()
        Me.SearchArticleInStock.SuspendLayout()
        Me.ListOfStock.SuspendLayout()
        CType(Me.DeleteArticle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Modify_Article_BTN, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Stock_AddArticle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ListOfArticles_DGV, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PNL_Stock_Suppliers.SuspendLayout()
        Me.Stock_Suppliers_SearchArea.SuspendLayout()
        Me.ListOfSuppliers.SuspendLayout()
        CType(Me.Suppliers_DeleteSupplier, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Stock_ModifySupplier_BTN, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Stock_AddSupplier_BTN, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SuppliersList_DGV, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PNL_Ordonnance.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.ListeMedicament.SuspendLayout()
        CType(Me.ClearBTN, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.Ordonnance1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ordonnance2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ordonnance3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ordonnance4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ordonnance5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Save1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Save5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Save2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Save4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Save3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MedListPanel.SuspendLayout()
        CType(Me.RemoveMed5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RemoveMed4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RemoveMed3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RemoveMed2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RemoveMed1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LeftPanel
        '
        Me.LeftPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.LeftPanel.Controls.Add(Me.ConnectedUserName)
        Me.LeftPanel.Controls.Add(Me.Logout_BTN)
        Me.LeftPanel.Controls.Add(Me.MainMenuParametre_B_BTN)
        Me.LeftPanel.Controls.Add(Me.MainMenuMutuelle_B_BTN)
        Me.LeftPanel.Controls.Add(Me.MainMenuTracabilite_B_BTN)
        Me.LeftPanel.Controls.Add(Me.MainMenuLaboratoire_B_BTN)
        Me.LeftPanel.Controls.Add(Me.MainMenuOrdonnance_B_BTN)
        Me.LeftPanel.Controls.Add(Me.MainMenuStock_B_BTN)
        Me.LeftPanel.Controls.Add(Me.SlideMenuRight_IMG)
        Me.LeftPanel.Controls.Add(Me.MainMenuDashboard_BTN)
        Me.LeftPanel.Dock = System.Windows.Forms.DockStyle.Left
        Me.LeftPanel.Location = New System.Drawing.Point(0, 100)
        Me.LeftPanel.Name = "LeftPanel"
        Me.LeftPanel.Size = New System.Drawing.Size(200, 766)
        Me.LeftPanel.TabIndex = 0
        '
        'ConnectedUserName
        '
        Me.ConnectedUserName.AutoSize = True
        Me.ConnectedUserName.BackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.ConnectedUserName.Font = New System.Drawing.Font("Century Gothic", 8.25!)
        Me.ConnectedUserName.ForeColor = System.Drawing.Color.White
        Me.ConnectedUserName.Location = New System.Drawing.Point(12, 704)
        Me.ConnectedUserName.Name = "ConnectedUserName"
        Me.ConnectedUserName.Size = New System.Drawing.Size(69, 16)
        Me.ConnectedUserName.TabIndex = 10
        Me.ConnectedUserName.Text = "Bienvenue "
        '
        'Logout_BTN
        '
        Me.Logout_BTN.Activecolor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Logout_BTN.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.Logout_BTN.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Logout_BTN.BorderRadius = 0
        Me.Logout_BTN.ButtonText = "Logout"
        Me.Logout_BTN.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Logout_BTN.DisabledColor = System.Drawing.Color.Gray
        Me.Logout_BTN.Iconcolor = System.Drawing.Color.Transparent
        Me.Logout_BTN.Iconimage = CType(resources.GetObject("Logout_BTN.Iconimage"), System.Drawing.Image)
        Me.Logout_BTN.Iconimage_right = Nothing
        Me.Logout_BTN.Iconimage_right_Selected = Nothing
        Me.Logout_BTN.Iconimage_Selected = Nothing
        Me.Logout_BTN.IconMarginLeft = 0
        Me.Logout_BTN.IconMarginRight = 0
        Me.Logout_BTN.IconRightVisible = True
        Me.Logout_BTN.IconRightZoom = 0R
        Me.Logout_BTN.IconVisible = True
        Me.Logout_BTN.IconZoom = 90.0R
        Me.Logout_BTN.IsTab = True
        Me.Logout_BTN.Location = New System.Drawing.Point(45, 851)
        Me.Logout_BTN.Name = "Logout_BTN"
        Me.Logout_BTN.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.Logout_BTN.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.Logout_BTN.OnHoverTextColor = System.Drawing.Color.White
        Me.Logout_BTN.selected = False
        Me.Logout_BTN.Size = New System.Drawing.Size(114, 33)
        Me.Logout_BTN.TabIndex = 9
        Me.Logout_BTN.Tag = "7"
        Me.Logout_BTN.Text = "Logout"
        Me.Logout_BTN.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Logout_BTN.Textcolor = System.Drawing.Color.White
        Me.Logout_BTN.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'MainMenuParametre_B_BTN
        '
        Me.MainMenuParametre_B_BTN.Activecolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.MainMenuParametre_B_BTN.BackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.MainMenuParametre_B_BTN.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.MainMenuParametre_B_BTN.BorderRadius = 0
        Me.MainMenuParametre_B_BTN.ButtonText = "        Parametre"
        Me.MainMenuParametre_B_BTN.Cursor = System.Windows.Forms.Cursors.Default
        Me.MainMenuParametre_B_BTN.DisabledColor = System.Drawing.Color.Gray
        Me.MainMenuParametre_B_BTN.Iconcolor = System.Drawing.Color.Transparent
        Me.MainMenuParametre_B_BTN.Iconimage = Nothing
        Me.MainMenuParametre_B_BTN.Iconimage_right = Nothing
        Me.MainMenuParametre_B_BTN.Iconimage_right_Selected = Nothing
        Me.MainMenuParametre_B_BTN.Iconimage_Selected = Nothing
        Me.MainMenuParametre_B_BTN.IconMarginLeft = 0
        Me.MainMenuParametre_B_BTN.IconMarginRight = 0
        Me.MainMenuParametre_B_BTN.IconRightVisible = True
        Me.MainMenuParametre_B_BTN.IconRightZoom = 0R
        Me.MainMenuParametre_B_BTN.IconVisible = True
        Me.MainMenuParametre_B_BTN.IconZoom = 50.0R
        Me.MainMenuParametre_B_BTN.IsTab = True
        Me.MainMenuParametre_B_BTN.Location = New System.Drawing.Point(0, 486)
        Me.MainMenuParametre_B_BTN.Name = "MainMenuParametre_B_BTN"
        Me.MainMenuParametre_B_BTN.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.MainMenuParametre_B_BTN.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.MainMenuParametre_B_BTN.OnHoverTextColor = System.Drawing.Color.White
        Me.MainMenuParametre_B_BTN.selected = False
        Me.MainMenuParametre_B_BTN.Size = New System.Drawing.Size(200, 80)
        Me.MainMenuParametre_B_BTN.TabIndex = 8
        Me.MainMenuParametre_B_BTN.Tag = "6"
        Me.MainMenuParametre_B_BTN.Text = "        Parametre"
        Me.MainMenuParametre_B_BTN.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MainMenuParametre_B_BTN.Textcolor = System.Drawing.Color.White
        Me.MainMenuParametre_B_BTN.TextFont = New System.Drawing.Font("Candara", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'MainMenuMutuelle_B_BTN
        '
        Me.MainMenuMutuelle_B_BTN.Activecolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.MainMenuMutuelle_B_BTN.BackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.MainMenuMutuelle_B_BTN.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.MainMenuMutuelle_B_BTN.BorderRadius = 0
        Me.MainMenuMutuelle_B_BTN.ButtonText = "        Mutuelle"
        Me.MainMenuMutuelle_B_BTN.Cursor = System.Windows.Forms.Cursors.Default
        Me.MainMenuMutuelle_B_BTN.DisabledColor = System.Drawing.Color.Gray
        Me.MainMenuMutuelle_B_BTN.Iconcolor = System.Drawing.Color.Transparent
        Me.MainMenuMutuelle_B_BTN.Iconimage = Nothing
        Me.MainMenuMutuelle_B_BTN.Iconimage_right = Nothing
        Me.MainMenuMutuelle_B_BTN.Iconimage_right_Selected = Nothing
        Me.MainMenuMutuelle_B_BTN.Iconimage_Selected = Nothing
        Me.MainMenuMutuelle_B_BTN.IconMarginLeft = 0
        Me.MainMenuMutuelle_B_BTN.IconMarginRight = 0
        Me.MainMenuMutuelle_B_BTN.IconRightVisible = True
        Me.MainMenuMutuelle_B_BTN.IconRightZoom = 0R
        Me.MainMenuMutuelle_B_BTN.IconVisible = True
        Me.MainMenuMutuelle_B_BTN.IconZoom = 50.0R
        Me.MainMenuMutuelle_B_BTN.IsTab = True
        Me.MainMenuMutuelle_B_BTN.Location = New System.Drawing.Point(0, 400)
        Me.MainMenuMutuelle_B_BTN.Name = "MainMenuMutuelle_B_BTN"
        Me.MainMenuMutuelle_B_BTN.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.MainMenuMutuelle_B_BTN.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.MainMenuMutuelle_B_BTN.OnHoverTextColor = System.Drawing.Color.White
        Me.MainMenuMutuelle_B_BTN.selected = False
        Me.MainMenuMutuelle_B_BTN.Size = New System.Drawing.Size(200, 80)
        Me.MainMenuMutuelle_B_BTN.TabIndex = 7
        Me.MainMenuMutuelle_B_BTN.Tag = "5"
        Me.MainMenuMutuelle_B_BTN.Text = "        Mutuelle"
        Me.MainMenuMutuelle_B_BTN.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MainMenuMutuelle_B_BTN.Textcolor = System.Drawing.Color.White
        Me.MainMenuMutuelle_B_BTN.TextFont = New System.Drawing.Font("Candara", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'MainMenuTracabilite_B_BTN
        '
        Me.MainMenuTracabilite_B_BTN.Activecolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.MainMenuTracabilite_B_BTN.BackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.MainMenuTracabilite_B_BTN.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.MainMenuTracabilite_B_BTN.BorderRadius = 0
        Me.MainMenuTracabilite_B_BTN.ButtonText = "        Tracabilite"
        Me.MainMenuTracabilite_B_BTN.Cursor = System.Windows.Forms.Cursors.Default
        Me.MainMenuTracabilite_B_BTN.DisabledColor = System.Drawing.Color.Gray
        Me.MainMenuTracabilite_B_BTN.Iconcolor = System.Drawing.Color.Transparent
        Me.MainMenuTracabilite_B_BTN.Iconimage = Nothing
        Me.MainMenuTracabilite_B_BTN.Iconimage_right = Nothing
        Me.MainMenuTracabilite_B_BTN.Iconimage_right_Selected = Nothing
        Me.MainMenuTracabilite_B_BTN.Iconimage_Selected = Nothing
        Me.MainMenuTracabilite_B_BTN.IconMarginLeft = 0
        Me.MainMenuTracabilite_B_BTN.IconMarginRight = 0
        Me.MainMenuTracabilite_B_BTN.IconRightVisible = True
        Me.MainMenuTracabilite_B_BTN.IconRightZoom = 0R
        Me.MainMenuTracabilite_B_BTN.IconVisible = True
        Me.MainMenuTracabilite_B_BTN.IconZoom = 50.0R
        Me.MainMenuTracabilite_B_BTN.IsTab = True
        Me.MainMenuTracabilite_B_BTN.Location = New System.Drawing.Point(0, 320)
        Me.MainMenuTracabilite_B_BTN.Name = "MainMenuTracabilite_B_BTN"
        Me.MainMenuTracabilite_B_BTN.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.MainMenuTracabilite_B_BTN.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.MainMenuTracabilite_B_BTN.OnHoverTextColor = System.Drawing.Color.White
        Me.MainMenuTracabilite_B_BTN.selected = False
        Me.MainMenuTracabilite_B_BTN.Size = New System.Drawing.Size(200, 80)
        Me.MainMenuTracabilite_B_BTN.TabIndex = 6
        Me.MainMenuTracabilite_B_BTN.Tag = "4"
        Me.MainMenuTracabilite_B_BTN.Text = "        Tracabilite"
        Me.MainMenuTracabilite_B_BTN.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MainMenuTracabilite_B_BTN.Textcolor = System.Drawing.Color.White
        Me.MainMenuTracabilite_B_BTN.TextFont = New System.Drawing.Font("Candara", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'MainMenuLaboratoire_B_BTN
        '
        Me.MainMenuLaboratoire_B_BTN.Activecolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.MainMenuLaboratoire_B_BTN.BackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.MainMenuLaboratoire_B_BTN.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.MainMenuLaboratoire_B_BTN.BorderRadius = 0
        Me.MainMenuLaboratoire_B_BTN.ButtonText = "        Laboratoire"
        Me.MainMenuLaboratoire_B_BTN.Cursor = System.Windows.Forms.Cursors.Default
        Me.MainMenuLaboratoire_B_BTN.DisabledColor = System.Drawing.Color.Gray
        Me.MainMenuLaboratoire_B_BTN.Iconcolor = System.Drawing.Color.Transparent
        Me.MainMenuLaboratoire_B_BTN.Iconimage = Nothing
        Me.MainMenuLaboratoire_B_BTN.Iconimage_right = Nothing
        Me.MainMenuLaboratoire_B_BTN.Iconimage_right_Selected = Nothing
        Me.MainMenuLaboratoire_B_BTN.Iconimage_Selected = Nothing
        Me.MainMenuLaboratoire_B_BTN.IconMarginLeft = 0
        Me.MainMenuLaboratoire_B_BTN.IconMarginRight = 0
        Me.MainMenuLaboratoire_B_BTN.IconRightVisible = True
        Me.MainMenuLaboratoire_B_BTN.IconRightZoom = 0R
        Me.MainMenuLaboratoire_B_BTN.IconVisible = True
        Me.MainMenuLaboratoire_B_BTN.IconZoom = 50.0R
        Me.MainMenuLaboratoire_B_BTN.IsTab = True
        Me.MainMenuLaboratoire_B_BTN.Location = New System.Drawing.Point(0, 240)
        Me.MainMenuLaboratoire_B_BTN.Name = "MainMenuLaboratoire_B_BTN"
        Me.MainMenuLaboratoire_B_BTN.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.MainMenuLaboratoire_B_BTN.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.MainMenuLaboratoire_B_BTN.OnHoverTextColor = System.Drawing.Color.White
        Me.MainMenuLaboratoire_B_BTN.selected = False
        Me.MainMenuLaboratoire_B_BTN.Size = New System.Drawing.Size(200, 80)
        Me.MainMenuLaboratoire_B_BTN.TabIndex = 5
        Me.MainMenuLaboratoire_B_BTN.Tag = "3"
        Me.MainMenuLaboratoire_B_BTN.Text = "        Laboratoire"
        Me.MainMenuLaboratoire_B_BTN.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MainMenuLaboratoire_B_BTN.Textcolor = System.Drawing.Color.White
        Me.MainMenuLaboratoire_B_BTN.TextFont = New System.Drawing.Font("Candara", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'MainMenuOrdonnance_B_BTN
        '
        Me.MainMenuOrdonnance_B_BTN.Activecolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.MainMenuOrdonnance_B_BTN.BackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.MainMenuOrdonnance_B_BTN.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.MainMenuOrdonnance_B_BTN.BorderRadius = 0
        Me.MainMenuOrdonnance_B_BTN.ButtonText = "        Ordonnance"
        Me.MainMenuOrdonnance_B_BTN.Cursor = System.Windows.Forms.Cursors.Default
        Me.MainMenuOrdonnance_B_BTN.DisabledColor = System.Drawing.Color.Gray
        Me.MainMenuOrdonnance_B_BTN.Iconcolor = System.Drawing.Color.Transparent
        Me.MainMenuOrdonnance_B_BTN.Iconimage = Nothing
        Me.MainMenuOrdonnance_B_BTN.Iconimage_right = Nothing
        Me.MainMenuOrdonnance_B_BTN.Iconimage_right_Selected = Nothing
        Me.MainMenuOrdonnance_B_BTN.Iconimage_Selected = Nothing
        Me.MainMenuOrdonnance_B_BTN.IconMarginLeft = 0
        Me.MainMenuOrdonnance_B_BTN.IconMarginRight = 0
        Me.MainMenuOrdonnance_B_BTN.IconRightVisible = True
        Me.MainMenuOrdonnance_B_BTN.IconRightZoom = 0R
        Me.MainMenuOrdonnance_B_BTN.IconVisible = True
        Me.MainMenuOrdonnance_B_BTN.IconZoom = 50.0R
        Me.MainMenuOrdonnance_B_BTN.IsTab = True
        Me.MainMenuOrdonnance_B_BTN.Location = New System.Drawing.Point(0, 160)
        Me.MainMenuOrdonnance_B_BTN.Name = "MainMenuOrdonnance_B_BTN"
        Me.MainMenuOrdonnance_B_BTN.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.MainMenuOrdonnance_B_BTN.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.MainMenuOrdonnance_B_BTN.OnHoverTextColor = System.Drawing.Color.White
        Me.MainMenuOrdonnance_B_BTN.selected = False
        Me.MainMenuOrdonnance_B_BTN.Size = New System.Drawing.Size(201, 80)
        Me.MainMenuOrdonnance_B_BTN.TabIndex = 4
        Me.MainMenuOrdonnance_B_BTN.Tag = "2"
        Me.MainMenuOrdonnance_B_BTN.Text = "        Ordonnance"
        Me.MainMenuOrdonnance_B_BTN.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MainMenuOrdonnance_B_BTN.Textcolor = System.Drawing.Color.White
        Me.MainMenuOrdonnance_B_BTN.TextFont = New System.Drawing.Font("Candara", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'MainMenuStock_B_BTN
        '
        Me.MainMenuStock_B_BTN.Activecolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.MainMenuStock_B_BTN.BackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.MainMenuStock_B_BTN.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.MainMenuStock_B_BTN.BorderRadius = 0
        Me.MainMenuStock_B_BTN.ButtonText = "        Stock"
        Me.MainMenuStock_B_BTN.Cursor = System.Windows.Forms.Cursors.Default
        Me.MainMenuStock_B_BTN.DisabledColor = System.Drawing.Color.Gray
        Me.MainMenuStock_B_BTN.Iconcolor = System.Drawing.Color.Transparent
        Me.MainMenuStock_B_BTN.Iconimage = Global.GestionDeStock.My.Resources.Resources.icons8_Dashboard_96
        Me.MainMenuStock_B_BTN.Iconimage_right = Nothing
        Me.MainMenuStock_B_BTN.Iconimage_right_Selected = Nothing
        Me.MainMenuStock_B_BTN.Iconimage_Selected = Nothing
        Me.MainMenuStock_B_BTN.IconMarginLeft = 0
        Me.MainMenuStock_B_BTN.IconMarginRight = 0
        Me.MainMenuStock_B_BTN.IconRightVisible = True
        Me.MainMenuStock_B_BTN.IconRightZoom = 0R
        Me.MainMenuStock_B_BTN.IconVisible = True
        Me.MainMenuStock_B_BTN.IconZoom = 50.0R
        Me.MainMenuStock_B_BTN.IsTab = True
        Me.MainMenuStock_B_BTN.Location = New System.Drawing.Point(0, 80)
        Me.MainMenuStock_B_BTN.Name = "MainMenuStock_B_BTN"
        Me.MainMenuStock_B_BTN.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.MainMenuStock_B_BTN.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.MainMenuStock_B_BTN.OnHoverTextColor = System.Drawing.Color.White
        Me.MainMenuStock_B_BTN.selected = False
        Me.MainMenuStock_B_BTN.Size = New System.Drawing.Size(201, 80)
        Me.MainMenuStock_B_BTN.TabIndex = 3
        Me.MainMenuStock_B_BTN.Tag = "1"
        Me.MainMenuStock_B_BTN.Text = "        Stock"
        Me.MainMenuStock_B_BTN.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MainMenuStock_B_BTN.Textcolor = System.Drawing.Color.White
        Me.MainMenuStock_B_BTN.TextFont = New System.Drawing.Font("Candara", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'SlideMenuRight_IMG
        '
        Me.SlideMenuRight_IMG.BackColor = System.Drawing.Color.FromArgb(CType(CType(37, Byte), Integer), CType(CType(46, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.SlideMenuRight_IMG.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Menu_96
        Me.SlideMenuRight_IMG.ImageActive = Nothing
        Me.SlideMenuRight_IMG.Location = New System.Drawing.Point(213, 6)
        Me.SlideMenuRight_IMG.Name = "SlideMenuRight_IMG"
        Me.SlideMenuRight_IMG.Size = New System.Drawing.Size(25, 25)
        Me.SlideMenuRight_IMG.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.SlideMenuRight_IMG.TabIndex = 2
        Me.SlideMenuRight_IMG.TabStop = False
        Me.SlideMenuRight_IMG.Zoom = 10
        '
        'MainMenuDashboard_BTN
        '
        Me.MainMenuDashboard_BTN.Activecolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.MainMenuDashboard_BTN.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.MainMenuDashboard_BTN.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.MainMenuDashboard_BTN.BorderRadius = 0
        Me.MainMenuDashboard_BTN.ButtonText = "        Dashboard"
        Me.MainMenuDashboard_BTN.Cursor = System.Windows.Forms.Cursors.Default
        Me.MainMenuDashboard_BTN.DisabledColor = System.Drawing.Color.Gray
        Me.MainMenuDashboard_BTN.Iconcolor = System.Drawing.Color.Transparent
        Me.MainMenuDashboard_BTN.Iconimage = Nothing
        Me.MainMenuDashboard_BTN.Iconimage_right = Nothing
        Me.MainMenuDashboard_BTN.Iconimage_right_Selected = Nothing
        Me.MainMenuDashboard_BTN.Iconimage_Selected = Nothing
        Me.MainMenuDashboard_BTN.IconMarginLeft = 0
        Me.MainMenuDashboard_BTN.IconMarginRight = 0
        Me.MainMenuDashboard_BTN.IconRightVisible = True
        Me.MainMenuDashboard_BTN.IconRightZoom = 0R
        Me.MainMenuDashboard_BTN.IconVisible = True
        Me.MainMenuDashboard_BTN.IconZoom = 50.0R
        Me.MainMenuDashboard_BTN.IsTab = True
        Me.MainMenuDashboard_BTN.Location = New System.Drawing.Point(0, 0)
        Me.MainMenuDashboard_BTN.Name = "MainMenuDashboard_BTN"
        Me.MainMenuDashboard_BTN.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.MainMenuDashboard_BTN.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.MainMenuDashboard_BTN.OnHoverTextColor = System.Drawing.Color.White
        Me.MainMenuDashboard_BTN.selected = True
        Me.MainMenuDashboard_BTN.Size = New System.Drawing.Size(200, 80)
        Me.MainMenuDashboard_BTN.TabIndex = 1
        Me.MainMenuDashboard_BTN.Tag = "0"
        Me.MainMenuDashboard_BTN.Text = "        Dashboard"
        Me.MainMenuDashboard_BTN.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MainMenuDashboard_BTN.Textcolor = System.Drawing.Color.White
        Me.MainMenuDashboard_BTN.TextFont = New System.Drawing.Font("Candara", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'TopPanel
        '
        Me.TopPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TopPanel.Controls.Add(Me.SubMenuButton_5_BTN)
        Me.TopPanel.Controls.Add(Me.SubMenuButton_4_BTN)
        Me.TopPanel.Controls.Add(Me.SubMenuButton_3_BTN)
        Me.TopPanel.Controls.Add(Me.SubMenuButton_2_BTN)
        Me.TopPanel.Controls.Add(Me.Indicator)
        Me.TopPanel.Controls.Add(Me.SubMenuButton_1_BTN)
        Me.TopPanel.Controls.Add(Me.PanelDragger)
        Me.TopPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me.TopPanel.ForeColor = System.Drawing.Color.Silver
        Me.TopPanel.Location = New System.Drawing.Point(0, 0)
        Me.TopPanel.Name = "TopPanel"
        Me.TopPanel.Size = New System.Drawing.Size(1366, 100)
        Me.TopPanel.TabIndex = 1
        '
        'SubMenuButton_5_BTN
        '
        Me.SubMenuButton_5_BTN.BackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.SubMenuButton_5_BTN.color = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.SubMenuButton_5_BTN.colorActive = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.SubMenuButton_5_BTN.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SubMenuButton_5_BTN.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SubMenuButton_5_BTN.ForeColor = System.Drawing.Color.White
        Me.SubMenuButton_5_BTN.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Product
        Me.SubMenuButton_5_BTN.ImagePosition = 7
        Me.SubMenuButton_5_BTN.ImageZoom = 35
        Me.SubMenuButton_5_BTN.LabelPosition = 20
        Me.SubMenuButton_5_BTN.LabelText = "Fournisseur"
        Me.SubMenuButton_5_BTN.Location = New System.Drawing.Point(624, 11)
        Me.SubMenuButton_5_BTN.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.SubMenuButton_5_BTN.Name = "SubMenuButton_5_BTN"
        Me.SubMenuButton_5_BTN.Size = New System.Drawing.Size(110, 82)
        Me.SubMenuButton_5_BTN.TabIndex = 7
        Me.SubMenuButton_5_BTN.Tag = "4"
        Me.SubMenuButton_5_BTN.Visible = False
        '
        'SubMenuButton_4_BTN
        '
        Me.SubMenuButton_4_BTN.BackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.SubMenuButton_4_BTN.color = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.SubMenuButton_4_BTN.colorActive = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.SubMenuButton_4_BTN.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SubMenuButton_4_BTN.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SubMenuButton_4_BTN.ForeColor = System.Drawing.Color.White
        Me.SubMenuButton_4_BTN.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Product
        Me.SubMenuButton_4_BTN.ImagePosition = 7
        Me.SubMenuButton_4_BTN.ImageZoom = 35
        Me.SubMenuButton_4_BTN.LabelPosition = 20
        Me.SubMenuButton_4_BTN.LabelText = "Articles"
        Me.SubMenuButton_4_BTN.Location = New System.Drawing.Point(506, 11)
        Me.SubMenuButton_4_BTN.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.SubMenuButton_4_BTN.Name = "SubMenuButton_4_BTN"
        Me.SubMenuButton_4_BTN.Size = New System.Drawing.Size(110, 82)
        Me.SubMenuButton_4_BTN.TabIndex = 6
        Me.SubMenuButton_4_BTN.Tag = "3"
        Me.SubMenuButton_4_BTN.Visible = False
        '
        'SubMenuButton_3_BTN
        '
        Me.SubMenuButton_3_BTN.BackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.SubMenuButton_3_BTN.color = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.SubMenuButton_3_BTN.colorActive = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.SubMenuButton_3_BTN.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SubMenuButton_3_BTN.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SubMenuButton_3_BTN.ForeColor = System.Drawing.Color.White
        Me.SubMenuButton_3_BTN.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Product
        Me.SubMenuButton_3_BTN.ImagePosition = 7
        Me.SubMenuButton_3_BTN.ImageZoom = 35
        Me.SubMenuButton_3_BTN.LabelPosition = 20
        Me.SubMenuButton_3_BTN.LabelText = "Stock"
        Me.SubMenuButton_3_BTN.Location = New System.Drawing.Point(388, 11)
        Me.SubMenuButton_3_BTN.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.SubMenuButton_3_BTN.Name = "SubMenuButton_3_BTN"
        Me.SubMenuButton_3_BTN.Size = New System.Drawing.Size(110, 82)
        Me.SubMenuButton_3_BTN.TabIndex = 5
        Me.SubMenuButton_3_BTN.Tag = "2"
        Me.SubMenuButton_3_BTN.Visible = False
        '
        'SubMenuButton_2_BTN
        '
        Me.SubMenuButton_2_BTN.BackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.SubMenuButton_2_BTN.color = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.SubMenuButton_2_BTN.colorActive = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.SubMenuButton_2_BTN.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SubMenuButton_2_BTN.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SubMenuButton_2_BTN.ForeColor = System.Drawing.Color.White
        Me.SubMenuButton_2_BTN.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Product
        Me.SubMenuButton_2_BTN.ImagePosition = 7
        Me.SubMenuButton_2_BTN.ImageZoom = 35
        Me.SubMenuButton_2_BTN.LabelPosition = 20
        Me.SubMenuButton_2_BTN.LabelText = "Receptions"
        Me.SubMenuButton_2_BTN.Location = New System.Drawing.Point(893, 11)
        Me.SubMenuButton_2_BTN.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.SubMenuButton_2_BTN.Name = "SubMenuButton_2_BTN"
        Me.SubMenuButton_2_BTN.Size = New System.Drawing.Size(110, 82)
        Me.SubMenuButton_2_BTN.TabIndex = 3
        Me.SubMenuButton_2_BTN.Tag = "1"
        Me.SubMenuButton_2_BTN.Visible = False
        '
        'Indicator
        '
        Me.Indicator.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Indicator.Location = New System.Drawing.Point(270, 91)
        Me.Indicator.Name = "Indicator"
        Me.Indicator.Size = New System.Drawing.Size(110, 10)
        Me.Indicator.TabIndex = 2
        Me.Indicator.TabStop = False
        Me.Indicator.Visible = False
        '
        'SubMenuButton_1_BTN
        '
        Me.SubMenuButton_1_BTN.BackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.SubMenuButton_1_BTN.color = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.SubMenuButton_1_BTN.colorActive = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.SubMenuButton_1_BTN.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SubMenuButton_1_BTN.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SubMenuButton_1_BTN.ForeColor = System.Drawing.Color.White
        Me.SubMenuButton_1_BTN.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Product
        Me.SubMenuButton_1_BTN.ImagePosition = 7
        Me.SubMenuButton_1_BTN.ImageZoom = 35
        Me.SubMenuButton_1_BTN.LabelPosition = 20
        Me.SubMenuButton_1_BTN.LabelText = "Commandes"
        Me.SubMenuButton_1_BTN.Location = New System.Drawing.Point(270, 11)
        Me.SubMenuButton_1_BTN.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.SubMenuButton_1_BTN.Name = "SubMenuButton_1_BTN"
        Me.SubMenuButton_1_BTN.Size = New System.Drawing.Size(110, 82)
        Me.SubMenuButton_1_BTN.TabIndex = 1
        Me.SubMenuButton_1_BTN.Tag = "0"
        Me.SubMenuButton_1_BTN.Visible = False
        '
        'PanelDragger
        '
        Me.PanelDragger.Controls.Add(Me.Close_BTN)
        Me.PanelDragger.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelDragger.Location = New System.Drawing.Point(0, 0)
        Me.PanelDragger.Name = "PanelDragger"
        Me.PanelDragger.Size = New System.Drawing.Size(1366, 29)
        Me.PanelDragger.TabIndex = 4
        '
        'Close_BTN
        '
        Me.Close_BTN.BackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Close_BTN.Dock = System.Windows.Forms.DockStyle.Right
        Me.Close_BTN.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Delete
        Me.Close_BTN.ImageActive = Nothing
        Me.Close_BTN.Location = New System.Drawing.Point(1308, 0)
        Me.Close_BTN.Name = "Close_BTN"
        Me.Close_BTN.Size = New System.Drawing.Size(58, 29)
        Me.Close_BTN.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Close_BTN.TabIndex = 5
        Me.Close_BTN.TabStop = False
        Me.Close_BTN.Zoom = 0
        '
        'NDataGridViewTextBoxColumn
        '
        Me.NDataGridViewTextBoxColumn.DataPropertyName = "N"
        Me.NDataGridViewTextBoxColumn.HeaderText = "N"
        Me.NDataGridViewTextBoxColumn.Name = "NDataGridViewTextBoxColumn"
        Me.NDataGridViewTextBoxColumn.ReadOnly = True
        '
        'UniteDataGridViewTextBoxColumn
        '
        Me.UniteDataGridViewTextBoxColumn.DataPropertyName = "unite"
        Me.UniteDataGridViewTextBoxColumn.HeaderText = "unite"
        Me.UniteDataGridViewTextBoxColumn.Name = "UniteDataGridViewTextBoxColumn"
        Me.UniteDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PNL_Stock_Commande
        '
        Me.PNL_Stock_Commande.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.PNL_Stock_Commande.Controls.Add(Me.Commande_GeneralData1)
        Me.PNL_Stock_Commande.Controls.Add(Me.ListOfOrders)
        Me.PNL_Stock_Commande.Controls.Add(Me.SearchAreaOrders)
        Me.PNL_Stock_Commande.Location = New System.Drawing.Point(206, 106)
        Me.PNL_Stock_Commande.Name = "PNL_Stock_Commande"
        Me.PNL_Stock_Commande.Size = New System.Drawing.Size(1179, 764)
        Me.PNL_Stock_Commande.TabIndex = 1
        Me.PNL_Stock_Commande.Visible = False
        '
        'Commande_GeneralData1
        '
        Me.Commande_GeneralData1.BackColor = System.Drawing.Color.White
        Me.Commande_GeneralData1.Controls.Add(Me.TotalMoneyToSuppliers)
        Me.Commande_GeneralData1.Controls.Add(Me.UnpaidOrders)
        Me.Commande_GeneralData1.Location = New System.Drawing.Point(760, 20)
        Me.Commande_GeneralData1.Name = "Commande_GeneralData1"
        Me.Commande_GeneralData1.Size = New System.Drawing.Size(257, 250)
        Me.Commande_GeneralData1.TabIndex = 18
        '
        'TotalMoneyToSuppliers
        '
        Me.TotalMoneyToSuppliers.AutoSize = True
        Me.TotalMoneyToSuppliers.Font = New System.Drawing.Font("Century Gothic", 36.0!)
        Me.TotalMoneyToSuppliers.ForeColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.TotalMoneyToSuppliers.Location = New System.Drawing.Point(34, 82)
        Me.TotalMoneyToSuppliers.Name = "TotalMoneyToSuppliers"
        Me.TotalMoneyToSuppliers.Size = New System.Drawing.Size(124, 58)
        Me.TotalMoneyToSuppliers.TabIndex = 2
        Me.TotalMoneyToSuppliers.Text = "Error"
        Me.TotalMoneyToSuppliers.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'UnpaidOrders
        '
        Me.UnpaidOrders.AutoSize = True
        Me.UnpaidOrders.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.UnpaidOrders.Location = New System.Drawing.Point(31, 210)
        Me.UnpaidOrders.Name = "UnpaidOrders"
        Me.UnpaidOrders.Size = New System.Drawing.Size(195, 21)
        Me.UnpaidOrders.TabIndex = 1
        Me.UnpaidOrders.Text = "A payer aux fournisseurs"
        Me.UnpaidOrders.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListOfOrders
        '
        Me.ListOfOrders.BackColor = System.Drawing.Color.White
        Me.ListOfOrders.Controls.Add(Me.PayOrder_BTN)
        Me.ListOfOrders.Controls.Add(Me.Delete_Order_Oders_BTN)
        Me.ListOfOrders.Controls.Add(Me.Modify_Order_Oders_BTN)
        Me.ListOfOrders.Controls.Add(Me.Add_Order_Oders_BTN)
        Me.ListOfOrders.Controls.Add(Me.ListeDeCommande_LBL)
        Me.ListOfOrders.Controls.Add(Me.dgv1)
        Me.ListOfOrders.Location = New System.Drawing.Point(23, 276)
        Me.ListOfOrders.Name = "ListOfOrders"
        Me.ListOfOrders.Size = New System.Drawing.Size(1121, 476)
        Me.ListOfOrders.TabIndex = 16
        '
        'PayOrder_BTN
        '
        Me.PayOrder_BTN.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.PayOrder_BTN.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Inspection
        Me.PayOrder_BTN.ImageActive = Nothing
        Me.PayOrder_BTN.Location = New System.Drawing.Point(875, 14)
        Me.PayOrder_BTN.Name = "PayOrder_BTN"
        Me.PayOrder_BTN.Size = New System.Drawing.Size(46, 47)
        Me.PayOrder_BTN.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PayOrder_BTN.TabIndex = 15
        Me.PayOrder_BTN.TabStop = False
        Me.PayOrder_BTN.Zoom = 5
        '
        'Delete_Order_Oders_BTN
        '
        Me.Delete_Order_Oders_BTN.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.Delete_Order_Oders_BTN.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Delete_Row
        Me.Delete_Order_Oders_BTN.ImageActive = Nothing
        Me.Delete_Order_Oders_BTN.Location = New System.Drawing.Point(1031, 14)
        Me.Delete_Order_Oders_BTN.Name = "Delete_Order_Oders_BTN"
        Me.Delete_Order_Oders_BTN.Size = New System.Drawing.Size(46, 47)
        Me.Delete_Order_Oders_BTN.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Delete_Order_Oders_BTN.TabIndex = 14
        Me.Delete_Order_Oders_BTN.TabStop = False
        Me.Delete_Order_Oders_BTN.Zoom = 10
        '
        'Modify_Order_Oders_BTN
        '
        Me.Modify_Order_Oders_BTN.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.Modify_Order_Oders_BTN.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Edit_Row
        Me.Modify_Order_Oders_BTN.ImageActive = Nothing
        Me.Modify_Order_Oders_BTN.Location = New System.Drawing.Point(979, 14)
        Me.Modify_Order_Oders_BTN.Name = "Modify_Order_Oders_BTN"
        Me.Modify_Order_Oders_BTN.Size = New System.Drawing.Size(46, 47)
        Me.Modify_Order_Oders_BTN.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Modify_Order_Oders_BTN.TabIndex = 13
        Me.Modify_Order_Oders_BTN.TabStop = False
        Me.Modify_Order_Oders_BTN.Zoom = 10
        '
        'Add_Order_Oders_BTN
        '
        Me.Add_Order_Oders_BTN.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.Add_Order_Oders_BTN.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Add_Row
        Me.Add_Order_Oders_BTN.ImageActive = Nothing
        Me.Add_Order_Oders_BTN.Location = New System.Drawing.Point(927, 14)
        Me.Add_Order_Oders_BTN.Name = "Add_Order_Oders_BTN"
        Me.Add_Order_Oders_BTN.Size = New System.Drawing.Size(46, 47)
        Me.Add_Order_Oders_BTN.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Add_Order_Oders_BTN.TabIndex = 12
        Me.Add_Order_Oders_BTN.TabStop = False
        Me.Add_Order_Oders_BTN.Zoom = 10
        '
        'ListeDeCommande_LBL
        '
        Me.ListeDeCommande_LBL.AutoSize = True
        Me.ListeDeCommande_LBL.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.ListeDeCommande_LBL.Location = New System.Drawing.Point(476, 25)
        Me.ListeDeCommande_LBL.Name = "ListeDeCommande_LBL"
        Me.ListeDeCommande_LBL.Size = New System.Drawing.Size(173, 21)
        Me.ListeDeCommande_LBL.TabIndex = 11
        Me.ListeDeCommande_LBL.Text = "Liste de Commandes"
        '
        'dgv1
        '
        Me.dgv1.AllowUserToAddRows = False
        Me.dgv1.AllowUserToDeleteRows = False
        Me.dgv1.AllowUserToResizeColumns = False
        Me.dgv1.AllowUserToResizeRows = False
        DataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle27.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle27.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle27.NullValue = "-"
        DataGridViewCellStyle27.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        DataGridViewCellStyle27.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.dgv1.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle27
        Me.dgv1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgv1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgv1.BackgroundColor = System.Drawing.Color.White
        Me.dgv1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgv1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgv1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle28.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        DataGridViewCellStyle28.Font = New System.Drawing.Font("Century Gothic", 10.0!)
        DataGridViewCellStyle28.ForeColor = System.Drawing.Color.FromArgb(CType(CType(37, Byte), Integer), CType(CType(46, Byte), Integer), CType(CType(59, Byte), Integer))
        DataGridViewCellStyle28.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        DataGridViewCellStyle28.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(37, Byte), Integer), CType(CType(46, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.dgv1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle28
        Me.dgv1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle29.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle29.Font = New System.Drawing.Font("Century Gothic", 10.0!)
        DataGridViewCellStyle29.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle29.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        DataGridViewCellStyle29.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv1.DefaultCellStyle = DataGridViewCellStyle29
        Me.dgv1.DoubleBuffered = True
        Me.dgv1.EnableHeadersVisualStyles = False
        Me.dgv1.GridColor = System.Drawing.Color.White
        Me.dgv1.HeaderBgColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.dgv1.HeaderForeColor = System.Drawing.Color.FromArgb(CType(CType(37, Byte), Integer), CType(CType(46, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.dgv1.Location = New System.Drawing.Point(7, 67)
        Me.dgv1.Name = "dgv1"
        Me.dgv1.ReadOnly = True
        Me.dgv1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.dgv1.RowHeadersVisible = False
        Me.dgv1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        Me.dgv1.RowTemplate.Height = 35
        Me.dgv1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv1.ShowCellErrors = False
        Me.dgv1.ShowCellToolTips = False
        Me.dgv1.ShowEditingIcon = False
        Me.dgv1.ShowRowErrors = False
        Me.dgv1.Size = New System.Drawing.Size(1099, 396)
        Me.dgv1.TabIndex = 9
        '
        'SearchAreaOrders
        '
        Me.SearchAreaOrders.BackColor = System.Drawing.Color.White
        Me.SearchAreaOrders.Controls.Add(Me.Au_DTP)
        Me.SearchAreaOrders.Controls.Add(Me.De_DTP)
        Me.SearchAreaOrders.Controls.Add(Me.BunifuCustomLabel7)
        Me.SearchAreaOrders.Controls.Add(Me.Rechercher_Fournisseur_CMB)
        Me.SearchAreaOrders.Controls.Add(Me.BunifuCustomLabel4)
        Me.SearchAreaOrders.Controls.Add(Me.UseDateInSearch)
        Me.SearchAreaOrders.Controls.Add(Me.BunifuCustomLabel2)
        Me.SearchAreaOrders.Controls.Add(Me.Rechercher_Code_TXB)
        Me.SearchAreaOrders.Location = New System.Drawing.Point(60, 20)
        Me.SearchAreaOrders.Name = "SearchAreaOrders"
        Me.SearchAreaOrders.Size = New System.Drawing.Size(334, 250)
        Me.SearchAreaOrders.TabIndex = 15
        '
        'Au_DTP
        '
        Me.Au_DTP.CustomFormat = "dd-MM-yyyy"
        Me.Au_DTP.Enabled = False
        Me.Au_DTP.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Au_DTP.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.Au_DTP.Location = New System.Drawing.Point(114, 229)
        Me.Au_DTP.Name = "Au_DTP"
        Me.Au_DTP.Size = New System.Drawing.Size(125, 23)
        Me.Au_DTP.TabIndex = 28
        Me.Au_DTP.Visible = False
        '
        'De_DTP
        '
        Me.De_DTP.CalendarFont = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.De_DTP.CalendarForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.De_DTP.CalendarMonthBackground = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.De_DTP.CalendarTitleBackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.De_DTP.CalendarTitleForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.De_DTP.CustomFormat = "dd-MM-yyyy"
        Me.De_DTP.Enabled = False
        Me.De_DTP.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.De_DTP.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.De_DTP.Location = New System.Drawing.Point(114, 198)
        Me.De_DTP.Name = "De_DTP"
        Me.De_DTP.Size = New System.Drawing.Size(125, 23)
        Me.De_DTP.TabIndex = 27
        Me.De_DTP.Value = New Date(2017, 8, 7, 0, 0, 0, 0)
        Me.De_DTP.Visible = False
        '
        'BunifuCustomLabel7
        '
        Me.BunifuCustomLabel7.AutoSize = True
        Me.BunifuCustomLabel7.Font = New System.Drawing.Font("Century Gothic", 10.0!)
        Me.BunifuCustomLabel7.Location = New System.Drawing.Point(13, 117)
        Me.BunifuCustomLabel7.Name = "BunifuCustomLabel7"
        Me.BunifuCustomLabel7.Size = New System.Drawing.Size(94, 19)
        Me.BunifuCustomLabel7.TabIndex = 26
        Me.BunifuCustomLabel7.Text = "Fournisseur : "
        '
        'Rechercher_Fournisseur_CMB
        '
        Me.Rechercher_Fournisseur_CMB.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Rechercher_Fournisseur_CMB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Rechercher_Fournisseur_CMB.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Rechercher_Fournisseur_CMB.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.Rechercher_Fournisseur_CMB.FormattingEnabled = True
        Me.Rechercher_Fournisseur_CMB.ItemHeight = 17
        Me.Rechercher_Fournisseur_CMB.Items.AddRange(New Object() {"Etat : Tous", "En cours ", "Reçue", "Annulée"})
        Me.Rechercher_Fournisseur_CMB.Location = New System.Drawing.Point(110, 115)
        Me.Rechercher_Fournisseur_CMB.MaxDropDownItems = 15
        Me.Rechercher_Fournisseur_CMB.Name = "Rechercher_Fournisseur_CMB"
        Me.Rechercher_Fournisseur_CMB.Size = New System.Drawing.Size(210, 25)
        Me.Rechercher_Fournisseur_CMB.TabIndex = 24
        '
        'BunifuCustomLabel4
        '
        Me.BunifuCustomLabel4.AutoSize = True
        Me.BunifuCustomLabel4.Enabled = False
        Me.BunifuCustomLabel4.Font = New System.Drawing.Font("Century Gothic", 10.0!)
        Me.BunifuCustomLabel4.Location = New System.Drawing.Point(59, 165)
        Me.BunifuCustomLabel4.Name = "BunifuCustomLabel4"
        Me.BunifuCustomLabel4.Size = New System.Drawing.Size(165, 19)
        Me.BunifuCustomLabel4.TabIndex = 21
        Me.BunifuCustomLabel4.Text = "Rechercher Par Date : "
        Me.BunifuCustomLabel4.Visible = False
        '
        'UseDateInSearch
        '
        Me.UseDateInSearch.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.UseDateInSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.UseDateInSearch.BorderRadius = 10
        Me.UseDateInSearch.Cursor = System.Windows.Forms.Cursors.Hand
        Me.UseDateInSearch.Enabled = False
        Me.UseDateInSearch.ForeColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.UseDateInSearch.Location = New System.Drawing.Point(240, 167)
        Me.UseDateInSearch.Name = "UseDateInSearch"
        Me.UseDateInSearch.Oncolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.UseDateInSearch.Onoffcolor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(35, Byte), Integer), CType(CType(26, Byte), Integer))
        Me.UseDateInSearch.Size = New System.Drawing.Size(51, 19)
        Me.UseDateInSearch.TabIndex = 20
        Me.UseDateInSearch.Textcolor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.UseDateInSearch.Value = False
        Me.UseDateInSearch.Visible = False
        '
        'BunifuCustomLabel2
        '
        Me.BunifuCustomLabel2.AutoSize = True
        Me.BunifuCustomLabel2.Font = New System.Drawing.Font("Century Gothic", 10.0!)
        Me.BunifuCustomLabel2.Location = New System.Drawing.Point(93, 12)
        Me.BunifuCustomLabel2.Name = "BunifuCustomLabel2"
        Me.BunifuCustomLabel2.Size = New System.Drawing.Size(167, 19)
        Me.BunifuCustomLabel2.TabIndex = 18
        Me.BunifuCustomLabel2.Text = "Criteres de Recherches"
        '
        'Rechercher_Code_TXB
        '
        Me.Rechercher_Code_TXB.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Rechercher_Code_TXB.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Rechercher_Code_TXB.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Rechercher_Code_TXB.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Rechercher_Code_TXB.BorderThickness = 3
        Me.Rechercher_Code_TXB.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Rechercher_Code_TXB.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.Rechercher_Code_TXB.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.Rechercher_Code_TXB.isPassword = False
        Me.Rechercher_Code_TXB.Location = New System.Drawing.Point(17, 48)
        Me.Rechercher_Code_TXB.Margin = New System.Windows.Forms.Padding(4)
        Me.Rechercher_Code_TXB.Name = "Rechercher_Code_TXB"
        Me.Rechercher_Code_TXB.Size = New System.Drawing.Size(304, 36)
        Me.Rechercher_Code_TXB.TabIndex = 14
        Me.Rechercher_Code_TXB.Text = "Nom D'Article"
        Me.Rechercher_Code_TXB.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'PNL_Stock_Stock
        '
        Me.PNL_Stock_Stock.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.PNL_Stock_Stock.Controls.Add(Me.HighPanel)
        Me.PNL_Stock_Stock.Controls.Add(Me.Stock_SearchArea)
        Me.PNL_Stock_Stock.Controls.Add(Me.LowPanel)
        Me.PNL_Stock_Stock.Controls.Add(Me.ListOfArticlesInStock)
        Me.PNL_Stock_Stock.Location = New System.Drawing.Point(206, 106)
        Me.PNL_Stock_Stock.Name = "PNL_Stock_Stock"
        Me.PNL_Stock_Stock.Size = New System.Drawing.Size(1184, 764)
        Me.PNL_Stock_Stock.TabIndex = 4
        Me.PNL_Stock_Stock.Visible = False
        '
        'HighPanel
        '
        Me.HighPanel.BackColor = System.Drawing.Color.White
        Me.HighPanel.Controls.Add(Me.Low_LBL)
        Me.HighPanel.Controls.Add(Me.HighAlert)
        Me.HighPanel.Location = New System.Drawing.Point(572, 18)
        Me.HighPanel.Name = "HighPanel"
        Me.HighPanel.Size = New System.Drawing.Size(272, 172)
        Me.HighPanel.TabIndex = 3
        '
        'Low_LBL
        '
        Me.Low_LBL.AutoSize = True
        Me.Low_LBL.Font = New System.Drawing.Font("Century Gothic", 36.0!)
        Me.Low_LBL.ForeColor = System.Drawing.Color.FromArgb(CType(CType(190, Byte), Integer), CType(CType(200, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Low_LBL.Location = New System.Drawing.Point(116, 41)
        Me.Low_LBL.Name = "Low_LBL"
        Me.Low_LBL.Size = New System.Drawing.Size(52, 58)
        Me.Low_LBL.TabIndex = 3
        Me.Low_LBL.Text = "0"
        Me.Low_LBL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'HighAlert
        '
        Me.HighAlert.AutoSize = True
        Me.HighAlert.Font = New System.Drawing.Font("Century Gothic", 11.0!)
        Me.HighAlert.Location = New System.Drawing.Point(13, 131)
        Me.HighAlert.Name = "HighAlert"
        Me.HighAlert.Size = New System.Drawing.Size(247, 20)
        Me.HighAlert.TabIndex = 2
        Me.HighAlert.Text = "Articles en etat critique Minimum"
        Me.HighAlert.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Stock_SearchArea
        '
        Me.Stock_SearchArea.BackColor = System.Drawing.Color.White
        Me.Stock_SearchArea.Controls.Add(Me.article_name_TXB)
        Me.Stock_SearchArea.Controls.Add(Me.BunifuCustomLabel14)
        Me.Stock_SearchArea.Location = New System.Drawing.Point(33, 17)
        Me.Stock_SearchArea.Name = "Stock_SearchArea"
        Me.Stock_SearchArea.Size = New System.Drawing.Size(383, 172)
        Me.Stock_SearchArea.TabIndex = 2
        '
        'article_name_TXB
        '
        Me.article_name_TXB.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.article_name_TXB.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.article_name_TXB.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.article_name_TXB.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.article_name_TXB.BorderThickness = 3
        Me.article_name_TXB.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.article_name_TXB.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.article_name_TXB.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.article_name_TXB.isPassword = False
        Me.article_name_TXB.Location = New System.Drawing.Point(60, 94)
        Me.article_name_TXB.Margin = New System.Windows.Forms.Padding(5)
        Me.article_name_TXB.Name = "article_name_TXB"
        Me.article_name_TXB.Size = New System.Drawing.Size(258, 40)
        Me.article_name_TXB.TabIndex = 17
        Me.article_name_TXB.Text = "Nom D'Article"
        Me.article_name_TXB.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'BunifuCustomLabel14
        '
        Me.BunifuCustomLabel14.AutoSize = True
        Me.BunifuCustomLabel14.Location = New System.Drawing.Point(133, 20)
        Me.BunifuCustomLabel14.Name = "BunifuCustomLabel14"
        Me.BunifuCustomLabel14.Size = New System.Drawing.Size(112, 13)
        Me.BunifuCustomLabel14.TabIndex = 0
        Me.BunifuCustomLabel14.Text = "criteres de Recherche"
        '
        'LowPanel
        '
        Me.LowPanel.BackColor = System.Drawing.Color.White
        Me.LowPanel.Controls.Add(Me.High_LBL)
        Me.LowPanel.Controls.Add(Me.LowAlert)
        Me.LowPanel.Location = New System.Drawing.Point(858, 18)
        Me.LowPanel.Name = "LowPanel"
        Me.LowPanel.Size = New System.Drawing.Size(272, 172)
        Me.LowPanel.TabIndex = 1
        '
        'High_LBL
        '
        Me.High_LBL.AutoSize = True
        Me.High_LBL.Font = New System.Drawing.Font("Century Gothic", 36.0!)
        Me.High_LBL.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.High_LBL.Location = New System.Drawing.Point(113, 41)
        Me.High_LBL.Name = "High_LBL"
        Me.High_LBL.Size = New System.Drawing.Size(52, 58)
        Me.High_LBL.TabIndex = 4
        Me.High_LBL.Text = "0"
        Me.High_LBL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LowAlert
        '
        Me.LowAlert.AutoSize = True
        Me.LowAlert.Font = New System.Drawing.Font("Century Gothic", 11.0!)
        Me.LowAlert.Location = New System.Drawing.Point(13, 131)
        Me.LowAlert.Name = "LowAlert"
        Me.LowAlert.Size = New System.Drawing.Size(252, 20)
        Me.LowAlert.TabIndex = 2
        Me.LowAlert.Text = "Articles en etat critique Maximum"
        Me.LowAlert.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListOfArticlesInStock
        '
        Me.ListOfArticlesInStock.BackColor = System.Drawing.Color.White
        Me.ListOfArticlesInStock.Controls.Add(Me.UseSupply)
        Me.ListOfArticlesInStock.Controls.Add(Me.Stock_DGV)
        Me.ListOfArticlesInStock.Controls.Add(Me.Stock_LBL)
        Me.ListOfArticlesInStock.Location = New System.Drawing.Point(23, 276)
        Me.ListOfArticlesInStock.Name = "ListOfArticlesInStock"
        Me.ListOfArticlesInStock.Size = New System.Drawing.Size(1121, 476)
        Me.ListOfArticlesInStock.TabIndex = 0
        '
        'UseSupply
        '
        Me.UseSupply.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.UseSupply.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Edit_Row
        Me.UseSupply.ImageActive = Nothing
        Me.UseSupply.Location = New System.Drawing.Point(1014, 14)
        Me.UseSupply.Name = "UseSupply"
        Me.UseSupply.Size = New System.Drawing.Size(46, 47)
        Me.UseSupply.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.UseSupply.TabIndex = 13
        Me.UseSupply.TabStop = False
        Me.UseSupply.Zoom = 10
        '
        'Stock_DGV
        '
        Me.Stock_DGV.AllowUserToAddRows = False
        Me.Stock_DGV.AllowUserToDeleteRows = False
        Me.Stock_DGV.AllowUserToResizeColumns = False
        Me.Stock_DGV.AllowUserToResizeRows = False
        DataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle30.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle30.Font = New System.Drawing.Font("Century Gothic", 10.0!)
        DataGridViewCellStyle30.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle30.NullValue = "-"
        DataGridViewCellStyle30.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        DataGridViewCellStyle30.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.Stock_DGV.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle30
        Me.Stock_DGV.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Stock_DGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.Stock_DGV.BackgroundColor = System.Drawing.Color.White
        Me.Stock_DGV.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Stock_DGV.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.Stock_DGV.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle31.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        DataGridViewCellStyle31.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle31.ForeColor = System.Drawing.Color.FromArgb(CType(CType(37, Byte), Integer), CType(CType(46, Byte), Integer), CType(CType(59, Byte), Integer))
        DataGridViewCellStyle31.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        DataGridViewCellStyle31.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(37, Byte), Integer), CType(CType(46, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.Stock_DGV.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle31
        Me.Stock_DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle32.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle32.Font = New System.Drawing.Font("Century Gothic", 10.0!)
        DataGridViewCellStyle32.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle32.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        DataGridViewCellStyle32.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Stock_DGV.DefaultCellStyle = DataGridViewCellStyle32
        Me.Stock_DGV.DoubleBuffered = True
        Me.Stock_DGV.EnableHeadersVisualStyles = False
        Me.Stock_DGV.GridColor = System.Drawing.Color.White
        Me.Stock_DGV.HeaderBgColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Stock_DGV.HeaderForeColor = System.Drawing.Color.FromArgb(CType(CType(37, Byte), Integer), CType(CType(46, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.Stock_DGV.Location = New System.Drawing.Point(7, 67)
        Me.Stock_DGV.MultiSelect = False
        Me.Stock_DGV.Name = "Stock_DGV"
        Me.Stock_DGV.ReadOnly = True
        Me.Stock_DGV.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.Stock_DGV.RowHeadersVisible = False
        Me.Stock_DGV.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        DataGridViewCellStyle33.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle33.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle33.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        DataGridViewCellStyle33.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.Stock_DGV.RowsDefaultCellStyle = DataGridViewCellStyle33
        Me.Stock_DGV.RowTemplate.Height = 35
        Me.Stock_DGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Stock_DGV.ShowCellErrors = False
        Me.Stock_DGV.ShowCellToolTips = False
        Me.Stock_DGV.ShowEditingIcon = False
        Me.Stock_DGV.ShowRowErrors = False
        Me.Stock_DGV.Size = New System.Drawing.Size(1099, 396)
        Me.Stock_DGV.TabIndex = 9
        '
        'Stock_LBL
        '
        Me.Stock_LBL.AutoSize = True
        Me.Stock_LBL.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.Stock_LBL.Location = New System.Drawing.Point(464, 25)
        Me.Stock_LBL.Name = "Stock_LBL"
        Me.Stock_LBL.Size = New System.Drawing.Size(179, 21)
        Me.Stock_LBL.TabIndex = 11
        Me.Stock_LBL.Text = "Liste d'article en stock"
        '
        'PNL_Login
        '
        Me.PNL_Login.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.PNL_Login.Controls.Add(Me.BunifuCustomLabel10)
        Me.PNL_Login.Controls.Add(Me.PNL_Login_Authentification)
        Me.PNL_Login.Location = New System.Drawing.Point(1512, 123)
        Me.PNL_Login.Name = "PNL_Login"
        Me.PNL_Login.Size = New System.Drawing.Size(59, 406)
        Me.PNL_Login.TabIndex = 2
        Me.PNL_Login.Visible = False
        '
        'BunifuCustomLabel10
        '
        Me.BunifuCustomLabel10.AutoSize = True
        Me.BunifuCustomLabel10.Location = New System.Drawing.Point(7, 68)
        Me.BunifuCustomLabel10.Name = "BunifuCustomLabel10"
        Me.BunifuCustomLabel10.Size = New System.Drawing.Size(33, 13)
        Me.BunifuCustomLabel10.TabIndex = 4
        Me.BunifuCustomLabel10.Text = "Login"
        '
        'PNL_Login_Authentification
        '
        Me.PNL_Login_Authentification.BackColor = System.Drawing.Color.White
        Me.PNL_Login_Authentification.Controls.Add(Me.Password_TXB)
        Me.PNL_Login_Authentification.Controls.Add(Me.Authentificate_BTN)
        Me.PNL_Login_Authentification.Controls.Add(Me.Login_TXB)
        Me.PNL_Login_Authentification.Location = New System.Drawing.Point(60, 20)
        Me.PNL_Login_Authentification.Name = "PNL_Login_Authentification"
        Me.PNL_Login_Authentification.Size = New System.Drawing.Size(288, 305)
        Me.PNL_Login_Authentification.TabIndex = 3
        '
        'Password_TXB
        '
        Me.Password_TXB.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Password_TXB.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.Password_TXB.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Password_TXB.HintForeColor = System.Drawing.Color.Empty
        Me.Password_TXB.HintText = ""
        Me.Password_TXB.isPassword = False
        Me.Password_TXB.LineFocusedColor = System.Drawing.Color.Blue
        Me.Password_TXB.LineIdleColor = System.Drawing.Color.Gray
        Me.Password_TXB.LineMouseHoverColor = System.Drawing.Color.Blue
        Me.Password_TXB.LineThickness = 3
        Me.Password_TXB.Location = New System.Drawing.Point(26, 120)
        Me.Password_TXB.Margin = New System.Windows.Forms.Padding(4)
        Me.Password_TXB.Name = "Password_TXB"
        Me.Password_TXB.Size = New System.Drawing.Size(236, 33)
        Me.Password_TXB.TabIndex = 2
        Me.Password_TXB.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'Authentificate_BTN
        '
        Me.Authentificate_BTN.ActiveBorderThickness = 1
        Me.Authentificate_BTN.ActiveCornerRadius = 20
        Me.Authentificate_BTN.ActiveFillColor = System.Drawing.Color.SeaGreen
        Me.Authentificate_BTN.ActiveForecolor = System.Drawing.Color.White
        Me.Authentificate_BTN.ActiveLineColor = System.Drawing.Color.SeaGreen
        Me.Authentificate_BTN.BackColor = System.Drawing.Color.White
        Me.Authentificate_BTN.BackgroundImage = CType(resources.GetObject("Authentificate_BTN.BackgroundImage"), System.Drawing.Image)
        Me.Authentificate_BTN.ButtonText = "Login"
        Me.Authentificate_BTN.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Authentificate_BTN.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Authentificate_BTN.ForeColor = System.Drawing.Color.SeaGreen
        Me.Authentificate_BTN.IdleBorderThickness = 1
        Me.Authentificate_BTN.IdleCornerRadius = 20
        Me.Authentificate_BTN.IdleFillColor = System.Drawing.Color.White
        Me.Authentificate_BTN.IdleForecolor = System.Drawing.Color.SeaGreen
        Me.Authentificate_BTN.IdleLineColor = System.Drawing.Color.SeaGreen
        Me.Authentificate_BTN.Location = New System.Drawing.Point(66, 218)
        Me.Authentificate_BTN.Margin = New System.Windows.Forms.Padding(5)
        Me.Authentificate_BTN.Name = "Authentificate_BTN"
        Me.Authentificate_BTN.Size = New System.Drawing.Size(181, 41)
        Me.Authentificate_BTN.TabIndex = 0
        Me.Authentificate_BTN.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Login_TXB
        '
        Me.Login_TXB.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Login_TXB.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.Login_TXB.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Login_TXB.HintForeColor = System.Drawing.Color.Empty
        Me.Login_TXB.HintText = ""
        Me.Login_TXB.isPassword = False
        Me.Login_TXB.LineFocusedColor = System.Drawing.Color.Blue
        Me.Login_TXB.LineIdleColor = System.Drawing.Color.Gray
        Me.Login_TXB.LineMouseHoverColor = System.Drawing.Color.Blue
        Me.Login_TXB.LineThickness = 3
        Me.Login_TXB.Location = New System.Drawing.Point(26, 65)
        Me.Login_TXB.Margin = New System.Windows.Forms.Padding(4)
        Me.Login_TXB.Name = "Login_TXB"
        Me.Login_TXB.Size = New System.Drawing.Size(236, 33)
        Me.Login_TXB.TabIndex = 1
        Me.Login_TXB.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'PNL_Dashboard
        '
        Me.PNL_Dashboard.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.PNL_Dashboard.Controls.Add(Me.BunifuCustomLabel11)
        Me.PNL_Dashboard.Controls.Add(Me.Dashboard_First_PNL)
        Me.PNL_Dashboard.Location = New System.Drawing.Point(203, 103)
        Me.PNL_Dashboard.Name = "PNL_Dashboard"
        Me.PNL_Dashboard.Size = New System.Drawing.Size(1112, 699)
        Me.PNL_Dashboard.TabIndex = 3
        Me.PNL_Dashboard.Visible = False
        '
        'BunifuCustomLabel11
        '
        Me.BunifuCustomLabel11.AutoSize = True
        Me.BunifuCustomLabel11.Location = New System.Drawing.Point(4, 50)
        Me.BunifuCustomLabel11.Name = "BunifuCustomLabel11"
        Me.BunifuCustomLabel11.Size = New System.Drawing.Size(59, 13)
        Me.BunifuCustomLabel11.TabIndex = 2
        Me.BunifuCustomLabel11.Text = "Dashboard"
        '
        'Dashboard_First_PNL
        '
        Me.Dashboard_First_PNL.BackColor = System.Drawing.Color.White
        Me.Dashboard_First_PNL.Controls.Add(Me.BunifuCustomLabel5)
        Me.Dashboard_First_PNL.Location = New System.Drawing.Point(206, 151)
        Me.Dashboard_First_PNL.Name = "Dashboard_First_PNL"
        Me.Dashboard_First_PNL.Size = New System.Drawing.Size(321, 176)
        Me.Dashboard_First_PNL.TabIndex = 1
        '
        'BunifuCustomLabel5
        '
        Me.BunifuCustomLabel5.AutoSize = True
        Me.BunifuCustomLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel5.Location = New System.Drawing.Point(10, 65)
        Me.BunifuCustomLabel5.Name = "BunifuCustomLabel5"
        Me.BunifuCustomLabel5.Size = New System.Drawing.Size(311, 24)
        Me.BunifuCustomLabel5.TabIndex = 0
        Me.BunifuCustomLabel5.Text = "Dashboard Panel (cliquez sur stock)"
        '
        'PNL_Stock_Articles
        '
        Me.PNL_Stock_Articles.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.PNL_Stock_Articles.Controls.Add(Me.SearchArticleInStock)
        Me.PNL_Stock_Articles.Controls.Add(Me.ListOfStock)
        Me.PNL_Stock_Articles.Location = New System.Drawing.Point(206, 106)
        Me.PNL_Stock_Articles.Name = "PNL_Stock_Articles"
        Me.PNL_Stock_Articles.Size = New System.Drawing.Size(1185, 764)
        Me.PNL_Stock_Articles.TabIndex = 5
        Me.PNL_Stock_Articles.Visible = False
        '
        'SearchArticleInStock
        '
        Me.SearchArticleInStock.BackColor = System.Drawing.Color.White
        Me.SearchArticleInStock.Controls.Add(Me.Stock_Articles_Articles_TXB)
        Me.SearchArticleInStock.Controls.Add(Me.BunifuCustomLabel16)
        Me.SearchArticleInStock.Location = New System.Drawing.Point(33, 17)
        Me.SearchArticleInStock.Name = "SearchArticleInStock"
        Me.SearchArticleInStock.Size = New System.Drawing.Size(383, 173)
        Me.SearchArticleInStock.TabIndex = 2
        '
        'Stock_Articles_Articles_TXB
        '
        Me.Stock_Articles_Articles_TXB.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Stock_Articles_Articles_TXB.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Stock_Articles_Articles_TXB.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Stock_Articles_Articles_TXB.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Stock_Articles_Articles_TXB.BorderThickness = 3
        Me.Stock_Articles_Articles_TXB.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Stock_Articles_Articles_TXB.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.Stock_Articles_Articles_TXB.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.Stock_Articles_Articles_TXB.isPassword = False
        Me.Stock_Articles_Articles_TXB.Location = New System.Drawing.Point(60, 87)
        Me.Stock_Articles_Articles_TXB.Margin = New System.Windows.Forms.Padding(5)
        Me.Stock_Articles_Articles_TXB.Name = "Stock_Articles_Articles_TXB"
        Me.Stock_Articles_Articles_TXB.Size = New System.Drawing.Size(258, 40)
        Me.Stock_Articles_Articles_TXB.TabIndex = 17
        Me.Stock_Articles_Articles_TXB.Text = "Nom D'Article"
        Me.Stock_Articles_Articles_TXB.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'BunifuCustomLabel16
        '
        Me.BunifuCustomLabel16.AutoSize = True
        Me.BunifuCustomLabel16.Location = New System.Drawing.Point(134, 41)
        Me.BunifuCustomLabel16.Name = "BunifuCustomLabel16"
        Me.BunifuCustomLabel16.Size = New System.Drawing.Size(112, 13)
        Me.BunifuCustomLabel16.TabIndex = 0
        Me.BunifuCustomLabel16.Text = "criteres de Recherche"
        '
        'ListOfStock
        '
        Me.ListOfStock.BackColor = System.Drawing.Color.White
        Me.ListOfStock.Controls.Add(Me.DeleteArticle)
        Me.ListOfStock.Controls.Add(Me.Modify_Article_BTN)
        Me.ListOfStock.Controls.Add(Me.Stock_AddArticle)
        Me.ListOfStock.Controls.Add(Me.ListOfArticles_DGV)
        Me.ListOfStock.Controls.Add(Me.BunifuCustomLabel18)
        Me.ListOfStock.Location = New System.Drawing.Point(23, 276)
        Me.ListOfStock.Name = "ListOfStock"
        Me.ListOfStock.Size = New System.Drawing.Size(1121, 476)
        Me.ListOfStock.TabIndex = 0
        '
        'DeleteArticle
        '
        Me.DeleteArticle.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.DeleteArticle.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Delete_Row
        Me.DeleteArticle.ImageActive = Nothing
        Me.DeleteArticle.Location = New System.Drawing.Point(1037, 14)
        Me.DeleteArticle.Name = "DeleteArticle"
        Me.DeleteArticle.Size = New System.Drawing.Size(46, 47)
        Me.DeleteArticle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.DeleteArticle.TabIndex = 14
        Me.DeleteArticle.TabStop = False
        Me.DeleteArticle.Zoom = 10
        '
        'Modify_Article_BTN
        '
        Me.Modify_Article_BTN.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.Modify_Article_BTN.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Edit_Row
        Me.Modify_Article_BTN.ImageActive = Nothing
        Me.Modify_Article_BTN.Location = New System.Drawing.Point(985, 14)
        Me.Modify_Article_BTN.Name = "Modify_Article_BTN"
        Me.Modify_Article_BTN.Size = New System.Drawing.Size(46, 47)
        Me.Modify_Article_BTN.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Modify_Article_BTN.TabIndex = 13
        Me.Modify_Article_BTN.TabStop = False
        Me.Modify_Article_BTN.Zoom = 10
        '
        'Stock_AddArticle
        '
        Me.Stock_AddArticle.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.Stock_AddArticle.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Add_Row
        Me.Stock_AddArticle.ImageActive = Nothing
        Me.Stock_AddArticle.Location = New System.Drawing.Point(933, 14)
        Me.Stock_AddArticle.Name = "Stock_AddArticle"
        Me.Stock_AddArticle.Size = New System.Drawing.Size(46, 47)
        Me.Stock_AddArticle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Stock_AddArticle.TabIndex = 12
        Me.Stock_AddArticle.TabStop = False
        Me.Stock_AddArticle.Zoom = 10
        '
        'ListOfArticles_DGV
        '
        Me.ListOfArticles_DGV.AllowUserToAddRows = False
        Me.ListOfArticles_DGV.AllowUserToDeleteRows = False
        Me.ListOfArticles_DGV.AllowUserToResizeColumns = False
        Me.ListOfArticles_DGV.AllowUserToResizeRows = False
        DataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle34.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle34.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle34.NullValue = "-"
        DataGridViewCellStyle34.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(230, Byte), Integer))
        DataGridViewCellStyle34.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.ListOfArticles_DGV.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle34
        Me.ListOfArticles_DGV.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ListOfArticles_DGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.ListOfArticles_DGV.BackgroundColor = System.Drawing.Color.White
        Me.ListOfArticles_DGV.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.ListOfArticles_DGV.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.ListOfArticles_DGV.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle35.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        DataGridViewCellStyle35.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle35.ForeColor = System.Drawing.Color.FromArgb(CType(CType(37, Byte), Integer), CType(CType(46, Byte), Integer), CType(CType(59, Byte), Integer))
        DataGridViewCellStyle35.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        DataGridViewCellStyle35.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(37, Byte), Integer), CType(CType(46, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.ListOfArticles_DGV.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle35
        Me.ListOfArticles_DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle36.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle36.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle36.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle36.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        DataGridViewCellStyle36.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle36.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ListOfArticles_DGV.DefaultCellStyle = DataGridViewCellStyle36
        Me.ListOfArticles_DGV.DoubleBuffered = True
        Me.ListOfArticles_DGV.EnableHeadersVisualStyles = False
        Me.ListOfArticles_DGV.GridColor = System.Drawing.Color.White
        Me.ListOfArticles_DGV.HeaderBgColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.ListOfArticles_DGV.HeaderForeColor = System.Drawing.Color.FromArgb(CType(CType(37, Byte), Integer), CType(CType(46, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.ListOfArticles_DGV.Location = New System.Drawing.Point(7, 67)
        Me.ListOfArticles_DGV.MultiSelect = False
        Me.ListOfArticles_DGV.Name = "ListOfArticles_DGV"
        Me.ListOfArticles_DGV.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.ListOfArticles_DGV.RowHeadersVisible = False
        Me.ListOfArticles_DGV.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        Me.ListOfArticles_DGV.RowTemplate.Height = 35
        Me.ListOfArticles_DGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.ListOfArticles_DGV.ShowCellErrors = False
        Me.ListOfArticles_DGV.ShowCellToolTips = False
        Me.ListOfArticles_DGV.ShowEditingIcon = False
        Me.ListOfArticles_DGV.ShowRowErrors = False
        Me.ListOfArticles_DGV.Size = New System.Drawing.Size(1099, 396)
        Me.ListOfArticles_DGV.TabIndex = 9
        '
        'BunifuCustomLabel18
        '
        Me.BunifuCustomLabel18.AutoSize = True
        Me.BunifuCustomLabel18.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel18.Location = New System.Drawing.Point(491, 20)
        Me.BunifuCustomLabel18.Name = "BunifuCustomLabel18"
        Me.BunifuCustomLabel18.Size = New System.Drawing.Size(116, 21)
        Me.BunifuCustomLabel18.TabIndex = 11
        Me.BunifuCustomLabel18.Text = "Liste d'articles"
        '
        'PNL_Stock_Suppliers
        '
        Me.PNL_Stock_Suppliers.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.PNL_Stock_Suppliers.Controls.Add(Me.Stock_Suppliers_SearchArea)
        Me.PNL_Stock_Suppliers.Controls.Add(Me.ListOfSuppliers)
        Me.PNL_Stock_Suppliers.Location = New System.Drawing.Point(206, 106)
        Me.PNL_Stock_Suppliers.Name = "PNL_Stock_Suppliers"
        Me.PNL_Stock_Suppliers.Size = New System.Drawing.Size(1185, 764)
        Me.PNL_Stock_Suppliers.TabIndex = 6
        Me.PNL_Stock_Suppliers.Visible = False
        '
        'Stock_Suppliers_SearchArea
        '
        Me.Stock_Suppliers_SearchArea.BackColor = System.Drawing.Color.White
        Me.Stock_Suppliers_SearchArea.Controls.Add(Me.Suppliercity)
        Me.Stock_Suppliers_SearchArea.Controls.Add(Me.SupplierPhone)
        Me.Stock_Suppliers_SearchArea.Controls.Add(Me.SupplierAddresse)
        Me.Stock_Suppliers_SearchArea.Controls.Add(Me.SupplierLastName)
        Me.Stock_Suppliers_SearchArea.Controls.Add(Me.SupplierName)
        Me.Stock_Suppliers_SearchArea.Controls.Add(Me.BunifuCustomLabel28)
        Me.Stock_Suppliers_SearchArea.Location = New System.Drawing.Point(38, 17)
        Me.Stock_Suppliers_SearchArea.Name = "Stock_Suppliers_SearchArea"
        Me.Stock_Suppliers_SearchArea.Size = New System.Drawing.Size(383, 250)
        Me.Stock_Suppliers_SearchArea.TabIndex = 2
        '
        'Suppliercity
        '
        Me.Suppliercity.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Suppliercity.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Suppliercity.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Suppliercity.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Suppliercity.BorderThickness = 3
        Me.Suppliercity.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Suppliercity.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.Suppliercity.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.Suppliercity.isPassword = False
        Me.Suppliercity.Location = New System.Drawing.Point(58, 166)
        Me.Suppliercity.Margin = New System.Windows.Forms.Padding(5)
        Me.Suppliercity.Name = "Suppliercity"
        Me.Suppliercity.Size = New System.Drawing.Size(258, 32)
        Me.Suppliercity.TabIndex = 21
        Me.Suppliercity.Text = "Ville du Founisseur"
        Me.Suppliercity.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'SupplierPhone
        '
        Me.SupplierPhone.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierPhone.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierPhone.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierPhone.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierPhone.BorderThickness = 3
        Me.SupplierPhone.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.SupplierPhone.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.SupplierPhone.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.SupplierPhone.isPassword = False
        Me.SupplierPhone.Location = New System.Drawing.Point(58, 208)
        Me.SupplierPhone.Margin = New System.Windows.Forms.Padding(5)
        Me.SupplierPhone.Name = "SupplierPhone"
        Me.SupplierPhone.Size = New System.Drawing.Size(258, 32)
        Me.SupplierPhone.TabIndex = 20
        Me.SupplierPhone.Text = "Telephone du Fournisseur"
        Me.SupplierPhone.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'SupplierAddresse
        '
        Me.SupplierAddresse.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierAddresse.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierAddresse.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierAddresse.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierAddresse.BorderThickness = 3
        Me.SupplierAddresse.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.SupplierAddresse.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.SupplierAddresse.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.SupplierAddresse.isPassword = False
        Me.SupplierAddresse.Location = New System.Drawing.Point(60, 124)
        Me.SupplierAddresse.Margin = New System.Windows.Forms.Padding(5)
        Me.SupplierAddresse.Name = "SupplierAddresse"
        Me.SupplierAddresse.Size = New System.Drawing.Size(258, 32)
        Me.SupplierAddresse.TabIndex = 19
        Me.SupplierAddresse.Text = "Adresse du Founisseur"
        Me.SupplierAddresse.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'SupplierLastName
        '
        Me.SupplierLastName.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierLastName.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierLastName.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierLastName.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierLastName.BorderThickness = 3
        Me.SupplierLastName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.SupplierLastName.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.SupplierLastName.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.SupplierLastName.isPassword = False
        Me.SupplierLastName.Location = New System.Drawing.Point(60, 82)
        Me.SupplierLastName.Margin = New System.Windows.Forms.Padding(5)
        Me.SupplierLastName.Name = "SupplierLastName"
        Me.SupplierLastName.Size = New System.Drawing.Size(258, 32)
        Me.SupplierLastName.TabIndex = 18
        Me.SupplierLastName.Text = "Nom du Fournisseur"
        Me.SupplierLastName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'SupplierName
        '
        Me.SupplierName.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierName.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierName.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierName.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SupplierName.BorderThickness = 3
        Me.SupplierName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.SupplierName.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.SupplierName.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.SupplierName.isPassword = False
        Me.SupplierName.Location = New System.Drawing.Point(60, 41)
        Me.SupplierName.Margin = New System.Windows.Forms.Padding(5)
        Me.SupplierName.Name = "SupplierName"
        Me.SupplierName.Size = New System.Drawing.Size(258, 32)
        Me.SupplierName.TabIndex = 17
        Me.SupplierName.Text = "Prenom du Fournisseur"
        Me.SupplierName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'BunifuCustomLabel28
        '
        Me.BunifuCustomLabel28.AutoSize = True
        Me.BunifuCustomLabel28.Font = New System.Drawing.Font("Century Gothic", 10.0!)
        Me.BunifuCustomLabel28.Location = New System.Drawing.Point(115, 12)
        Me.BunifuCustomLabel28.Name = "BunifuCustomLabel28"
        Me.BunifuCustomLabel28.Size = New System.Drawing.Size(160, 19)
        Me.BunifuCustomLabel28.TabIndex = 0
        Me.BunifuCustomLabel28.Text = "criteres de Recherche"
        '
        'ListOfSuppliers
        '
        Me.ListOfSuppliers.BackColor = System.Drawing.Color.White
        Me.ListOfSuppliers.Controls.Add(Me.Suppliers_DeleteSupplier)
        Me.ListOfSuppliers.Controls.Add(Me.Stock_ModifySupplier_BTN)
        Me.ListOfSuppliers.Controls.Add(Me.Stock_AddSupplier_BTN)
        Me.ListOfSuppliers.Controls.Add(Me.SuppliersList_DGV)
        Me.ListOfSuppliers.Controls.Add(Me.BunifuCustomLabel30)
        Me.ListOfSuppliers.Location = New System.Drawing.Point(23, 276)
        Me.ListOfSuppliers.Name = "ListOfSuppliers"
        Me.ListOfSuppliers.Size = New System.Drawing.Size(1121, 476)
        Me.ListOfSuppliers.TabIndex = 0
        '
        'Suppliers_DeleteSupplier
        '
        Me.Suppliers_DeleteSupplier.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.Suppliers_DeleteSupplier.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Delete_Row
        Me.Suppliers_DeleteSupplier.ImageActive = Nothing
        Me.Suppliers_DeleteSupplier.Location = New System.Drawing.Point(1031, 14)
        Me.Suppliers_DeleteSupplier.Name = "Suppliers_DeleteSupplier"
        Me.Suppliers_DeleteSupplier.Size = New System.Drawing.Size(46, 47)
        Me.Suppliers_DeleteSupplier.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Suppliers_DeleteSupplier.TabIndex = 14
        Me.Suppliers_DeleteSupplier.TabStop = False
        Me.Suppliers_DeleteSupplier.Zoom = 10
        '
        'Stock_ModifySupplier_BTN
        '
        Me.Stock_ModifySupplier_BTN.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.Stock_ModifySupplier_BTN.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Edit_Row
        Me.Stock_ModifySupplier_BTN.ImageActive = Nothing
        Me.Stock_ModifySupplier_BTN.Location = New System.Drawing.Point(979, 14)
        Me.Stock_ModifySupplier_BTN.Name = "Stock_ModifySupplier_BTN"
        Me.Stock_ModifySupplier_BTN.Size = New System.Drawing.Size(46, 47)
        Me.Stock_ModifySupplier_BTN.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Stock_ModifySupplier_BTN.TabIndex = 13
        Me.Stock_ModifySupplier_BTN.TabStop = False
        Me.Stock_ModifySupplier_BTN.Zoom = 10
        '
        'Stock_AddSupplier_BTN
        '
        Me.Stock_AddSupplier_BTN.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.Stock_AddSupplier_BTN.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Add_Row
        Me.Stock_AddSupplier_BTN.ImageActive = Nothing
        Me.Stock_AddSupplier_BTN.Location = New System.Drawing.Point(927, 14)
        Me.Stock_AddSupplier_BTN.Name = "Stock_AddSupplier_BTN"
        Me.Stock_AddSupplier_BTN.Size = New System.Drawing.Size(46, 47)
        Me.Stock_AddSupplier_BTN.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Stock_AddSupplier_BTN.TabIndex = 12
        Me.Stock_AddSupplier_BTN.TabStop = False
        Me.Stock_AddSupplier_BTN.Zoom = 10
        '
        'SuppliersList_DGV
        '
        Me.SuppliersList_DGV.AllowUserToAddRows = False
        Me.SuppliersList_DGV.AllowUserToDeleteRows = False
        Me.SuppliersList_DGV.AllowUserToResizeColumns = False
        Me.SuppliersList_DGV.AllowUserToResizeRows = False
        DataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle37.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle37.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle37.NullValue = "-"
        DataGridViewCellStyle37.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(230, Byte), Integer))
        DataGridViewCellStyle37.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.SuppliersList_DGV.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle37
        Me.SuppliersList_DGV.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SuppliersList_DGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.SuppliersList_DGV.BackgroundColor = System.Drawing.Color.White
        Me.SuppliersList_DGV.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.SuppliersList_DGV.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.SuppliersList_DGV.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle38.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        DataGridViewCellStyle38.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle38.ForeColor = System.Drawing.Color.FromArgb(CType(CType(37, Byte), Integer), CType(CType(46, Byte), Integer), CType(CType(59, Byte), Integer))
        DataGridViewCellStyle38.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        DataGridViewCellStyle38.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(37, Byte), Integer), CType(CType(46, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.SuppliersList_DGV.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle38
        Me.SuppliersList_DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle39.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle39.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle39.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle39.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        DataGridViewCellStyle39.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle39.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.SuppliersList_DGV.DefaultCellStyle = DataGridViewCellStyle39
        Me.SuppliersList_DGV.DoubleBuffered = True
        Me.SuppliersList_DGV.EnableHeadersVisualStyles = False
        Me.SuppliersList_DGV.GridColor = System.Drawing.Color.White
        Me.SuppliersList_DGV.HeaderBgColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.SuppliersList_DGV.HeaderForeColor = System.Drawing.Color.FromArgb(CType(CType(37, Byte), Integer), CType(CType(46, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.SuppliersList_DGV.Location = New System.Drawing.Point(7, 67)
        Me.SuppliersList_DGV.MultiSelect = False
        Me.SuppliersList_DGV.Name = "SuppliersList_DGV"
        Me.SuppliersList_DGV.ReadOnly = True
        Me.SuppliersList_DGV.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.SuppliersList_DGV.RowHeadersVisible = False
        Me.SuppliersList_DGV.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        Me.SuppliersList_DGV.RowTemplate.Height = 35
        Me.SuppliersList_DGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.SuppliersList_DGV.ShowCellErrors = False
        Me.SuppliersList_DGV.ShowCellToolTips = False
        Me.SuppliersList_DGV.ShowEditingIcon = False
        Me.SuppliersList_DGV.ShowRowErrors = False
        Me.SuppliersList_DGV.Size = New System.Drawing.Size(1099, 396)
        Me.SuppliersList_DGV.TabIndex = 9
        '
        'BunifuCustomLabel30
        '
        Me.BunifuCustomLabel30.AutoSize = True
        Me.BunifuCustomLabel30.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.BunifuCustomLabel30.Location = New System.Drawing.Point(491, 20)
        Me.BunifuCustomLabel30.Name = "BunifuCustomLabel30"
        Me.BunifuCustomLabel30.Size = New System.Drawing.Size(161, 21)
        Me.BunifuCustomLabel30.TabIndex = 11
        Me.BunifuCustomLabel30.Text = "Liste de Fournisseurs"
        '
        'PNL_Ordonnance
        '
        Me.PNL_Ordonnance.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.PNL_Ordonnance.Controls.Add(Me.Panel1)
        Me.PNL_Ordonnance.Controls.Add(Me.ListeMedicament)
        Me.PNL_Ordonnance.Controls.Add(Me.Panel3)
        Me.PNL_Ordonnance.Controls.Add(Me.MedListPanel)
        Me.PNL_Ordonnance.Location = New System.Drawing.Point(201, 101)
        Me.PNL_Ordonnance.Name = "PNL_Ordonnance"
        Me.PNL_Ordonnance.Size = New System.Drawing.Size(1185, 764)
        Me.PNL_Ordonnance.TabIndex = 7
        Me.PNL_Ordonnance.Visible = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.BunifuCustomLabel3)
        Me.Panel1.Location = New System.Drawing.Point(53, 619)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1142, 100)
        Me.Panel1.TabIndex = 3
        '
        'BunifuCustomLabel3
        '
        Me.BunifuCustomLabel3.AutoSize = True
        Me.BunifuCustomLabel3.Location = New System.Drawing.Point(553, 40)
        Me.BunifuCustomLabel3.Name = "BunifuCustomLabel3"
        Me.BunifuCustomLabel3.Size = New System.Drawing.Size(59, 13)
        Me.BunifuCustomLabel3.TabIndex = 0
        Me.BunifuCustomLabel3.Text = "Advertising"
        '
        'ListeMedicament
        '
        Me.ListeMedicament.BackColor = System.Drawing.Color.White
        Me.ListeMedicament.Controls.Add(Me.AjouterMed)
        Me.ListeMedicament.Controls.Add(Me.TypeLabel)
        Me.ListeMedicament.Controls.Add(Me.ListeType_CMB)
        Me.ListeMedicament.Controls.Add(Me.ListePosologie_CMB)
        Me.ListeMedicament.Controls.Add(Me.ClearBTN)
        Me.ListeMedicament.Controls.Add(Me.PosologieLabel)
        Me.ListeMedicament.Controls.Add(Me.MedicamentLabel)
        Me.ListeMedicament.Controls.Add(Me.ListeMedicament_CMB)
        Me.ListeMedicament.Location = New System.Drawing.Point(33, 17)
        Me.ListeMedicament.Name = "ListeMedicament"
        Me.ListeMedicament.Size = New System.Drawing.Size(276, 248)
        Me.ListeMedicament.TabIndex = 2
        '
        'AjouterMed
        '
        Me.AjouterMed.ActiveBorderThickness = 1
        Me.AjouterMed.ActiveCornerRadius = 20
        Me.AjouterMed.ActiveFillColor = System.Drawing.Color.SeaGreen
        Me.AjouterMed.ActiveForecolor = System.Drawing.Color.White
        Me.AjouterMed.ActiveLineColor = System.Drawing.Color.SeaGreen
        Me.AjouterMed.BackColor = System.Drawing.Color.White
        Me.AjouterMed.BackgroundImage = CType(resources.GetObject("AjouterMed.BackgroundImage"), System.Drawing.Image)
        Me.AjouterMed.ButtonText = "Ajouter"
        Me.AjouterMed.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AjouterMed.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AjouterMed.ForeColor = System.Drawing.Color.SeaGreen
        Me.AjouterMed.IdleBorderThickness = 1
        Me.AjouterMed.IdleCornerRadius = 20
        Me.AjouterMed.IdleFillColor = System.Drawing.Color.White
        Me.AjouterMed.IdleForecolor = System.Drawing.Color.SeaGreen
        Me.AjouterMed.IdleLineColor = System.Drawing.Color.SeaGreen
        Me.AjouterMed.Location = New System.Drawing.Point(74, 197)
        Me.AjouterMed.Margin = New System.Windows.Forms.Padding(5)
        Me.AjouterMed.Name = "AjouterMed"
        Me.AjouterMed.Size = New System.Drawing.Size(136, 42)
        Me.AjouterMed.TabIndex = 7
        Me.AjouterMed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TypeLabel
        '
        Me.TypeLabel.AutoSize = True
        Me.TypeLabel.Location = New System.Drawing.Point(24, 16)
        Me.TypeLabel.Name = "TypeLabel"
        Me.TypeLabel.Size = New System.Drawing.Size(37, 13)
        Me.TypeLabel.TabIndex = 6
        Me.TypeLabel.Text = "Type :"
        '
        'ListeType_CMB
        '
        Me.ListeType_CMB.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.ListeType_CMB.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListeType_CMB.FormattingEnabled = True
        Me.ListeType_CMB.Location = New System.Drawing.Point(25, 38)
        Me.ListeType_CMB.Name = "ListeType_CMB"
        Me.ListeType_CMB.Size = New System.Drawing.Size(231, 29)
        Me.ListeType_CMB.TabIndex = 5
        '
        'ListePosologie_CMB
        '
        Me.ListePosologie_CMB.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.ListePosologie_CMB.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListePosologie_CMB.FormattingEnabled = True
        Me.ListePosologie_CMB.Location = New System.Drawing.Point(23, 160)
        Me.ListePosologie_CMB.Name = "ListePosologie_CMB"
        Me.ListePosologie_CMB.Size = New System.Drawing.Size(231, 29)
        Me.ListePosologie_CMB.TabIndex = 4
        '
        'ClearBTN
        '
        Me.ClearBTN.BackColor = System.Drawing.Color.SeaGreen
        Me.ClearBTN.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Refresh
        Me.ClearBTN.ImageActive = Nothing
        Me.ClearBTN.Location = New System.Drawing.Point(232, 5)
        Me.ClearBTN.Name = "ClearBTN"
        Me.ClearBTN.Size = New System.Drawing.Size(24, 25)
        Me.ClearBTN.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.ClearBTN.TabIndex = 3
        Me.ClearBTN.TabStop = False
        Me.ClearBTN.Zoom = 10
        '
        'PosologieLabel
        '
        Me.PosologieLabel.AutoSize = True
        Me.PosologieLabel.Location = New System.Drawing.Point(28, 139)
        Me.PosologieLabel.Name = "PosologieLabel"
        Me.PosologieLabel.Size = New System.Drawing.Size(59, 13)
        Me.PosologieLabel.TabIndex = 2
        Me.PosologieLabel.Text = "Posologie :"
        '
        'MedicamentLabel
        '
        Me.MedicamentLabel.AutoSize = True
        Me.MedicamentLabel.Location = New System.Drawing.Point(22, 78)
        Me.MedicamentLabel.Name = "MedicamentLabel"
        Me.MedicamentLabel.Size = New System.Drawing.Size(71, 13)
        Me.MedicamentLabel.TabIndex = 1
        Me.MedicamentLabel.Text = "Medicament :"
        '
        'ListeMedicament_CMB
        '
        Me.ListeMedicament_CMB.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.ListeMedicament_CMB.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListeMedicament_CMB.FormattingEnabled = True
        Me.ListeMedicament_CMB.Location = New System.Drawing.Point(23, 100)
        Me.ListeMedicament_CMB.Name = "ListeMedicament_CMB"
        Me.ListeMedicament_CMB.Size = New System.Drawing.Size(231, 29)
        Me.ListeMedicament_CMB.TabIndex = 0
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.White
        Me.Panel3.Controls.Add(Me.Ordonnance1)
        Me.Panel3.Controls.Add(Me.Ordonnance2)
        Me.Panel3.Controls.Add(Me.Ordonnance3)
        Me.Panel3.Controls.Add(Me.Ordonnance4)
        Me.Panel3.Controls.Add(Me.Ordonnance5)
        Me.Panel3.Controls.Add(Me.Save1)
        Me.Panel3.Controls.Add(Me.Save5)
        Me.Panel3.Controls.Add(Me.Save2)
        Me.Panel3.Controls.Add(Me.Save4)
        Me.Panel3.Controls.Add(Me.Save3)
        Me.Panel3.Location = New System.Drawing.Point(315, 431)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(860, 140)
        Me.Panel3.TabIndex = 1
        '
        'Ordonnance1
        '
        Me.Ordonnance1.BackColor = System.Drawing.Color.SeaGreen
        Me.Ordonnance1.Image = CType(resources.GetObject("Ordonnance1.Image"), System.Drawing.Image)
        Me.Ordonnance1.ImageActive = Nothing
        Me.Ordonnance1.Location = New System.Drawing.Point(58, 33)
        Me.Ordonnance1.Name = "Ordonnance1"
        Me.Ordonnance1.Size = New System.Drawing.Size(71, 71)
        Me.Ordonnance1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Ordonnance1.TabIndex = 21
        Me.Ordonnance1.TabStop = False
        Me.Ordonnance1.Tag = "1"
        Me.Ordonnance1.Zoom = 10
        '
        'Ordonnance2
        '
        Me.Ordonnance2.BackColor = System.Drawing.Color.SeaGreen
        Me.Ordonnance2.Image = CType(resources.GetObject("Ordonnance2.Image"), System.Drawing.Image)
        Me.Ordonnance2.ImageActive = Nothing
        Me.Ordonnance2.Location = New System.Drawing.Point(221, 33)
        Me.Ordonnance2.Name = "Ordonnance2"
        Me.Ordonnance2.Size = New System.Drawing.Size(71, 71)
        Me.Ordonnance2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Ordonnance2.TabIndex = 22
        Me.Ordonnance2.TabStop = False
        Me.Ordonnance2.Tag = "2"
        Me.Ordonnance2.Zoom = 10
        '
        'Ordonnance3
        '
        Me.Ordonnance3.BackColor = System.Drawing.Color.SeaGreen
        Me.Ordonnance3.Image = CType(resources.GetObject("Ordonnance3.Image"), System.Drawing.Image)
        Me.Ordonnance3.ImageActive = Nothing
        Me.Ordonnance3.Location = New System.Drawing.Point(377, 33)
        Me.Ordonnance3.Name = "Ordonnance3"
        Me.Ordonnance3.Size = New System.Drawing.Size(71, 71)
        Me.Ordonnance3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Ordonnance3.TabIndex = 23
        Me.Ordonnance3.TabStop = False
        Me.Ordonnance3.Tag = "3"
        Me.Ordonnance3.Zoom = 10
        '
        'Ordonnance4
        '
        Me.Ordonnance4.BackColor = System.Drawing.Color.SeaGreen
        Me.Ordonnance4.Image = CType(resources.GetObject("Ordonnance4.Image"), System.Drawing.Image)
        Me.Ordonnance4.ImageActive = Nothing
        Me.Ordonnance4.Location = New System.Drawing.Point(537, 33)
        Me.Ordonnance4.Name = "Ordonnance4"
        Me.Ordonnance4.Size = New System.Drawing.Size(71, 71)
        Me.Ordonnance4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Ordonnance4.TabIndex = 24
        Me.Ordonnance4.TabStop = False
        Me.Ordonnance4.Tag = "4"
        Me.Ordonnance4.Zoom = 10
        '
        'Ordonnance5
        '
        Me.Ordonnance5.BackColor = System.Drawing.Color.SeaGreen
        Me.Ordonnance5.Image = CType(resources.GetObject("Ordonnance5.Image"), System.Drawing.Image)
        Me.Ordonnance5.ImageActive = Nothing
        Me.Ordonnance5.Location = New System.Drawing.Point(702, 33)
        Me.Ordonnance5.Name = "Ordonnance5"
        Me.Ordonnance5.Size = New System.Drawing.Size(71, 71)
        Me.Ordonnance5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Ordonnance5.TabIndex = 25
        Me.Ordonnance5.TabStop = False
        Me.Ordonnance5.Tag = "5"
        Me.Ordonnance5.Zoom = 10
        '
        'Save1
        '
        Me.Save1.BackColor = System.Drawing.Color.SeaGreen
        Me.Save1.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Save
        Me.Save1.ImageActive = Nothing
        Me.Save1.Location = New System.Drawing.Point(135, 33)
        Me.Save1.Name = "Save1"
        Me.Save1.Size = New System.Drawing.Size(25, 25)
        Me.Save1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Save1.TabIndex = 26
        Me.Save1.TabStop = False
        Me.Save1.Tag = "1"
        Me.Save1.Zoom = 10
        '
        'Save5
        '
        Me.Save5.BackColor = System.Drawing.Color.SeaGreen
        Me.Save5.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Save
        Me.Save5.ImageActive = Nothing
        Me.Save5.Location = New System.Drawing.Point(779, 33)
        Me.Save5.Name = "Save5"
        Me.Save5.Size = New System.Drawing.Size(25, 25)
        Me.Save5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Save5.TabIndex = 34
        Me.Save5.TabStop = False
        Me.Save5.Tag = "5"
        Me.Save5.Zoom = 10
        '
        'Save2
        '
        Me.Save2.BackColor = System.Drawing.Color.SeaGreen
        Me.Save2.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Save
        Me.Save2.ImageActive = Nothing
        Me.Save2.Location = New System.Drawing.Point(298, 33)
        Me.Save2.Name = "Save2"
        Me.Save2.Size = New System.Drawing.Size(25, 25)
        Me.Save2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Save2.TabIndex = 28
        Me.Save2.TabStop = False
        Me.Save2.Tag = "2"
        Me.Save2.Zoom = 10
        '
        'Save4
        '
        Me.Save4.BackColor = System.Drawing.Color.SeaGreen
        Me.Save4.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Save
        Me.Save4.ImageActive = Nothing
        Me.Save4.Location = New System.Drawing.Point(614, 33)
        Me.Save4.Name = "Save4"
        Me.Save4.Size = New System.Drawing.Size(25, 25)
        Me.Save4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Save4.TabIndex = 32
        Me.Save4.TabStop = False
        Me.Save4.Tag = "4"
        Me.Save4.Zoom = 10
        '
        'Save3
        '
        Me.Save3.BackColor = System.Drawing.Color.SeaGreen
        Me.Save3.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Save
        Me.Save3.ImageActive = Nothing
        Me.Save3.Location = New System.Drawing.Point(454, 33)
        Me.Save3.Name = "Save3"
        Me.Save3.Size = New System.Drawing.Size(25, 25)
        Me.Save3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Save3.TabIndex = 30
        Me.Save3.TabStop = False
        Me.Save3.Tag = "3"
        Me.Save3.Zoom = 10
        '
        'MedListPanel
        '
        Me.MedListPanel.BackColor = System.Drawing.Color.White
        Me.MedListPanel.Controls.Add(Me.Posologie5)
        Me.MedListPanel.Controls.Add(Me.Medicament5)
        Me.MedListPanel.Controls.Add(Me.RemoveMed5)
        Me.MedListPanel.Controls.Add(Me.Posologie4)
        Me.MedListPanel.Controls.Add(Me.Medicament4)
        Me.MedListPanel.Controls.Add(Me.RemoveMed4)
        Me.MedListPanel.Controls.Add(Me.Posologie3)
        Me.MedListPanel.Controls.Add(Me.Posologie2)
        Me.MedListPanel.Controls.Add(Me.Medicament3)
        Me.MedListPanel.Controls.Add(Me.Medicament2)
        Me.MedListPanel.Controls.Add(Me.Posologie1)
        Me.MedListPanel.Controls.Add(Me.Medicament1)
        Me.MedListPanel.Controls.Add(Me.NomPatient)
        Me.MedListPanel.Controls.Add(Me.RemoveMed3)
        Me.MedListPanel.Controls.Add(Me.RemoveMed2)
        Me.MedListPanel.Controls.Add(Me.RemoveMed1)
        Me.MedListPanel.Location = New System.Drawing.Point(315, 19)
        Me.MedListPanel.Name = "MedListPanel"
        Me.MedListPanel.Size = New System.Drawing.Size(860, 395)
        Me.MedListPanel.TabIndex = 0
        '
        'Posologie5
        '
        Me.Posologie5.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Posologie5.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Posologie5.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Posologie5.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Posologie5.BorderThickness = 3
        Me.Posologie5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Posologie5.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.Posologie5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.Posologie5.isPassword = False
        Me.Posologie5.Location = New System.Drawing.Point(358, 318)
        Me.Posologie5.Margin = New System.Windows.Forms.Padding(4)
        Me.Posologie5.Name = "Posologie5"
        Me.Posologie5.Size = New System.Drawing.Size(397, 36)
        Me.Posologie5.TabIndex = 41
        Me.Posologie5.Text = "Posologie"
        Me.Posologie5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'Medicament5
        '
        Me.Medicament5.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Medicament5.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Medicament5.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Medicament5.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Medicament5.BorderThickness = 3
        Me.Medicament5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Medicament5.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.Medicament5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.Medicament5.isPassword = False
        Me.Medicament5.Location = New System.Drawing.Point(41, 318)
        Me.Medicament5.Margin = New System.Windows.Forms.Padding(4)
        Me.Medicament5.Name = "Medicament5"
        Me.Medicament5.Size = New System.Drawing.Size(304, 36)
        Me.Medicament5.TabIndex = 40
        Me.Medicament5.Text = "Medicament"
        Me.Medicament5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'RemoveMed5
        '
        Me.RemoveMed5.BackColor = System.Drawing.Color.White
        Me.RemoveMed5.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Cancel
        Me.RemoveMed5.ImageActive = Nothing
        Me.RemoveMed5.Location = New System.Drawing.Point(783, 318)
        Me.RemoveMed5.Name = "RemoveMed5"
        Me.RemoveMed5.Size = New System.Drawing.Size(36, 36)
        Me.RemoveMed5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.RemoveMed5.TabIndex = 39
        Me.RemoveMed5.TabStop = False
        Me.RemoveMed5.Zoom = 10
        '
        'Posologie4
        '
        Me.Posologie4.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Posologie4.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Posologie4.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Posologie4.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Posologie4.BorderThickness = 3
        Me.Posologie4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Posologie4.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.Posologie4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.Posologie4.isPassword = False
        Me.Posologie4.Location = New System.Drawing.Point(358, 260)
        Me.Posologie4.Margin = New System.Windows.Forms.Padding(4)
        Me.Posologie4.Name = "Posologie4"
        Me.Posologie4.Size = New System.Drawing.Size(397, 36)
        Me.Posologie4.TabIndex = 38
        Me.Posologie4.Text = "Posologie"
        Me.Posologie4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'Medicament4
        '
        Me.Medicament4.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Medicament4.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Medicament4.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Medicament4.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Medicament4.BorderThickness = 3
        Me.Medicament4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Medicament4.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.Medicament4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.Medicament4.isPassword = False
        Me.Medicament4.Location = New System.Drawing.Point(41, 260)
        Me.Medicament4.Margin = New System.Windows.Forms.Padding(4)
        Me.Medicament4.Name = "Medicament4"
        Me.Medicament4.Size = New System.Drawing.Size(304, 36)
        Me.Medicament4.TabIndex = 37
        Me.Medicament4.Text = "Medicament"
        Me.Medicament4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'RemoveMed4
        '
        Me.RemoveMed4.BackColor = System.Drawing.Color.White
        Me.RemoveMed4.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Cancel
        Me.RemoveMed4.ImageActive = Nothing
        Me.RemoveMed4.Location = New System.Drawing.Point(783, 260)
        Me.RemoveMed4.Name = "RemoveMed4"
        Me.RemoveMed4.Size = New System.Drawing.Size(36, 36)
        Me.RemoveMed4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.RemoveMed4.TabIndex = 36
        Me.RemoveMed4.TabStop = False
        Me.RemoveMed4.Zoom = 10
        '
        'Posologie3
        '
        Me.Posologie3.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Posologie3.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Posologie3.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Posologie3.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Posologie3.BorderThickness = 3
        Me.Posologie3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Posologie3.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.Posologie3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.Posologie3.isPassword = False
        Me.Posologie3.Location = New System.Drawing.Point(358, 193)
        Me.Posologie3.Margin = New System.Windows.Forms.Padding(4)
        Me.Posologie3.Name = "Posologie3"
        Me.Posologie3.Size = New System.Drawing.Size(397, 36)
        Me.Posologie3.TabIndex = 20
        Me.Posologie3.Text = "Posologie"
        Me.Posologie3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'Posologie2
        '
        Me.Posologie2.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Posologie2.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Posologie2.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Posologie2.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Posologie2.BorderThickness = 3
        Me.Posologie2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Posologie2.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.Posologie2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.Posologie2.isPassword = False
        Me.Posologie2.Location = New System.Drawing.Point(358, 124)
        Me.Posologie2.Margin = New System.Windows.Forms.Padding(4)
        Me.Posologie2.Name = "Posologie2"
        Me.Posologie2.Size = New System.Drawing.Size(397, 36)
        Me.Posologie2.TabIndex = 19
        Me.Posologie2.Text = "Posologie"
        Me.Posologie2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'Medicament3
        '
        Me.Medicament3.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Medicament3.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Medicament3.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Medicament3.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Medicament3.BorderThickness = 3
        Me.Medicament3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Medicament3.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.Medicament3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.Medicament3.isPassword = False
        Me.Medicament3.Location = New System.Drawing.Point(41, 193)
        Me.Medicament3.Margin = New System.Windows.Forms.Padding(4)
        Me.Medicament3.Name = "Medicament3"
        Me.Medicament3.Size = New System.Drawing.Size(304, 36)
        Me.Medicament3.TabIndex = 18
        Me.Medicament3.Text = "Medicament"
        Me.Medicament3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'Medicament2
        '
        Me.Medicament2.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Medicament2.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Medicament2.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Medicament2.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Medicament2.BorderThickness = 3
        Me.Medicament2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Medicament2.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.Medicament2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.Medicament2.isPassword = False
        Me.Medicament2.Location = New System.Drawing.Point(41, 124)
        Me.Medicament2.Margin = New System.Windows.Forms.Padding(4)
        Me.Medicament2.Name = "Medicament2"
        Me.Medicament2.Size = New System.Drawing.Size(304, 36)
        Me.Medicament2.TabIndex = 17
        Me.Medicament2.Text = "Medicament"
        Me.Medicament2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'Posologie1
        '
        Me.Posologie1.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Posologie1.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Posologie1.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Posologie1.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Posologie1.BorderThickness = 3
        Me.Posologie1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Posologie1.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.Posologie1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.Posologie1.isPassword = False
        Me.Posologie1.Location = New System.Drawing.Point(356, 54)
        Me.Posologie1.Margin = New System.Windows.Forms.Padding(4)
        Me.Posologie1.Name = "Posologie1"
        Me.Posologie1.Size = New System.Drawing.Size(397, 36)
        Me.Posologie1.TabIndex = 16
        Me.Posologie1.Text = "Posologie"
        Me.Posologie1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'Medicament1
        '
        Me.Medicament1.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Medicament1.BorderColorFocused = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Medicament1.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Medicament1.BorderColorMouseHover = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.Medicament1.BorderThickness = 3
        Me.Medicament1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Medicament1.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.Medicament1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(136, Byte), Integer))
        Me.Medicament1.isPassword = False
        Me.Medicament1.Location = New System.Drawing.Point(41, 54)
        Me.Medicament1.Margin = New System.Windows.Forms.Padding(4)
        Me.Medicament1.Name = "Medicament1"
        Me.Medicament1.Size = New System.Drawing.Size(304, 36)
        Me.Medicament1.TabIndex = 15
        Me.Medicament1.Text = "Medicament"
        Me.Medicament1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'NomPatient
        '
        Me.NomPatient.AutoSize = True
        Me.NomPatient.Location = New System.Drawing.Point(172, 14)
        Me.NomPatient.Name = "NomPatient"
        Me.NomPatient.Size = New System.Drawing.Size(88, 13)
        Me.NomPatient.TabIndex = 12
        Me.NomPatient.Text = "Nom du patient : "
        '
        'RemoveMed3
        '
        Me.RemoveMed3.BackColor = System.Drawing.Color.White
        Me.RemoveMed3.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Cancel
        Me.RemoveMed3.ImageActive = Nothing
        Me.RemoveMed3.Location = New System.Drawing.Point(783, 193)
        Me.RemoveMed3.Name = "RemoveMed3"
        Me.RemoveMed3.Size = New System.Drawing.Size(36, 36)
        Me.RemoveMed3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.RemoveMed3.TabIndex = 11
        Me.RemoveMed3.TabStop = False
        Me.RemoveMed3.Zoom = 10
        '
        'RemoveMed2
        '
        Me.RemoveMed2.BackColor = System.Drawing.Color.White
        Me.RemoveMed2.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Cancel
        Me.RemoveMed2.ImageActive = Nothing
        Me.RemoveMed2.Location = New System.Drawing.Point(783, 123)
        Me.RemoveMed2.Name = "RemoveMed2"
        Me.RemoveMed2.Size = New System.Drawing.Size(36, 36)
        Me.RemoveMed2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.RemoveMed2.TabIndex = 10
        Me.RemoveMed2.TabStop = False
        Me.RemoveMed2.Zoom = 10
        '
        'RemoveMed1
        '
        Me.RemoveMed1.BackColor = System.Drawing.Color.White
        Me.RemoveMed1.Image = Global.GestionDeStock.My.Resources.Resources.icons8_Cancel
        Me.RemoveMed1.ImageActive = Nothing
        Me.RemoveMed1.Location = New System.Drawing.Point(783, 54)
        Me.RemoveMed1.Name = "RemoveMed1"
        Me.RemoveMed1.Size = New System.Drawing.Size(36, 36)
        Me.RemoveMed1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.RemoveMed1.TabIndex = 9
        Me.RemoveMed1.TabStop = False
        Me.RemoveMed1.Zoom = 10
        '
        'BunifuDragControl1
        '
        Me.BunifuDragControl1.Fixed = True
        Me.BunifuDragControl1.Horizontal = True
        Me.BunifuDragControl1.TargetControl = Me.TopPanel
        Me.BunifuDragControl1.Vertical = True
        '
        'BunifuDragControl2
        '
        Me.BunifuDragControl2.Fixed = True
        Me.BunifuDragControl2.Horizontal = True
        Me.BunifuDragControl2.TargetControl = Me.PanelDragger
        Me.BunifuDragControl2.Vertical = True
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1366, 866)
        Me.Controls.Add(Me.PNL_Ordonnance)
        Me.Controls.Add(Me.PNL_Stock_Articles)
        Me.Controls.Add(Me.PNL_Stock_Commande)
        Me.Controls.Add(Me.PNL_Stock_Stock)
        Me.Controls.Add(Me.PNL_Stock_Suppliers)
        Me.Controls.Add(Me.PNL_Dashboard)
        Me.Controls.Add(Me.PNL_Login)
        Me.Controls.Add(Me.LeftPanel)
        Me.Controls.Add(Me.TopPanel)
        Me.ForeColor = System.Drawing.SystemColors.ControlText
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "MainForm"
        Me.LeftPanel.ResumeLayout(False)
        Me.LeftPanel.PerformLayout()
        CType(Me.SlideMenuRight_IMG, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TopPanel.ResumeLayout(False)
        CType(Me.Indicator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelDragger.ResumeLayout(False)
        CType(Me.Close_BTN, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PNL_Stock_Commande.ResumeLayout(False)
        Me.Commande_GeneralData1.ResumeLayout(False)
        Me.Commande_GeneralData1.PerformLayout()
        Me.ListOfOrders.ResumeLayout(False)
        Me.ListOfOrders.PerformLayout()
        CType(Me.PayOrder_BTN, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Delete_Order_Oders_BTN, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Modify_Order_Oders_BTN, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Add_Order_Oders_BTN, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SearchAreaOrders.ResumeLayout(False)
        Me.SearchAreaOrders.PerformLayout()
        Me.PNL_Stock_Stock.ResumeLayout(False)
        Me.HighPanel.ResumeLayout(False)
        Me.HighPanel.PerformLayout()
        Me.Stock_SearchArea.ResumeLayout(False)
        Me.Stock_SearchArea.PerformLayout()
        Me.LowPanel.ResumeLayout(False)
        Me.LowPanel.PerformLayout()
        Me.ListOfArticlesInStock.ResumeLayout(False)
        Me.ListOfArticlesInStock.PerformLayout()
        CType(Me.UseSupply, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Stock_DGV, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PNL_Login.ResumeLayout(False)
        Me.PNL_Login.PerformLayout()
        Me.PNL_Login_Authentification.ResumeLayout(False)
        Me.PNL_Dashboard.ResumeLayout(False)
        Me.PNL_Dashboard.PerformLayout()
        Me.Dashboard_First_PNL.ResumeLayout(False)
        Me.Dashboard_First_PNL.PerformLayout()
        Me.PNL_Stock_Articles.ResumeLayout(False)
        Me.SearchArticleInStock.ResumeLayout(False)
        Me.SearchArticleInStock.PerformLayout()
        Me.ListOfStock.ResumeLayout(False)
        Me.ListOfStock.PerformLayout()
        CType(Me.DeleteArticle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Modify_Article_BTN, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Stock_AddArticle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ListOfArticles_DGV, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PNL_Stock_Suppliers.ResumeLayout(False)
        Me.Stock_Suppliers_SearchArea.ResumeLayout(False)
        Me.Stock_Suppliers_SearchArea.PerformLayout()
        Me.ListOfSuppliers.ResumeLayout(False)
        Me.ListOfSuppliers.PerformLayout()
        CType(Me.Suppliers_DeleteSupplier, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Stock_ModifySupplier_BTN, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Stock_AddSupplier_BTN, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SuppliersList_DGV, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PNL_Ordonnance.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ListeMedicament.ResumeLayout(False)
        Me.ListeMedicament.PerformLayout()
        CType(Me.ClearBTN, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        CType(Me.Ordonnance1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ordonnance2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ordonnance3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ordonnance4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ordonnance5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Save1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Save5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Save2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Save4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Save3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MedListPanel.ResumeLayout(False)
        Me.MedListPanel.PerformLayout()
        CType(Me.RemoveMed5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RemoveMed4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RemoveMed3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RemoveMed2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RemoveMed1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LeftPanel As Panel
    Friend WithEvents TopPanel As Panel
    Friend WithEvents MenuSlideRight_IMG As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents BunifuCustomLabel1 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents BunifuFlatButton1 As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents BunifuImageButton2 As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents BunifuFlatButton5 As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents BunifuFlatButton4 As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents BunifuFlatButton3 As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents BunifuFlatButton2 As Bunifu.Framework.UI.BunifuFlatButton
    ' Friend WithEvents Dashboard_BTN As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents LogoAnimator As BunifuAnimatorNS.BunifuTransition
    Friend WithEvents PanelAnimator As BunifuAnimatorNS.BunifuTransition
    Friend WithEvents BunifuElipse1 As Bunifu.Framework.UI.BunifuElipse
    Friend WithEvents MenuSlideLeft_IMG As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents Search_BTN As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents Delete_BTN As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents Modify_BTN As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents Add_BTN As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents ActionsAnimator As BunifuAnimatorNS.BunifuTransition
    Friend WithEvents BunifuCustomDataGrid1 As Bunifu.Framework.UI.BunifuCustomDataGrid
    Friend WithEvents NDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents UniteDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MainMenuDashboard_BTN As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents SlideMenuRight_IMG As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents PNL_Stock_Commande As Panel
    Friend WithEvents dgv1 As Bunifu.Framework.UI.BunifuCustomDataGrid
    Friend WithEvents MainMenuStock_B_BTN As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents MainMenuLaboratoire_B_BTN As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents MainMenuOrdonnance_B_BTN As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents MainMenuMutuelle_B_BTN As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents MainMenuTracabilite_B_BTN As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents SubMenuButton_1_BTN As Bunifu.Framework.UI.BunifuTileButton
    Friend WithEvents Indicator As PictureBox
    Friend WithEvents SubMenuButton_2_BTN As Bunifu.Framework.UI.BunifuTileButton
    Friend WithEvents PanelDragger As Panel
    Friend WithEvents Close_BTN As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents SubMenuButton_5_BTN As Bunifu.Framework.UI.BunifuTileButton
    Friend WithEvents SubMenuButton_4_BTN As Bunifu.Framework.UI.BunifuTileButton
    Friend WithEvents SubMenuButton_3_BTN As Bunifu.Framework.UI.BunifuTileButton
    Friend WithEvents MainMenuParametre_B_BTN As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents SearchAreaOrders As Panel
    Friend WithEvents BunifuCustomLabel2 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents Rechercher_Code_TXB As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents ListOfOrders As Panel
    Friend WithEvents ListeDeCommande_LBL As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents Delete_Order_Oders_BTN As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents Modify_Order_Oders_BTN As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents Add_Order_Oders_BTN As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents UseDateInSearch As Bunifu.Framework.UI.BunifuSwitch
    Friend WithEvents BunifuCustomLabel4 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents Rechercher_Fournisseur_CMB As ComboBox
    Friend WithEvents PNL_Login As Panel
    Friend WithEvents Password_TXB As Bunifu.Framework.UI.BunifuMaterialTextbox
    Friend WithEvents Login_TXB As Bunifu.Framework.UI.BunifuMaterialTextbox
    Friend WithEvents Authentificate_BTN As Bunifu.Framework.UI.BunifuThinButton2
    Friend WithEvents PNL_Login_Authentification As Panel
    Friend WithEvents PNL_Dashboard As Panel
    Friend WithEvents BunifuCustomLabel5 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents Logout_BTN As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents Dashboard_First_PNL As Panel
    Friend WithEvents Commande_GeneralData1 As Panel
    Friend WithEvents BunifuCustomLabel7 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents BunifuCustomLabel10 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents BunifuCustomLabel11 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents PNL_Stock_Stock As Panel
    Friend WithEvents Stock_SearchArea As Panel
    Friend WithEvents BunifuCustomLabel14 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents ListOfArticlesInStock As Panel
    Friend WithEvents UseSupply As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents Stock_DGV As Bunifu.Framework.UI.BunifuCustomDataGrid
    Friend WithEvents Stock_LBL As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents article_name_TXB As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents PNL_Stock_Articles As Panel
    Friend WithEvents SearchArticleInStock As Panel
    Friend WithEvents Stock_Articles_Articles_TXB As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents BunifuCustomLabel16 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents ListOfStock As Panel
    Friend WithEvents DeleteArticle As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents Modify_Article_BTN As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents Stock_AddArticle As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents ListOfArticles_DGV As Bunifu.Framework.UI.BunifuCustomDataGrid
    Friend WithEvents BunifuCustomLabel18 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents PayOrder_BTN As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents PNL_Stock_Suppliers As Panel
    Friend WithEvents Stock_Suppliers_SearchArea As Panel
    Friend WithEvents SupplierName As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents BunifuCustomLabel28 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents ListOfSuppliers As Panel
    Friend WithEvents Suppliers_DeleteSupplier As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents Stock_ModifySupplier_BTN As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents Stock_AddSupplier_BTN As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents SuppliersList_DGV As Bunifu.Framework.UI.BunifuCustomDataGrid
    Friend WithEvents BunifuCustomLabel30 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents SupplierPhone As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents SupplierAddresse As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents SupplierLastName As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents Suppliercity As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents Au_DTP As DateTimePicker
    Friend WithEvents De_DTP As DateTimePicker
    Friend WithEvents UnpaidOrders As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents LowPanel As Panel
    Friend WithEvents HighPanel As Panel
    Friend WithEvents HighAlert As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents PNL_Ordonnance As Panel
    Friend WithEvents ListeMedicament As Panel
    Friend WithEvents ListeMedicament_CMB As ComboBox
    Friend WithEvents Panel3 As Panel
    Friend WithEvents MedListPanel As Panel
    Friend WithEvents MedicamentLabel As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents ListePosologie_CMB As ComboBox
    Friend WithEvents ClearBTN As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents PosologieLabel As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents TypeLabel As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents ListeType_CMB As ComboBox
    Friend WithEvents NomPatient As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents RemoveMed3 As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents RemoveMed2 As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents RemoveMed1 As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents Posologie3 As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents Posologie2 As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents Medicament3 As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents Medicament2 As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents Posologie1 As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents Medicament1 As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents AjouterMed As Bunifu.Framework.UI.BunifuThinButton2
    Friend WithEvents Ordonnance5 As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents Ordonnance4 As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents Ordonnance3 As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents Ordonnance2 As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents Ordonnance1 As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents Save5 As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents Save4 As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents Save3 As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents Save2 As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents Save1 As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents Panel1 As Panel
    Friend WithEvents BunifuCustomLabel3 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents Posologie5 As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents Medicament5 As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents RemoveMed5 As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents Posologie4 As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents Medicament4 As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents RemoveMed4 As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents Low_LBL As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents ConnectedUserName As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents High_LBL As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents LowAlert As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents TotalMoneyToSuppliers As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents BunifuDragControl1 As Bunifu.Framework.UI.BunifuDragControl
    Friend WithEvents BunifuDragControl2 As Bunifu.Framework.UI.BunifuDragControl
End Class
